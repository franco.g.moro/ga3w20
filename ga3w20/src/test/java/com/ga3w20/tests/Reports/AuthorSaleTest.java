package com.ga3w20.tests.Reports;

import com.ga3w20.beans.Quartet;
import com.ga3w20.controllers.BooksJpaController;
import com.ga3w20.custom.entities.AuthorSale;
import com.ga3w20.entities.Books;
import java.io.File;
import static org.assertj.core.api.Assertions.assertThat;
import com.ga3w20.exceptions.RollbackFailureException;
import com.ga3w20.tests.JpaController.ParameterRule;
import java.util.List;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.runner.RunWith;
import javax.inject.Inject;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Hadil Elhashani
 */
@RunWith(Arquillian.class)
public class AuthorSaleTest {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(AuthorSaleTest.class);

    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The webArchive is the special packaging of your project
        // so that only the test cases run on the server or embedded
        // container
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(Books.class.getPackage())
                .addPackage(BooksJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/test/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createTables.sql")
                .addClass(ParameterRule.class)
                .addClass(AuthorSale.class)
                .addClass(Quartet.class)
                .addAsLibraries(dependencies);

        return webArchive;
    }

    @Inject
    private BooksJpaController booksController;

    private Quartet<String, String, String, Integer> dateRange;
    
    @Rule
    public ParameterRule rule = new ParameterRule("dateRange",
            new Quartet<String, String, String, Integer>("2020-01-01", "2020-01-08", "Stephen King", 5),
            new Quartet<String, String, String, Integer>("2020-01-01", "2020-01-07", "Thomas Harris", 1),
            new Quartet<String, String, String, Integer>("2020-01-04", "2020-02-12", "Marie Shelly", 9),
            new Quartet<String, String, String, Integer>("2020-02-01", "2020-02-11", "Shirley Jackson", 1),
            new Quartet<String, String, String, Integer>("2020-01-17", "2020-02-11", "Frank Herbert", 2),
            new Quartet<String, String, String, Integer>("2020-01-01", "2020-02-01", "Suzanne Collins", 9));

    @Test
    public void findRowCount() {
        LOG.debug("findRowCount");
        List topSellers = this.booksController.findSalesByAuthor(dateRange.getValue0(), dateRange.getValue1(), dateRange.getValue2());
        assertThat(topSellers).hasSize(dateRange.getValue3());
    }

}
