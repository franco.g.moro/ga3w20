package com.ga3w20.tests.ui;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Selenium test class for client front door
 *
 * @author Hadil Elhashani
 */
public class FrontDoorTestIT {

    private WebDriver driver;

    @BeforeClass
    public static void setupClass() {
        // Normally an executable that matches the browser you are using must
        // be in the classpath. The webdrivermanager library by Boni Garcia
        // downloads the required driver and makes it available
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void setupTest() {
        driver = new ChromeDriver();
    }

    /**
     * Test window title
     *
     * @throws Exception
     */
    @Test
    public void testBookStoreTitle() throws Exception {
        driver.get("http://localhost:8080/ga3w20");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // Wait for the page to load, timeout after 10 seconds
        wait.until(ExpectedConditions.titleIs("TomNookBooks"));
    }

    /**
     * Test banner ad link and goes to the second tab
     *
     * @author Hadil Elhashani
     * @throws Exception
     */
    @Test
    public void testAdvertisment() throws Exception {
        driver.get("http://localhost:8080/ga3w20");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // Check the title of the page
        wait.until(ExpectedConditions.titleIs("TomNookBooks"));
        //finds div with class name advertisement
        WebElement advertisement = driver.findElement(By.className("advertisement"));
        //finds the outputlink tag and click it
        WebElement link = advertisement.findElement(By.tagName("a"));

        String href = link.getAttribute("href");

        link.click();
        //finds all tabs of browser
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        //goes to second one
        driver.switchTo().window(tabs.get(1));
        //compares urls
        wait.until(ExpectedConditions.urlContains(href));
    }

    /**
     * Test news feed links and goes to the specified tabs
     *
     * @author Hadil Elhashani
     * @throws Exception
     */
    @Test
    public void testNewsFeed() throws Exception {
        driver.get("http://localhost:8080/ga3w20");

        // Wait for the page to load, timeout after 20 seconds
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.titleIs("TomNookBooks"));

        //finds all news feed buttons 
        List<WebElement> newsBtn = driver.findElements(By.id("newsBtn"));

        //retrives the href attribute 
        String href = newsBtn.get(0).getAttribute("href");

        newsBtn.get(0).click();

        //retrieves all tabs in the browser 
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        //goes to first tab 
        driver.switchTo().window(tabs.get(1));
        //compares urls
        wait.until(ExpectedConditions.urlContains(href));
    }

    /**
     * Test browse book button's url
     *
     * @author Hadil Elhashani
     * @throws Exception
     */
    @Test
    public void testBrowseBookButton() throws Exception {
        driver.get("http://localhost:8080/ga3w20");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        //finds brwoseBooksBtn and click it
        WebElement browseBookBtn = driver.findElement(By.id("brwoseBooksBtn"));

        browseBookBtn.click();
        //url to be should be this 
        wait.until(ExpectedConditions.urlToBe("http://localhost:8080/ga3w20/browsepage.xhtml?selectBy=bookTitle"));
    }
    

    /**
     * Test help button URL
     *
     * @author Alexander Berestetskyy
     * @throws Exception
     */
    @Test
    public void testHelpButton() throws Exception {
        driver.get("http://localhost:8080/ga3w20");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        //finds helpBtn and click it
        WebElement helpBtn = driver.findElement(By.id("helpBtn"));

        helpBtn.click();
        //url to be should be this 
        wait.until(ExpectedConditions.urlToBe("http://localhost:8080/ga3w20/help.xhtml"));
    }
    
    /**
     * Test browse page redirection URL
     *
     * @author Alexander Berestetskyy
     * @throws Exception
     */
    @Test
    public void testBrowseSearchButton() throws Exception {
        driver.get("http://localhost:8080/ga3w20");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        //change select of searching by author name
        Select selectBy = new Select(driver.findElement(By.className("selectByMenu")));
        selectBy.selectByIndex(1);
        //find the input for the search and type in it "King"
        WebElement selectBySearch = driver.findElement(By.id("j_idt38:searchTerm"));
        selectBySearch.sendKeys("king");
        //finds browsepage button and click it
        WebElement browseBtn = driver.findElement(By.id("searchSelectBy"));

        browseBtn.click();
        //url to be should be this 
        wait.until(ExpectedConditions.urlToBe("http://localhost:8080/ga3w20/browsepage.xhtml?selectBy=authorName&searchTerm=king"));
    }
    
    /**
     * Test browse by genre/top seller by genre URL page
     *
     * @author Alexander Berestetskyy
     * @throws Exception
     */
    @Test
    public void testBrowseByGenre() throws Exception {
        driver.get("http://localhost:8080/ga3w20");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        //change select of browse by genre to dystopia to find top sellers in that genre
        Select selectBrowsePage = new Select(driver.findElement(By.className("selectByGenre")));
        selectBrowsePage.selectByIndex(2);
       //finds browsepage button and click it
        WebElement browseByGenreBtn = driver.findElement(By.id("searchBrowseByGenre"));

        browseByGenreBtn.click();
        //url to be should be this 
        wait.until(ExpectedConditions.urlToBe("http://localhost:8080/ga3w20/browsebygenre.xhtml?selectByGenre=Dystopia"));
    }
    
     /**
     * Test the log in page redirection
     *
     * @author Alexander Berestetskyy
     * @throws Exception
     */
    @Test
    public void testLogIn() throws Exception {
        driver.get("http://localhost:8080/ga3w20");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        //finds login button and click it
        WebElement loginBtn = driver.findElement(By.id("loginBtn"));

        loginBtn.click();
        //retrieving session id
        Cookie cookie= driver.manage().getCookieNamed("JSESSIONID");
        //url to be should be this 
        wait.until(ExpectedConditions.urlToBe("http://localhost:8080/ga3w20/signin.xhtml;jsessionid="+cookie.getValue()));
    }
    
     /**
     * Test the sign up page redirection
     *
     * @author Alexander Berestetskyy
     * @throws Exception
     */
    @Test
    public void testSignUp() throws Exception {
        driver.get("http://localhost:8080/ga3w20");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        //finds signup button and click it
        WebElement signupBtn = driver.findElement(By.id("signupBtn"));

        signupBtn.click();
        //retrieving session id
        Cookie cookie= driver.manage().getCookieNamed("JSESSIONID");
        //url to be should be this 
        wait.until(ExpectedConditions.urlToBe("http://localhost:8080/ga3w20/signup.xhtml;jsessionid="+cookie.getValue()));
    }
    
    /**
     * Test the administration button
     *
     * @author Alexander Berestetskyy
     * @throws Exception
     */
    @Test
    public void testAdministrationButton() throws Exception {
        driver.get("http://localhost:8080/ga3w20");

        // Wait for the page to load, timeout after 20 seconds
        WebDriverWait wait = new WebDriverWait(driver, 20);
        //finds login button and click it
        WebElement loginBtn = driver.findElement(By.id("loginBtn"));
        loginBtn.click();
        
        //fill inputs for email and password and click submit
        WebElement emailInput = driver.findElement(By.id("j_idt83:email"));
        emailInput.sendKeys("alex@alex.ca");
        WebElement pwdInput = driver.findElement(By.id("j_idt83:pwd"));
        pwdInput.sendKeys("ga3w20");
        WebElement submitBtn = driver.findElement(By.id("j_idt83:submitLogin"));
        submitBtn.click();
        
        //administration should be displayed and we can click on it
        WebElement adminBtn = driver.findElement(By.id("adminBtn"));
        adminBtn.click();
        
        wait.until(ExpectedConditions.urlToBe("http://localhost:8080/ga3w20/management/management.xhtml"));
    }
    
    /**
     * Test the download page button
     *
     * @author Alexander Berestetskyy
     * @throws Exception
     */
    @Test
    public void testDownloadPageButton() throws Exception {
        driver.get("http://localhost:8080/ga3w20");

        // Wait for the page to load, timeout after 20 seconds
        WebDriverWait wait = new WebDriverWait(driver, 20);
        //finds login button and click it
        WebElement loginBtn = driver.findElement(By.id("loginBtn"));
        loginBtn.click();
        
        //fill inputs for email and password and click submit
        WebElement emailInput = driver.findElement(By.id("j_idt83:email"));
        emailInput.sendKeys("cst.send@gmail.com");
        WebElement pwdInput = driver.findElement(By.id("j_idt83:pwd"));
        pwdInput.sendKeys("dawsoncollege");
        WebElement submitBtn = driver.findElement(By.id("j_idt83:submitLogin"));
        submitBtn.click();
        
        //Download page should be displayed and we can click on it
        WebElement adminBtn = driver.findElement(By.id("downloadPageBtn"));
        adminBtn.click();
        
        wait.until(ExpectedConditions.urlToBe("http://localhost:8080/ga3w20/downloads.xhtml"));
    }
    
    @After
    public void shutdownTest() {
        //Close the browser
        driver.quit();
    }

}
