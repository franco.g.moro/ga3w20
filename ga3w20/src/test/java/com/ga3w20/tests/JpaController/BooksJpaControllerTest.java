package com.ga3w20.tests.JpaController;

import com.ga3w20.controllers.BooksJpaController;
import com.ga3w20.controllers.GenreJpaController;
import com.ga3w20.entities.Books;
import com.ga3w20.entities.Genre;
import com.ga3w20.entities.Reviews;
import com.ga3w20.exceptions.RollbackFailureException;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import javax.inject.Inject;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Ignore;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Yongchao
 */
@RunWith(Arquillian.class)
public class BooksJpaControllerTest {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(BooksJpaControllerTest.class);

    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The webArchive is the special packaging of your project
        // so that only the test cases run on the server or embedded
        // container
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(Books.class.getPackage())
                .addPackage(BooksJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/test/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createTables.sql")
                .addAsLibraries(dependencies);

        return webArchive;
    }

    @Inject
    private BooksJpaController booksJpa;
    @Inject
    private GenreJpaController genreJpa;

    @Test
    public void FindAllEntities() {        
        List<Books> entityList = booksJpa.findBooksEntities();
        LOG.info("Total books number: ", entityList.size());
        assertThat(entityList).hasSize(99);
    }
    
    /**
     * Tests the method findBooksWithGenre in BooksJpaController, which retrieves all the books that follow the search
     * pattern that represents either a book title, author name, publisher or isbn and that is also found to a genre.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindBooksWithinGenre(){
        List<Books> entityList1 = booksJpa.findBooksWithGenre("the", "bookTitle", "Horror");
        assertThat(entityList1).hasSize(9);
        List<Books> entityList2 = booksJpa.findBooksWithGenre("the", "bookTitle", "Action-Adventure");
        assertThat(entityList2).hasSize(11);
        List<Books> entityList3 = booksJpa.findBooksWithGenre("king", "authorName", "Horror");
        assertThat(entityList3).hasSize(5);
        List<Books> entityList4 = booksJpa.findBooksWithGenre("a", "publisher", "Dystopia");
        assertThat(entityList4).hasSize(12);
        List<Books> entityList5 = booksJpa.findBooksWithGenre("1", "isbn", "Sci-Fi");
        assertThat(entityList5).hasSize(18);
    }
    
    /**
     * Tests the method findBooksWithGenreNoSearchTerm in BooksJpaController, which retrieves all the books that fall into
     * a genre, used for mainly when client clicks on the "browse books button".
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindBooksWithGenreNoSearchTerm(){
        List<Books> entityList1 = booksJpa.findBooksWithGenreNoSearchTerm("Horror");
        assertThat(entityList1).hasSize(20);
        List<Books> entityList2 = booksJpa.findBooksWithGenreNoSearchTerm("Action-Adventure");
        assertThat(entityList2).hasSize(18);
        List<Books> entityList3 = booksJpa.findBooksWithGenreNoSearchTerm("Fantasy");
        assertThat(entityList3).hasSize(21);
        List<Books> entityList4 = booksJpa.findBooksWithGenreNoSearchTerm("Dystopia");
        assertThat(entityList4).hasSize(20);
        List<Books> entityList5 = booksJpa.findBooksWithGenreNoSearchTerm("Sci-Fi");
        assertThat(entityList5).hasSize(20);
    }
    
    /**
     * Tests the method findBooksWithTitle in BooksJpaController, which retrieves all the books that follow the search
     * pattern that represents a book title.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindBooksWithTitle(){
        List<Books> entityList1 = booksJpa.findBooksWithTitle("the");
        assertThat(entityList1).hasSize(43);
        List<Books> entityList2 = booksJpa.findBooksWithTitle("tand");
        assertThat(entityList2).hasSize(1);
        List<Books> entityList3 = booksJpa.findBooksWithTitle("a");
        assertThat(entityList3).hasSize(67);
        List<Books> entityList4 = booksJpa.findBooksWithTitle("kfjndf");
        assertThat(entityList4).hasSize(0);
    }
    
    /**
     * Tests the method findBooksWithAuthorName in BooksJpaController, which retrieves all the books that follow the search
     * pattern that represents an author name.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindBooksWithAuthorName(){
        List<Books> entityList1 = booksJpa.findBooksWithAuthorName("King");
        assertThat(entityList1).hasSize(5);
        List<Books> entityList2 = booksJpa.findBooksWithAuthorName("sksdf");
        assertThat(entityList2).hasSize(0);
        List<Books> entityList3 = booksJpa.findBooksWithAuthorName("b");
        assertThat(entityList3).hasSize(19);
    }
    
    /**
     * Tests the method findBooksWithPublisher in BooksJpaController, which retrieves all the books that follow the search
     * pattern that represents a publisher.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindBooksWithPublisher(){
        List<Books> entityList1 = booksJpa.findBooksWithPublisher("anchor");
        assertThat(entityList1).hasSize(4);
        List<Books> entityList2 = booksJpa.findBooksWithPublisher("inode[pdf");
        assertThat(entityList2).hasSize(0);
        List<Books> entityList3 = booksJpa.findBooksWithPublisher("b");
        assertThat(entityList3).hasSize(43);
    }
    
    /**
     * Tests the method findBooksWithPublisher in BooksJpaController, which retrieves all the books that follow the search
     * pattern that represents an isbn.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindBooksWithIsbn(){
        List<Books> entityList1 = booksJpa.findBooksWithIsbn("17");
        assertThat(entityList1).hasSize(7);
        List<Books> entityList2 = booksJpa.findBooksWithIsbn("1");
        assertThat(entityList2).hasSize(76);
        List<Books> entityList3 = booksJpa.findBooksWithIsbn("b");
        assertThat(entityList3).hasSize(0);
    }
    
    /**
     * Tests the method findGenresWithTitle in BooksJpaController, which retrieves a hashmaps with the key representing
     * the name of genre and the value is the number of books in that genre, with a search pattern that represents a book title. 
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindGenresWithTitle(){
        HashMap entityHashMap1 = booksJpa.findGenresWithTitle("tand");
        assertThat(entityHashMap1).hasSize(1);
        assertThat(entityHashMap1).containsKey("Horror");
        assertThat(entityHashMap1).doesNotContainKey("Action-Adventure");
        assertThat(entityHashMap1).containsValues(1L);
        HashMap entityHashMap2 = booksJpa.findGenresWithTitle("the");
        assertThat(entityHashMap2).containsKeys("Horror","Sci-Fi","Fantasy","Dystopia","Action-Adventure");
        assertThat(entityHashMap2).containsValues(9L,11L);
        HashMap entityHashMap3 = booksJpa.findGenresWithTitle("sdkfjsidjs");
        assertThat(entityHashMap3).isEmpty();
    }
    
    /**
     * Tests the method findGenresWithAuthorName in BooksJpaController, which retrieves a hashmaps with the key representing
     * the name of genre and the value is the number of books in that genre, with a search pattern that represents an author name. 
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindGenresWithAuthorName(){
        HashMap entityHashMap1 = booksJpa.findGenresWithAuthorName("King");
        assertThat(entityHashMap1).containsKey("Horror");
        assertThat(entityHashMap1).doesNotContainKey("Dystopia");
        assertThat(entityHashMap1).containsValues(5L);
        HashMap entityHashMap2 = booksJpa.findGenresWithAuthorName("s");
        assertThat(entityHashMap2).containsKeys("Horror","Sci-Fi","Fantasy","Dystopia","Action-Adventure");
        assertThat(entityHashMap2).containsValues(17L,10L,11L,6L);
        HashMap entityHashMap3 = booksJpa.findGenresWithTitle("13245");
        assertThat(entityHashMap3).isEmpty();
    }
    
    /**
     * Tests the method findGenresWithAuthorName in BooksJpaController, which retrieves a hashmaps with the key representing
     * the name of genre and the value is the number of books in that genre, with a search pattern that represents a publisher. 
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindGenresWithPublisher(){
        HashMap entityHashMap1 = booksJpa.findGenresWithPublisher("anchor");
        assertThat(entityHashMap1).containsKey("Horror");
        assertThat(entityHashMap1).doesNotContainKey("Dystopia");
        assertThat(entityHashMap1).containsValues(3L);
        HashMap entityHashMap2 = booksJpa.findGenresWithPublisher("s");
        assertThat(entityHashMap2).containsKeys("Horror","Sci-Fi","Fantasy","Dystopia","Action-Adventure");
        assertThat(entityHashMap2).containsValues(11L,15L);
         HashMap entityHashMap3 = booksJpa.findGenresWithPublisher("xa");
        assertThat(entityHashMap3).isEmpty();
    }
    
    /**
     * Tests the method findGenresWithAuthorName in BooksJpaController, which retrieves a hashmaps with the key representing
     * the name of genre and the value is the number of books in that genre, with a search pattern that represents an isbn. 
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindGenresWithIsbn(){
        HashMap entityHashMap1 = booksJpa.findGenresWithIsbn("197");
        assertThat(entityHashMap1).containsKey("Sci-Fi");
        assertThat(entityHashMap1).doesNotContainKey("Horror");
        assertThat(entityHashMap1).containsValues(1L);
        HashMap entityHashMap2 = booksJpa.findGenresWithIsbn("1");
        assertThat(entityHashMap2).containsKeys("Horror","Sci-Fi","Fantasy","Dystopia","Action-Adventure");
        assertThat(entityHashMap2).containsValues(18L,14L,16L,15L,13L);
         HashMap entityHashMap3 = booksJpa.findGenresWithIsbn("798");
        assertThat(entityHashMap3).isEmpty();
    }
    
    /**
     * Tests the method findAllSixRecommendedBooks in BooksJpaController, which retrieves a list of 6 Books that are
     * recommended based on the information (based on a book) passed as an argument.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindAllSixRecommendedBooks(){
        Genre genre1 = genreJpa.findGenre(1);
        List<Books> recBooks1 = booksJpa.findAllSixRecommendedBooks("Stephen King", genre1, "9785557085373");
        assertThat(recBooks1).hasSize(6);
        List<Books> carrie = booksJpa.findBooksWithTitle("carrie");
        List<Books> shining = booksJpa.findBooksWithTitle("hining");
        List<Books> misery = booksJpa.findBooksWithTitle("Misery");
        assertThat(recBooks1).contains(carrie.get(0));
        assertThat(recBooks1).contains(shining.get(0));
        assertThat(recBooks1).contains(misery.get(0));
        Genre genre2 = genreJpa.findGenre(3);
        List<Books> recBooks2 = booksJpa.findAllSixRecommendedBooks("Aldous Huxley",genre2,"9780099458234");
        assertThat(recBooks2).hasSize(6);
        List<Books> bnw = booksJpa.findBooksWithIsbn("9780307356543");
        assertThat(recBooks2).contains(bnw.get(0));
    }
    
    /**
     * Tests the method findAllBooksInGenreBestSeller in BooksJpaController, which retrieves a list of the 5 best sellers in 
     * a genre and retrieves all the other books of that genre.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindAllBooksInGenreBestSeller(){
        List<Books> horrorBooks = booksJpa.findAllBooksInGenreBestSeller("Horror");
        assertThat(horrorBooks.get(0).getTitle()).isEqualTo("Dracula");
        assertThat(horrorBooks.get(1).getTitle()).isEqualTo("I am Legend");
        assertThat(horrorBooks.get(2).getTitle()).isEqualTo("Frankenstein");
        assertThat(horrorBooks.get(3).getTitle()).isEqualTo("The Terror");
        assertThat(horrorBooks.get(4).getIsbn()).isEqualTo("9786065792296");
        List<Books> scifiBooks = booksJpa.findAllBooksInGenreBestSeller("Sci-Fi");
        assertThat(scifiBooks.get(0).getTitle()).isEqualTo("Recursion");
        assertThat(scifiBooks.get(1).getTitle()).isEqualTo("The Hitchhiker's Guide to the Galaxy");
        assertThat(scifiBooks.get(2).getTitle()).isEqualTo("Akira");
        assertThat(scifiBooks.get(3).getTitle()).isEqualTo("The Humans");
        assertThat(scifiBooks.get(4).getTitle()).isEqualTo("2001:a Space Odyssey");
        List<Books> dystopiaBooks = booksJpa.findAllBooksInGenreBestSeller("Dystopia");
        assertThat(dystopiaBooks.get(0).getTitle()).isEqualTo("Brave New World");
        assertThat(dystopiaBooks.get(1).getTitle()).isEqualTo("Windup Girl");
        assertThat(dystopiaBooks.get(2).getTitle()).isEqualTo("Brave New World Revisited");
        assertThat(dystopiaBooks.get(3).getTitle()).isEqualTo("Progressive Dystopia: Abolition, Antiblackness, and Schooling in San Francisco");
        assertThat(dystopiaBooks.get(4).getTitle()).isEqualTo("Logan's Run");
        List<Books> fantasyBooks = booksJpa.findAllBooksInGenreBestSeller("Fantasy");
        assertThat(fantasyBooks.get(0).getTitle()).isEqualTo("Six of Crows");
        assertThat(fantasyBooks.get(1).getTitle()).isEqualTo("The Way of Kings");
        assertThat(fantasyBooks.get(2).getTitle()).isEqualTo("Daughter of Smoke & Bone");
        assertThat(fantasyBooks.get(3).getTitle()).isEqualTo("Assassin's Apprentice");
        assertThat(fantasyBooks.get(4).getTitle()).isEqualTo("The Wrath & the Dawn");
        List<Books> aaBooks = booksJpa.findAllBooksInGenreBestSeller("Action-Adventure");
        assertThat(aaBooks.get(0).getTitle()).isEqualTo("The Return of the King");
        assertThat(aaBooks.get(1).getTitle()).isEqualTo("The Lost World");
        assertThat(aaBooks.get(2).getTitle()).isEqualTo("The Count of Monte Cristo");
        assertThat(aaBooks.get(3).getTitle()).isEqualTo("Patriot Games");
        assertThat(aaBooks.get(4).getTitle()).isEqualTo("The Hunt for Red October");
    }
    
    /**
     * Tests the method findNumReviewsOfSingleBook in BooksJpaController, which retrieves the number of reviews 
     * written by an user for a book
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindNumReviewsOfSingleBook(){
        Long numReviews1 = booksJpa.findNumReviewsOfSingleBook("9785557085373");
        assertThat(numReviews1).isEqualTo(1);
        Long numReviews2 = booksJpa.findNumReviewsOfSingleBook("9781524759780");
        assertThat(numReviews2).isEqualTo(2);
        Long numReviews3 = booksJpa.findNumReviewsOfSingleBook("9780062748775");
        assertThat(numReviews3).isEqualTo(4);
    }
    
    /**
     * Tests the method findAvgRatingOfSingleBook in BooksJpaController, which retrieves a double, representing
     * the average rating of all the reviews for a single book.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindAvgRatingOfSingleBook(){
        double avgReviews1 = booksJpa.findAvgRatingOfSingleBook("9785557085373");
        assertThat(avgReviews1).isEqualTo(3.0);
        double avgReviews2 = booksJpa.findAvgRatingOfSingleBook("9780765357151");
        assertThat(avgReviews2).isEqualTo(4.0);
        double avgReviews3 = booksJpa.findAvgRatingOfSingleBook("9780062748775");
        assertThat(avgReviews3).isEqualTo(2.75);
    }
    
    /**
     * Tests the method getApprovedReviewsOfBook in BooksJpaController, which retrieves a list of Reviews for
     * a single book based on the isbn passed as an argument.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void GetApprovedReviewsOfBook(){
        List<Reviews> standReviews = booksJpa.getApprovedReviewsOfBook("9785557085373");
        assertThat(standReviews).hasSize(1);
        assertThat(standReviews.get(0).getUserId().getEmail()).isEqualTo("jmccloy2e@usnews.com");
        List<Reviews> futureReviews = booksJpa.getApprovedReviewsOfBook("9780062748775");
        assertThat(futureReviews).hasSize(4);
        assertThat(futureReviews.get(0).getUserId().getEmail()).isEqualTo("cst.send@gmail.com");//32
        assertThat(futureReviews.get(1).getUserId().getEmail()).isEqualTo("yongchao@yongchao.ca");
        assertThat(futureReviews.get(2).getUserId().getEmail()).isEqualTo("hhonniebalf@fotki.com");
        assertThat(futureReviews.get(3).getUserId().getEmail()).isEqualTo("hgavaghanv@ihg.com");
    }
    
    /**
     * Tests the method retrieveAllGenres in BooksJpaController, which retrieves all Genres in the database,
     * which initially is a size of 5.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void RetrieveAllGenres(){
        List<Genre> genres = booksJpa.retrieveAllGenres();
        assertThat(genres).hasSize(5);
    }
    
    /**
     * Tests the method findSingleBookForReview in BooksJpaController, which retrieves a single Books for reviews
     * based on the isbn of a book passed as an argument.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindSingleBookForReview(){
        Books stand = booksJpa.findSingleBookForReview("9785557085373");
        assertThat(stand.getTitle()).isEqualTo("The Stand");
        Books dune = booksJpa.findSingleBookForReview("9780441172719");
        assertThat(dune.getTitle()).isEqualTo("Dune");
        Books armada = booksJpa.findSingleBookForReview("9780804137270");
        assertThat(armada.getTitle()).isEqualTo("Armada");
    }
}
