package com.ga3w20.tests.JpaController;

import com.ga3w20.controllers.AuthorsJpaController;
import com.ga3w20.entities.Authors;
import com.ga3w20.exceptions.RollbackFailureException;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import javax.inject.Inject;
import java.io.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import org.junit.Ignore;
import org.junit.Rule;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Yongchao
 */
@RunWith(Arquillian.class)
public class AuthorsJpaControllerTest {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(AuthorsJpaControllerTest.class);

    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The webArchive is the special packaging of your project
        // so that only the test cases run on the server or embedded
        // container
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(Authors.class.getPackage())
                .addPackage(AuthorsJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addClass(ParameterRule.class)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/test/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createTables.sql")
                .addAsLibraries(dependencies);

        return webArchive;
    }

    /**
     * The ParameterRule class does not support an array of values that are 
     * assigned by the test class constructor as regular parameterized are done.
     * 
     * ParameterRule creates FinanceBean objects initialized through the 
     * FinanceBeani constructor one at a time and then runs the test methods.
     * 
     * In this example I was able to use the FinanceBean but for your tests you 
     * will need to create a special test class that contains the parameters to 
     * test and the result
     */
    @Rule
    public ParameterRule rule = new ParameterRule("dynamic", 
            new Authors(1, "Stephen King"),
            new Authors(2, "Marie Shelly"),
            new Authors(3, "Bram Stoker")
    );

    private Authors dynamic;
    @Inject
    private AuthorsJpaController authorsJpa;


    @Test
    public void FindEntiy() {        
        Authors author = authorsJpa.findAuthors(dynamic.getId());
        LOG.info("authors: ", author.toString());
        assertEquals(author.toString(), dynamic.toString());
    }

    /**
     * Based on the method getAuthorsCount in AuthorsJpaController, it retrieves the number of authors
     * stored in the database.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void GetAuthorsCount(){
        int authorsCount = authorsJpa.getAuthorsCount();
        assertThat(authorsCount).isEqualTo(80);
    }
}
