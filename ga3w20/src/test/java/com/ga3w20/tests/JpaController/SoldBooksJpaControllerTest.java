package com.ga3w20.tests.JpaController;

import com.ga3w20.controllers.SoldBooksJpaController;
import com.ga3w20.entities.SoldBooks;
import com.ga3w20.exceptions.RollbackFailureException;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import javax.inject.Inject;
import java.io.*;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Ignore;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Yongchao
 */
@RunWith(Arquillian.class)
public class SoldBooksJpaControllerTest {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(SoldBooksJpaControllerTest.class);

    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The webArchive is the special packaging of your project
        // so that only the test cases run on the server or embedded
        // container
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(SoldBooks.class.getPackage())
                .addPackage(SoldBooksJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/test/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createTables.sql")
                .addAsLibraries(dependencies);

        return webArchive;
    }

    @Inject
    private SoldBooksJpaController soldBooksJpa;

    @Test
    public void FindAllEntities() {        
        List<SoldBooks> entityList = soldBooksJpa.findSoldBooksEntities();
        LOG.info("Total sold books number: ", entityList.size());
        assertThat(entityList).hasSize(562);
    }

    
    /**
     * Tests the method findSaleOfBookWithUser in SoldBooksJpaController, which retrieves a SoldBooks object, trying
     * to determine if an user ever bought a book.
     * 
     * @author Alexander Berestetskyy
     */
    @Test
    public void FindSalesOfBookWithUser(){
        List<SoldBooks> sb1 = soldBooksJpa.findSaleOfBookWithUser(83, "9780451457998");
        assertThat(sb1.get(0).getBooksIsbn().getTitle()).isEqualTo("2001:a Space Odyssey");
        assertThat(sb1.get(0).getSalesId().getUsersId().getEmail()).isEqualTo("bstobart2a@hugedomains.com");
        List<SoldBooks> sb2 = soldBooksJpa.findSaleOfBookWithUser(1, "9781619760530");
        assertThat(sb2).hasSize(0);
        List<SoldBooks> sb3 = soldBooksJpa.findSaleOfBookWithUser(18, "9781478006688");
        assertThat(sb3.get(0).getBooksIsbn().getTitle()).isEqualTo("Progressive Dystopia: Abolition, Antiblackness, and Schooling in San Francisco");
        assertThat(sb3.get(0).getSalesId().getUsersId().getEmail()).isEqualTo("jpilmerh@altervista.org");
    }
}
