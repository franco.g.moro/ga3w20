package com.ga3w20.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author 1738714
 */
@Entity
@Table(name = "sold_books", catalog = "ga3w20", schema = "")
@NamedQueries({
    @NamedQuery(name = "SoldBooks.findAll", query = "SELECT s FROM SoldBooks s"),
    @NamedQuery(name = "SoldBooks.findById", query = "SELECT s FROM SoldBooks s WHERE s.id = :id"),
    @NamedQuery(name = "SoldBooks.findByPrice", query = "SELECT s FROM SoldBooks s WHERE s.price = :price")})
public class SoldBooks implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "price")
    private BigDecimal price;
    @JoinColumn(name = "books_isbn", referencedColumnName = "isbn")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Books booksIsbn;
    @JoinColumn(name = "sales_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Sales salesId;

    public SoldBooks() {
    }

    public SoldBooks(Integer id) {
        this.id = id;
    }

    public SoldBooks(Integer id, BigDecimal price) {
        this.id = id;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Books getBooksIsbn() {
        return booksIsbn;
    }

    public void setBooksIsbn(Books booksIsbn) {
        this.booksIsbn = booksIsbn;
    }

    public Sales getSalesId() {
        return salesId;
    }

    public void setSalesId(Sales salesId) {
        this.salesId = salesId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SoldBooks)) {
            return false;
        }
        SoldBooks other = (SoldBooks) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com..tomnookbooks.entities.SoldBooks[ id=" + id + " ]";
    }
    
}
