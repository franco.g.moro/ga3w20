package com.ga3w20.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author 1738714 Yongchao
 */
@Entity
@Table(name = "province", catalog = "ga3w20", schema = "")
public class Province implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GST")
    private BigDecimal gst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PST")
    private BigDecimal pst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "QST")
    private BigDecimal qst;
    @Basic(optional = false)
    @NotNull
    @Column(name = "HST")
    private BigDecimal hst;


    public Province() {
    }

    public Province(Integer id) {
        this.id = id;
    }

    public Province(int id, String name, BigDecimal gst, BigDecimal pst, BigDecimal qst, BigDecimal hst) {
        this.id = id;
        this.name = name;
        this.gst = gst;
        this.pst = pst;
        this.qst = qst;
        this.hst = hst;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getGst() {
        return gst;
    }

    public void setGst(BigDecimal gst) {
        this.gst = gst;
    }

    public BigDecimal getPst() {
        return pst;
    }

    public void setPst(BigDecimal pst) {
        this.pst = pst;
    }

    public BigDecimal getQst() {
        return qst;
    }

    public void setQst(BigDecimal qst) {
        this.qst = qst;
    }

    public BigDecimal getHst() {
        return hst;
    }

    public void setHst(BigDecimal hst) {
        this.hst = hst;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Province)) {
            return false;
        }
        Province other = (Province) object;
        if ((this.name == null && other.name != null) || (this.name != null && !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Province Id=" + this.id + 
                ", name=" + this.name;
    }
    
}
