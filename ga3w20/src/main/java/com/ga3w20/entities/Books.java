package com.ga3w20.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author 1738714 modified by Yongchao
 * 
 */
@Entity
@Table(name = "books", catalog = "ga3w20", schema = "")
@NamedQueries({
    @NamedQuery(name = "Books.findAll", query = "SELECT b FROM Books b"),
    @NamedQuery(name = "Books.findByIsbn", query = "SELECT b FROM Books b WHERE b.isbn = :isbn"),
    @NamedQuery(name = "Books.findByTitle", query = "SELECT b FROM Books b WHERE b.title = :title"),
    //@NamedQuery(name = "Books.findByGenre", query = "SELECT b FROM Books b WHERE b.genre = :genre"),
    @NamedQuery(name = "Books.findByDataPublication", query = "SELECT b FROM Books b WHERE b.dataPublication = :dataPublication"),
    @NamedQuery(name = "Books.findByNumberOfPages", query = "SELECT b FROM Books b WHERE b.numberOfPages = :numberOfPages"),
    @NamedQuery(name = "Books.findByDescription", query = "SELECT b FROM Books b WHERE b.description = :description"),
    @NamedQuery(name = "Books.findByImageUrl", query = "SELECT b FROM Books b WHERE b.imageUrl = :imageUrl"),
    @NamedQuery(name = "Books.findByWholesalePrice", query = "SELECT b FROM Books b WHERE b.wholesalePrice = :wholesalePrice"),
    @NamedQuery(name = "Books.findByListPrice", query = "SELECT b FROM Books b WHERE b.listPrice = :listPrice"),
    @NamedQuery(name = "Books.findBySalePrice", query = "SELECT b FROM Books b WHERE b.salePrice = :salePrice"),
    @NamedQuery(name = "Books.findByDateEntered", query = "SELECT b FROM Books b WHERE b.dateEntered = :dateEntered"),
    @NamedQuery(name = "Books.findByRemovalStatus", query = "SELECT b FROM Books b WHERE b.removalStatus = :removalStatus"),
    @NamedQuery(name = "Books.findByDiscount", query = "SELECT b FROM Books b WHERE b.discount = :discount")})
public class Books implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 13)
    @Column(name = "isbn")
    private String isbn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "title")
    private String title;
//    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 100)
//    @Column(name = "genre")
//    private String genre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data_publication")
    @Temporal(TemporalType.DATE)
    private Date dataPublication;
    @Basic(optional = false)
    @NotNull
    @Column(name = "number_of_pages")
    private int numberOfPages;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3000)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    //@NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "image_url")
    private String imageUrl;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "wholesale_price")
    private BigDecimal wholesalePrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "list_price")
    private BigDecimal listPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sale_price")
    private BigDecimal salePrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_entered")
    @Temporal(TemporalType.DATE)
    private Date dateEntered;
    @Basic(optional = false)
    @NotNull
    @Column(name = "removal_status")
    private boolean removalStatus;
    @Basic(optional = false)
    @NotNull
    @Column(name = "discount")
    private long discount;
    @JoinTable(name = "books_format", joinColumns = {
        @JoinColumn(name = "books_isbn", referencedColumnName = "isbn")}, inverseJoinColumns = {
        @JoinColumn(name = "format_id", referencedColumnName = "id")})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Format> formatList;
    
    @JoinTable(name = "books_genre", joinColumns = {
        @JoinColumn(name = "books_isbn", referencedColumnName = "isbn")}, inverseJoinColumns = {
        @JoinColumn(name = "genre_id", referencedColumnName = "id")})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Genre> genreList;
    
    @JoinTable(name = "sold_books",joinColumns={
        @JoinColumn(name="books_isbn",referencedColumnName="isbn")
    })      
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "booksIsbn", fetch = FetchType.LAZY)
    private List<SoldBooks> soldBooksList;
    
    @JoinColumn(name = "author_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Authors authorId;
    @JoinColumn(name = "publisher_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Publishers publisherId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "isbn", fetch = FetchType.LAZY)
    private List<Reviews> reviewsList;
    
    

    public Books() {
    }

    public Books(String isbn) {
        this.isbn = isbn;
    }

    public Books(String isbn, String title, Date dataPublication, int numberOfPages, String description, String imageUrl, BigDecimal wholesalePrice, BigDecimal listPrice, BigDecimal salePrice, Date dateEntered, boolean removalStatus, long discount) {
        this.isbn = isbn;
        this.title = title;
        //this.genre = genre;
        this.dataPublication = dataPublication;
        this.numberOfPages = numberOfPages;
        this.description = description;
        this.imageUrl = imageUrl;
        this.wholesalePrice = wholesalePrice;
        this.listPrice = listPrice;
        this.salePrice = salePrice;
        this.dateEntered = dateEntered;
        this.removalStatus = removalStatus;
        this.discount = discount;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

//    public String getGenre() {
//        return genre;
//    }
//
//    public void setGenre(String genre) {
//        this.genre = genre;
//    }

    public Date getDataPublication() {
        return dataPublication;
    }

    public void setDataPublication(Date dataPublication) {
        this.dataPublication = dataPublication;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public BigDecimal getWholesalePrice() {
        return wholesalePrice;
    }

    public void setWholesalePrice(BigDecimal wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public BigDecimal getListPrice() {
        return listPrice;
    }

    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public Date getDateEntered() {
        return dateEntered;
    }

    public void setDateEntered(Date dateEntered) {
        this.dateEntered = dateEntered;
    }

    public boolean getRemovalStatus() {
        return removalStatus;
    }

    public void setRemovalStatus(boolean removalStatus) {
        this.removalStatus = removalStatus;
    }

    public long getDiscount() {
        return discount;
    }

    public void setDiscount(long discount) {
        this.discount = discount;
    }

    public List<Format> getFormatList() {
        return formatList;
    }

    public void setFormatList(List<Format> formatList) {
        this.formatList = formatList;
    }
    
    public List<Genre> getGenreList() {
        return genreList;
    }

    public void setGenreList(List<Genre> genreList) {
        this.genreList = genreList;
    }

    public List<SoldBooks> getSoldBooksList() {
        return soldBooksList;
    }

    public void setSoldBooksList(List<SoldBooks> soldBooksList) {
        this.soldBooksList = soldBooksList;
    }

    public Authors getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Authors authorId) {
        this.authorId = authorId;
    }

    public Publishers getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Publishers publisherId) {
        this.publisherId = publisherId;
    }

    public List<Reviews> getReviewsList() {
        return reviewsList;
    }

    public void setReviewsList(List<Reviews> reviewsList) {
        this.reviewsList = reviewsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (isbn != null ? isbn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Books)) {
            return false;
        }
        Books other = (Books) object;
        if ((this.isbn == null && other.isbn != null) || (this.isbn != null && !this.isbn.equals(other.isbn))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com..tomnookbooks.entities.Books[ isbn=" + isbn + " ]";
    }
    
}
