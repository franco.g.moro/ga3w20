package com.ga3w20.cookies;

import java.io.Serializable;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class contains methods to read and write cookies Cookies can be read at
 * any time but can only be written to before any HTML is delivered to the
 * browser. For that reason creating cookies is always a preRenderView type
 * event
 *
 * @author Michael Mishin 1612993
 */
@Named
@SessionScoped
public class PreRenderViewBean implements Serializable{
    private final static Logger LOG = LoggerFactory.getLogger(PreRenderViewBean.class);
    
    private final static String COOKIE_NAME = "CookieTest";
    
    private boolean cookiesEnabled = false;

    /**
     * this method checks if cookies are enabled or not by writing a cookie, and trying to read it.
     */
    public void checkCookies() 
    {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, Object> cookieMap = context.getExternalContext().getRequestCookieMap();
        
        writeCookie(); // write a cookie

        // Retrieve a specific cookie
        Object my_cookie = context.getExternalContext().getRequestCookieMap().get(COOKIE_NAME);
        if (my_cookie == null) 
        {
            //LOG.info("cookies are disabled");
            this.cookiesEnabled = false;
        }
        else
        {
            //LOG.info("cookies are enabled");
            this.cookiesEnabled = true;
        }
    }

    /**
     * Writes a test cookie
     */
    private void writeCookie() 
    {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().addResponseCookie(COOKIE_NAME, COOKIE_NAME, null);
    }
    
    /**
     * This method returns the variable that follows the state of the availability of the cookies.
     * In other words, if cookies are enabled in the client side or not.
     * 
     * @return True if cookies are enabled.
     */
    public boolean getCookiesEnabled()
    {
        return this.cookiesEnabled;
    }

}
