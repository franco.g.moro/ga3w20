package com.ga3w20.cookies;

import com.ga3w20.beans.LocaleBean;
import java.io.Serializable;
import java.util.Locale;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Saves the user's language preference in a cookie so the website will load at his desired language.
 * 
 * @author Michael Mishin 1612993
 */
@Named
@SessionScoped
public class LanguagePreferenceCookie implements Serializable{
    private final static Logger LOG = LoggerFactory.getLogger(LanguagePreferenceCookie.class);
    
    private final static String COUNTRY = "country";
    private final static String LANGUAGE = "language";
    
    @Inject
    LocaleBean localeBean;
    
     /**
     * Look for a the locale cookie. if the cookie is found, sets up the locale
     * 
     * @author Michael Mishin
     */
    public void checkCookies() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, Object> cookieMap = context.getExternalContext().getRequestCookieMap();

        // Retrieve all cookies
        if (cookieMap == null || cookieMap.isEmpty()) 
        {
            //LOG.info("No cookies");
        }

        // Retrieve a specific cookie
        Object country = context.getExternalContext().getRequestCookieMap().get(COUNTRY);
        Object language = context.getExternalContext().getRequestCookieMap().get(LANGUAGE);
        //LOG.debug("country: "+country+", language: "+language);
        if (country != null || language != null)
        {
            Locale locale = new Locale(((Cookie) language).getValue(), ((Cookie) country).getValue());
            FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
        }
    }

    /**
     * Writes the local to a cookie
     * 
     * @author Michael Mishin
     */
    public void writeCookie() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().addResponseCookie(COUNTRY, localeBean.getLocale().getCountry(), null);
        context.getExternalContext().addResponseCookie(LANGUAGE, localeBean.getLocale().getLanguage(), null);
    }
}
