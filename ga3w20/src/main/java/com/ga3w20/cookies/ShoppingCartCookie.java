package com.ga3w20.cookies;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class contains methods to read and write the shopping cart cookie.
 *
 * @author Michael Mishin
 */
@Named
@SessionScoped
public class ShoppingCartCookie implements Serializable{
    private final static Logger LOG = LoggerFactory.getLogger(ShoppingCartCookie.class); 
    
    private final static String COOKIE_NAME = "shoppingCart";
    private final static String SEPARATOR = ",";
    
    /**
     * Look for a cookie with the shopping list
     * 
     * @return The shopping list
     * @author Michael Mishin 1612993
     */
    public List<String> checkShoppingList()
    {
        List<String> bookIds = new ArrayList<>();
        FacesContext context = FacesContext.getCurrentInstance();
        
        // Retrieve a specific cookie
        Cookie my_cookie = (Cookie) context.getExternalContext().getRequestCookieMap().get(COOKIE_NAME);
        if (my_cookie != null && !my_cookie.getValue().equalsIgnoreCase("")) {
            LOG.info(my_cookie.getName());
            LOG.info(my_cookie.getValue());
            
            bookIds.addAll(Arrays.asList(((Cookie)my_cookie).getValue().split(SEPARATOR)));
            LOG.debug("Loaded the cookie");
        }
        return bookIds;
    }

    /**
     * adding a new element to the list and adding it to the cookie
     * 
     * @param bookIds
     * @author Michael Mishin 1612993
     */
    public void writeCookie(List<String> bookIds)
    {
        LOG.debug("writing a cookie");
        
        //StringBuilder sb = new StringBuilder();
//        for(int i = 0; i < bookIds.size(); i++)
//        {
//            sb.append(bookIds.get(i));
//            
//            //if it is not the last elemet
//            if((i+1) != bookIds.size())
//            {
//                sb.append(',');
//            }
//        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().addResponseCookie(COOKIE_NAME, String.join(SEPARATOR, bookIds), null);
    }
}
