package com.ga3w20.cookies;

import com.ga3w20.beans.BookBackingBean;
import com.ga3w20.entities.Books;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import org.slf4j.LoggerFactory;

/**
 * This class handles the cookie for the resent page you visited
 *
 * @author Michael Mishin
 */
@Named
@RequestScoped
public class ResentPageCookie {
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(ResentPageCookie.class);
    
    private final static String COOKIE_NAME = "lastISBN";
    
    private boolean visitedBookPage;
    
    @Inject
    private PreRenderViewBean preRenderViewBean;
    
    @Inject
    private BookBackingBean bookBackingBean;
    
    @PostConstruct
    public void init()
    {
        if(preRenderViewBean.getCookiesEnabled())
        {
            FacesContext context = FacesContext.getCurrentInstance();
            Object isbn = context.getExternalContext().getRequestCookieMap().get(COOKIE_NAME);
            
            visitedBookPage = isbn != null;
        }
        else
        {
            visitedBookPage = false;
            LOG.debug("No recommendation cookie was found");
        }
    }
    
    /**
     * This method will write the last page visited to a cookie
     * 
     * @author Michael Mishin
     */
    public void writeLastBookPageVisitedToCookie()
    {
        Books lastBook = bookBackingBean.getSingleBook();
        if(lastBook != null)
        {
            FacesContext context = FacesContext.getCurrentInstance();
            context.getExternalContext().addResponseCookie(COOKIE_NAME, lastBook.getIsbn(), null);
        }
    }
    
    /**
     * This method will return the cookie for the last book page visited
     * 
     * @return the isbn of the book
     * @author Michael Mishin
     */
    public String getLastBookPageIsbn()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        Object isbn = context.getExternalContext().getRequestCookieMap().get(COOKIE_NAME);
        
        return ((Cookie) isbn).getValue();
    }
    
    /**
     * This method will return true if client has visited a book page before.
     * 
     * @return If the client is an old client or not
     * @author Michael Mishin
     */
    public boolean isOldCustomer()
    {
        return visitedBookPage;
    }
}
