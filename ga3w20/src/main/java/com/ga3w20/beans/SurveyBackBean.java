package com.ga3w20.beans;

import com.ga3w20.controllers.AnswersJpaController;
import com.ga3w20.controllers.QuestionsJpaController;
import com.ga3w20.entities.Answers;
import com.ga3w20.entities.Questions;
import com.ga3w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Yongchao
 */
@Named
@SessionScoped
public class SurveyBackBean implements Serializable {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(SurveyBackBean.class);

    private Answers answers;
    private Questions question;
    private String questionText;
    private String answerText;
    private String answerText1;
    private String answerText2;
    private String answerText3;

    public String getAnswerText1() {
        return answerText1;
    }

    public void setAnswerText1(String answerText1) {
        this.answerText1 = answerText1;
    }

    public String getAnswerText2() {
        return answerText2;
    }

    public void setAnswerText2(String answerText2) {
        this.answerText2 = answerText2;
    }

    public String getAnswerText3() {
        return answerText3;
    }

    public void setAnswerText3(String answerText3) {
        this.answerText3 = answerText3;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }
    @Inject
    private AnswersJpaController answersController;
    @Inject
    private QuestionsJpaController questionsController;

    public String editAnswer(int id) {
        this.answers = answersController.findAnswers(id);
        return "manageSurveyEditAnswer";
    }

    public String editQuestion(int id) {
        this.question = questionsController.findQuestions(id);
        return "manageSurveyEditQuestion";
    }

    public String saveAnswer() {

        try {
            answersController.edit(this.answers);
        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
        return "manageSurveys";
    }

    public String saveQuestion() {

        try {
            questionsController.edit(this.question);
        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
        return "manageSurveys";
    }

    public String addQuestion() {

        try {
            Questions q = new Questions();
            q.setIsActive(false);
            q.setQuestionText(this.questionText);
            questionsController.create(q);
        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
        this.questionText = "";
        return "manageSurveys";
    }

    public String deleteQuestion(int id) {

        try {
            questionsController.destroy(id);
        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }

        return "manageSurveys";
    }

    public String addAnswerToQuestion() {

        try {
            Answers newAnswer = new Answers();
            newAnswer.setAnswerText(this.answerText);
            newAnswer.setQuestionId(this.question);
            newAnswer.setChosenTimes(0);
            this.answersController.create(newAnswer);
            if (!this.answerText1.equals("")) {
                newAnswer = new Answers();
                newAnswer.setAnswerText(this.answerText1);
                newAnswer.setQuestionId(this.question);
                newAnswer.setChosenTimes(0);
                this.answersController.create(newAnswer);
            }
            if (!this.answerText2.equals("")) {
                newAnswer = new Answers();
                newAnswer.setAnswerText(this.answerText2);
                newAnswer.setQuestionId(this.question);
                newAnswer.setChosenTimes(0);
                this.answersController.create(newAnswer);
            }
            if (!this.answerText3.equals("")) {
                newAnswer = new Answers();
                newAnswer.setAnswerText(this.answerText3);
                newAnswer.setQuestionId(this.question);
                newAnswer.setChosenTimes(0);
                this.answersController.create(newAnswer);
            }
        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
        this.answerText = "";
        this.answerText1 = "";
        this.answerText2 = "";
        this.answerText3 = "";
        return "manageSurveys";
    }

    public String deleteAnswer(int answerId) {
        try {
            answersController.destroy(answerId);
        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
        return "manageSurveys";
    }

    public void setAnswers(Answers answers) {
        this.answers = answers;
    }

    public Answers getAnswers() {
        return this.answers;
    }

    public Questions getQuestion() {
        return question;
    }

    public void setQuestion(Questions question) {
        this.question = question;
    }

}
