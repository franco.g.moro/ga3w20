package com.ga3w20.beans;

import com.ga3w20.controllers.AdvertismentsJpaController;
import com.ga3w20.entities.Advertisments;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.LoggerFactory;

/**
 * This bean handles the advertisement that will show in the front page
 *
 * @author Michael Mishin 1612993
 */
@Named
@RequestScoped
public class AdvertisementBean implements Serializable{
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(AdvertisementBean.class);
    
    private Advertisments currentAdvertisement;
    private boolean anyActiveAdvertisement;
    
    @Inject
    private AdvertismentsJpaController advertisementsJpaController;
    
    /**
     * Constructor that will get the first active url link for the news feed
     * 
     * @author Michael Mishin 1612993
     */
    @PostConstruct
    public void init()
    {
        anyActiveAdvertisement = true;
        
        // gets the first active advertisement found
        currentAdvertisement = advertisementsJpaController.findAdvertismentsEntities().stream()
                .filter(advertisement -> advertisement.getIsActive())
                .findFirst()
                .get();
        if(currentAdvertisement == null)
        {
            LOG.debug("No active advertisements");
            anyActiveAdvertisement = false;
        }
    }
    
    /**
     * This method will return the advertisement image
     * 
     * @return The Image URL
     * @author Michael Mishin
     */
    public String getCurrentAdvertisementImage()
    {
        return currentAdvertisement.getImageUrl();
    }
    
    /**
     * This method will return the advertisement link
     * 
     * @return The link to redirect
     * @author Michael Mishin
     */
    public String getCurrentAdvertisementURL()
    {
        return currentAdvertisement.getLink();
    }
    
    /**
     * This method will check if the advertisement should be displayed or not.
     * The reason will be due to non-active advertisements.
     * 
     * @return True for displaying the advertisement. False to not render the view
     * @author Michael Mishin 1612993
     */
    public boolean isAdvertisementActive()
    {
        return anyActiveAdvertisement;
    }
}
