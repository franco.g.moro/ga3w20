package com.ga3w20.beans;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * A bean use to navigate between pages in faces-config.xml
 * 
 * @author Hadil Elhashani
 */
@Named
@RequestScoped
public class NavigationBean {
    
    /**
     * Returns the string of form-outcome in navigation-rules
     * and redirects to the specified page
     * 
     * @return 
     */
    public String redirectToManagement()
    {
        return "toManagement";
    }
    
    /**
     * Returns the string of form-outcome in navigation-rules
     * and redirects to the specified page
     * 
     * @return 
     */
    public String redirectToManageBannerAds()
    {
        return "toManageBannerAds";
    }
    
    /**
     * Returns the string of form-outcome in navigation-rules
     * and redirects to the specified page
     * 
     * @return 
     */
    public String redirectToManageClients()
    {
        return "toManageClients";
    }
    
    /**
     * Returns the string of form-outcome in navigation-rules
     * and redirects to the specified page
     * 
     * @return 
     */
    public String redirectToManageNewsFeed()
    {
        return "toManageNewsFeed";
    }
    
    /**
     * Returns the string of form-outcome in navigation-rules
     * and redirects to the specified page
     * 
     * @return 
     */
    public String redirectToManageOrders()
    {
        return "toManageOrders";
    }
    
    /**
     * Returns the string of form-outcome in navigation-rules
     * and redirects to the specified page
     * 
     * @return 
     */
    public String redirectToManageReports()
    {
        return "toManageReports";
    }
    
    /**
     * Returns the string of form-outcome in navigation-rules
     * and redirects to the specified page
     * 
     * @return 
     */
    public String redirectToManageReviews()
    {
        return "toManageReviews";
    }
    
    /**
     * Returns the string of form-outcome in navigation-rules
     * and redirects to the specified page
     * 
     * @return 
     */
    public String redirectToManageSales()
    {
        return "toManageSales";
    }
    
    /**
     * Returns the string of form-outcome in navigation-rules
     * and redirects to the specified page
     * 
     * @return 
     */
    public String redirectToManageSurveys()
    {
        return "toManageSurveys";
    }
}
