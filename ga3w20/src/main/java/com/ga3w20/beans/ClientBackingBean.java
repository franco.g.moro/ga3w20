package com.ga3w20.beans;

import com.ga3w20.controllers.ProvinceJpaController;
import com.ga3w20.controllers.UsersJpaController;
import com.ga3w20.entities.Province;
import com.ga3w20.entities.Users;
import com.ga3w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.RowEditEvent;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Yongchao
 */
@Named
@SessionScoped
public class ClientBackingBean implements Serializable {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(ClientBackingBean.class);
    private List<Users> usersList;

    private Integer id;
    private String title;
    private String lastName;
    private String firstName;
    private String companyName;
    private String address1;
    private String address2;
    private String city;
    private String provinceName;
    private String postalCode;
    private String homeTelephoe;
    private String cellphone;
    private String email;
    private String isManager;
    private String searchBy;
    private String searchClient;
    private List<String> isManagerList;
    private List<String> provinceList;
    private List<String> searchByList;

    @Inject
    UsersJpaController usersController;
    @Inject
    ProvinceJpaController provinceController;

    public ClientBackingBean() {
        isManagerList = new ArrayList<String>();
        isManagerList.add("true");
        isManagerList.add("false");
        searchByList = new ArrayList<String>();
        searchByList.add("Id");
        searchByList.add("First name");
        searchByList.add("Last name");
        searchByList.add("Company");
        searchByList.add("City");
        searchByList.add("Province");
        searchByList.add("Postal code");
        searchByList.add("Home telephone");
        searchByList.add("Cellphone");
        searchByList.add("Email");

    }

    @PostConstruct
    private void init() {
        usersList = usersController.findUsersEntities();
        List<Province> provinces = new ArrayList<Province>();
        provinces = provinceController.findProvinceEntities();

        provinceList = new ArrayList<String>();

        for (Province p : provinces) {
            provinceList.add(p.getName());
        }
    }

    public List<Users> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<Users> usersList) {
        this.usersList = usersList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getHomeTelephoe() {
        return homeTelephoe;
    }

    public void setHomeTelephoe(String homeTelephoe) {
        this.homeTelephoe = homeTelephoe;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIsManager() {
        return isManager;
    }

    public void setIsManager(String isManager) {
        this.isManager = isManager;
    }

    public List<String> getIsManagerList() {
        return this.isManagerList;
    }

    public List<String> getProvinceList() {
        return this.provinceList;
    }

    public List<String> getSearchByList() {
        return this.searchByList;
    }

    public String getSearchClient() {
        return searchClient;
    }

    public void setSearchClient(String searchClient) {
        this.searchClient = searchClient;
    }

    public String getSearchBy() {
        return searchBy;
    }

    public void setSearchBy(String searchBy) {
        this.searchBy = searchBy;
    }

    public void findUsersById() {

        switch (searchBy) {
            case "Id":
                try {
                
                    int i = Integer.parseInt(searchClient.trim());  
                    if (i > 0) {
                        usersList.clear();
                        usersList.add(usersController.findUsers(i));
                    } else {
                        usersList.clear();
                        usersList = usersController.findUsersEntities();
                    }
                
                } catch (NumberFormatException nfe) {
                    usersList.clear();
                    usersList = usersController.findUsersEntities();
                }
            
                break;
            case "First name":
                if (searchClient != null && searchClient.length() > 0) {
                    usersList.clear();
                    usersList = usersController.findUsersByFirstName(searchClient);
                } else {
                    usersList.clear();
                    usersList = usersController.findUsersEntities();
                }                
                break;
            case "Last name":
                if (searchClient != null && searchClient.length() > 0) {
                    usersList.clear();
                    usersList = usersController.findUsersByLastName(searchClient);
                } else {
                    usersList.clear();
                    usersList = usersController.findUsersEntities();
                }                
                break;  
            case "Company":
                if (searchClient != null && searchClient.length() > 0) {
                    usersList.clear();
                    usersList = usersController.findUsersByCompany(searchClient);
                } else {
                    usersList.clear();
                    usersList = usersController.findUsersEntities();
                }                
                break;
            case "City":
                if (searchClient != null && searchClient.length() > 0) {
                    usersList.clear();
                    usersList = usersController.findUsersByCity(searchClient);
                } else {
                    usersList.clear();
                    usersList = usersController.findUsersEntities();
                }                
                break;
            case "Province":
                if (searchClient != null && searchClient.length() > 0) {
                    usersList.clear();
                    usersList = usersController.findUsersByProvince(searchClient);
                } else {
                    usersList.clear();
                    usersList = usersController.findUsersEntities();
                }                
                break;
            case "Postal code":
                if (searchClient != null && searchClient.length() > 0) {
                    usersList.clear();
                    usersList = usersController.findUsersByPostalCode(searchClient);
                } else {
                    usersList.clear();
                    usersList = usersController.findUsersEntities();
                }                
                break;
            case "Home telephone":
                if (searchClient != null && searchClient.length() > 0) {
                    usersList.clear();
                    usersList = usersController.findUsersByHomeTelephone(searchClient);
                } else {
                    usersList.clear();
                    usersList = usersController.findUsersEntities();
                }                
                break;
            case "Cellphone":
                if (searchClient != null && searchClient.length() > 0) {
                    usersList.clear();
                    usersList = usersController.findUsersByCellphone(searchClient);
                } else {
                    usersList.clear();
                    usersList = usersController.findUsersEntities();
                }                
                break;
            case "Email":
                if (searchClient != null && searchClient.length() > 0) {
                    usersList.clear();
                    usersList = usersController.findUsersByEmail(searchClient);
                } else {
                    usersList.clear();
                    usersList = usersController.findUsersEntities();
                }                
                break;
        }

        //this.searchClient = null;
    }
    
    
    public void onRowEdit(RowEditEvent event) {

        FacesMessage msg = new FacesMessage("Client Edited. ");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        try {
            usersController.edit((Users) event.getObject());
        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

//    public String asManager(int userId) {
//        Users user = usersController.findUsers(userId);
//        
//        if (user.getIsManager().equals("false")) {
//            user.setIsManager("true");
//        } else {
//            user.setIsManager("false");
//        }
//        try {
//            usersController.edit(user);
//        } catch (RollbackFailureException ex) {
//            LOG.error(ex.getMessage());
//        } catch (Exception ex) {
//            LOG.error(ex.getMessage());
//        }
//        return "manageClients";
//    }
    public String addNewsFeed() {

//        try {
//            newsFeedController.create(n);
//        } catch (RollbackFailureException ex) {
//            LOG.error(ex.getMessage());
//        } catch (Exception ex) {
//            LOG.error(ex.getMessage());
//        }
        init();
        return "manageClients";
    }

    public String deleteNewsFeed(int newsFeedId) {
//        try {
//            newsFeedController.destroy(newsFeedId);
//        } catch (RollbackFailureException ex) {
//            LOG.error(ex.getMessage());
//        } catch (Exception ex) {
//            LOG.error(ex.getMessage());
//        }
        init();
        return "manageClients";
    }

}
