package com.ga3w20.beans;

import com.ga3w20.controllers.BooksJpaController;
import com.ga3w20.cookies.PreRenderViewBean;
import com.ga3w20.cookies.ResentPageCookie;
import com.ga3w20.entities.Books;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.LoggerFactory;

/**
 * This bean will handle the store offers including new books to show and books on sale.
 *
 * @author Michael Mishin
 */
@Named
@RequestScoped
public class FrontDoorOffersBean implements Serializable {
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(SurveyBackBean.class);
    
    private List<Books> booksOnSale;
    private List<Books> newBooks;
    private List<Books> recommendedBooks;
    
    @Inject
    private BooksJpaController booksJpaController;
    
    @Inject
    private ResentPageCookie resentPageCookie;
    
    @Inject
    private PreRenderViewBean preRenderViewBean;
    
    /**
     * This constructor collects the list of books needed to be displayed in the front door
     * 
     * @author Michael Mishin 
     */
    @PostConstruct
    public void init()
    {
        // checks if the last book visited still exists, if cookies are enabled, and that the client is an old customer
        if(preRenderViewBean.getCookiesEnabled() && resentPageCookie.isOldCustomer() && !booksJpaController.findBooksWithIsbn(resentPageCookie.getLastBookPageIsbn()).isEmpty())
        {
            Books lastBookVisited = (Books) booksJpaController.findBooksWithIsbn(resentPageCookie.getLastBookPageIsbn()).get(0);
            recommendedBooks = booksJpaController.findAllSixRecommendedBooks(lastBookVisited.getAuthorId().getName(), lastBookVisited.getGenreList().get(0), lastBookVisited.getIsbn());
            LOG.debug("found cookie for recommendation");
        }
        
        booksOnSale = booksJpaController.findRecentBooksOnSale();
        newBooks = booksJpaController.findRecentBooks();
    }
    
    /**
     * This method will return the list for the books that are on sale
     * 
     * @return list of books on sale
     * @author Michael Mishin
     */
    public List<Books> getBooksOnSale()
    {
        return booksOnSale;
    }
    
    /**
     * This method will return the list for new books (books that were added recently)
     * 
     * @return List of new books.
     * @author Michael Mishin
     */
    public List<Books> getNewBooks()
    {
        return newBooks;
    }
    
    /**
     * returns a list of recommended books based on the last book page visited
     * 
     * @return List of recommended books
     * @author Michael Mishin
     */
    public List<Books> getRecommendedBooks()
    {
        return recommendedBooks;
    }
}
