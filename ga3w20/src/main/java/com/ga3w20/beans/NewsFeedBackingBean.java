package com.ga3w20.beans;

import com.ga3w20.controllers.NewsFeedJpaController;
import com.ga3w20.entities.NewsFeed;
import com.ga3w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.RowEditEvent;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Yongchao
 */
@Named
@SessionScoped
public class NewsFeedBackingBean implements Serializable {

    
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(NewsFeedBackingBean.class);
    private List<NewsFeed> newsFeedList;
    
    private String link;

    
    @Inject
    NewsFeedJpaController newsFeedController;
    
    @PostConstruct
    private void init() {
        newsFeedList = newsFeedController.findNewsFeedEntities();
    }

    public List<NewsFeed> getNewsFeedList() {
        return newsFeedList;
    }

    public void setNewsFeedList(List<NewsFeed> newsFeedList) {
        this.newsFeedList = newsFeedList;
    }

    public void onRowEdit(RowEditEvent event) {
        
        FacesMessage msg = new FacesMessage("News feed Edited. ");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        try {
            newsFeedController.edit((NewsFeed)event.getObject());
        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }    

    

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    
    public String addNewsFeed() {
        NewsFeed n = new NewsFeed();
        
        n.setLink(this.link);
        n.setIsActive(false);
        
        this.link = "";
        try {
            newsFeedController.create(n);
        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
        init();
        return "manageNewsFeed";
    }
    
    public String deleteNewsFeed(int newsFeedId) {
        try {
            newsFeedController.destroy(newsFeedId);
        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
        init();
        return "manageNewsFeed";
    }
    
    public void activateNewsFeed(Integer id) {
        this.newsFeedController.activateNewsFeed(id);
        this.newsFeedList = this.newsFeedController.findNewsFeedEntities();
    }
    
}
