package com.ga3w20.beans;

/**
 * This class is used for testing report methods
 * 
 * @author Hadil Elhashani 
 * @param <T>
 * @param <U>
 * @param <X>
 * @param <Y>
 */
public class Quartet<T, U, X, Y>  {
    
    private T value0;
    private U value1;
    private X value2;
    private Y value3;

    public Quartet(T value0, U value1, X value2, Y value3) {
        this.value0 = value0;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
    }

    public T getValue0() {
        return value0;
    }

    public void setValue0(T value0) {
        this.value0 = value0;
    }

    public U getValue1() {
        return value1;
    }

    public void setValue1(U value1) {
        this.value1 = value1;
    }

    public X getValue2() {
        return value2;
    }

    public void setValue2(X value2) {
        this.value2 = value2;
    }

    public Y getValue3() {
        return value3;
    }

    public void setValue3(Y value3) {
        this.value3 = value3;
    }
    
}
