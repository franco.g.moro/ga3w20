package com.ga3w20.beans;

import com.ga3w20.controllers.AuthorsJpaController;
import com.ga3w20.controllers.BooksJpaController;
import com.ga3w20.controllers.FormatJpaController;
import com.ga3w20.controllers.GenreJpaController;
import com.ga3w20.controllers.PublishersJpaController;
import com.ga3w20.entities.Authors;
import com.ga3w20.entities.Books;
import com.ga3w20.entities.Format;
import com.ga3w20.entities.Genre;
import com.ga3w20.entities.Publishers;
import com.ga3w20.exceptions.RollbackFailureException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.primefaces.event.RowEditEvent;
import org.slf4j.LoggerFactory;

/**
 * InventoryBackingBean class used to mange inventory adding new books, or
 * editing info of a book
 *
 * @author Hadil Elhashani
 */
@Named
@SessionScoped
public class InventoryBackingBean implements Serializable {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(InventoryBackingBean.class);

    @Inject
    private BooksJpaController bookController;

    @Inject
    private AuthorsJpaController authorController;

    @Inject
    private PublishersJpaController publisherController;

    @Inject
    private GenreJpaController genreController;

    @Inject
    private FormatJpaController formatController;

    @NotNull
    @Size(min = 1, max = 13)
    private String isbn;
    @NotNull
    @Size(min = 1, max = 200)
    private String title;
    @NotNull
    private String dataPublication;
    @NotNull
    @Size(min = 1, max = 100000)
    private String numberOfPages;
    @NotNull
    @Size(min = 1, max = 3000)
    private String description;
    @NotNull
    @Size(min = 1, max = 1000)
    private String imageUrl;
    @NotNull
    @Size(min = 1, max = 100000)
    private String wholesalePrice;
    @NotNull
    @Size(min = 1, max = 100000)
    private String listPrice;
    @NotNull
    @Size(min = 1, max = 100000)
    private String salePrice;
    @Size(min = 1, max = 100)
    private String genreName;
    private Genre genre;
    private String dateEntered;
    private String removalStatus;
    private String discount;
    private List<Format> formatList;
    private List<Genre> genreList;
    private List<String> genreNames;
    private String authorName;
    private String publisherName;
    private Authors authorId;
    private Publishers publisherId;
    private List<Format> formats;

    private Part imageFile;

    private List<Books> allBooks;
    private List<Books> filteredBookList;

    public InventoryBackingBean() {
    }

    @PostConstruct
    public void init() {
        List<Genre> genres = genreController.findGenreEntities();
        this.setGenreList(new ArrayList<>());
        genres.forEach(g -> this.genreList.add(g));
        this.setGenreNames(new ArrayList<>());
        genres.forEach(g -> this.genreNames.add(g.getGenre()));
        setAllBooks(bookController.findBooksEntities());
    }

    /**
     * Validates and converts attributes of book object then adds it to the
     * inventory
     *
     * @author Hadil Elhashani
     * @return
     */
    public String addBookToInventory() {
        LOG.debug("addBookToInventory");
        //Setting the publication and enetred dates
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date pubDate = new Date();
        Date enteredDate = new Date();

        LOG.debug("this.dataPublication = " + this.dataPublication);
        Calendar cal = Calendar.getInstance();
        enteredDate = cal.getTime();
        try {
            pubDate = df.parse(this.dataPublication);
        } catch (ParseException ex) {
            Logger.getLogger(InventoryBackingBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.setAuthorId(setBookAuthor(this.authorName));
        this.setPublisherId(setBookPublisher(this.publisherName));

        setGenreByName();

        LOG.debug("genreName = " + this.genreName);
        LOG.debug("this.genre.getGenre() = " + this.genre.getGenre());

        if (this.imageUrl == null) {
            this.imageUrl = "noImage.png";
        }

        Books book = new Books(this.isbn, this.title, pubDate,
                Integer.parseInt(this.numberOfPages), this.description, this.imageUrl, new BigDecimal(this.wholesalePrice),
                new BigDecimal(this.listPrice), new BigDecimal(this.listPrice), enteredDate, false, 0);

        List<Genre> bookGenre = new ArrayList<>();
        bookGenre.add(this.genre);

        book.setGenreList(bookGenre);
        book.setAuthorId(this.authorId);
        book.setPublisherId(this.publisherId);
        book.setFormatList(this.formats);

        try {
            bookController.create(book);

        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }

        return "management.xhtml?faces-redirect=true";
    }

    /**
     * Creates an object of type author by calling the jpa controller create
     * method
     *
     * @author Hadil Elhashani
     * @param name
     * @return
     */
    public Authors setBookAuthor(String name) {
        LOG.debug("setBookAuthor");
        Authors author = null;
        List<Authors> authors = authorController.findAuthorsEntities();
        for (Authors a : authors) {
            if (name.trim().equalsIgnoreCase(a.getName())) {
                author = a;
            }
        }

        if (author == null) {
            author = new Authors();
            author.setName(name);
            try {
                authorController.create(author);
            } catch (RollbackFailureException ex) {
                LOG.error(ex.getMessage());
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
        return author;
    }

    /**
     * Creates an object of type publisher by calling the jpa controller create
     * method
     *
     * @author Hadil Elhashani
     * @param name
     * @return
     */
    public Publishers setBookPublisher(String name) {
        LOG.debug("setBookPublisher");
        Publishers publisher = null;
        List<Publishers> publishers = publisherController.findPublishersEntities();

        for (Publishers p : publishers) {
            if (name.trim().equalsIgnoreCase(p.getName())) {
                publisher = p;
            }
        }

        if (publisher == null) {
            publisher = new Publishers();
            publisher.setName(name);
            try {
                publisherController.create(publisher);
            } catch (RollbackFailureException ex) {
                LOG.error(ex.getMessage());
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
        return publisher;
    }

    /**
     * An event listener used to save the image file to the disk and set the
     * image url of the book
     *
     * @author Hadil Elhashani
     */
    public void saveImageFile() {
        LOG.debug("saveImageFile");
        try ( InputStream input = imageFile.getInputStream()) {

            this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();

            //get the path of this class and appends the folder names to where the images are stored
            String currentPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
            String path = currentPath.substring(0, currentPath.indexOf("target/")) + "src/main/webapp/resources/images/coverImages/";

            String submittedFileName = imageFile.getSubmittedFileName();
            this.setImageUrl(submittedFileName);

            Files.copy(input, new File(path, submittedFileName).toPath());

            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "image added "));

        } catch (IOException e) {
            LOG.debug(e.getMessage());
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "image not added"));
        }
    }

    /**
     *
     * @param event
     */
    public void onRowEdit(RowEditEvent event) {
        LOG.debug("onRowEdit");
        try {
            Books b = (Books) event.getObject();
            LOG.debug("onRowEdit b.title = " + b.getTitle());
            LOG.debug("onRowEdit b.author = " + b.getAuthorId().getName());
            //Creates new publisher/author if names changed 
            b.setAuthorId(this.setBookAuthor(b.getAuthorId().getName()));
            b.setPublisherId(this.setBookPublisher(b.getPublisherId().getName()));
            bookController.edit((Books) event.getObject());
        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    /**
     *
     * @param event
     */
    public void onRowCancel(RowEditEvent event) {
        LOG.debug("onRowCancel");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Edit cancelled"));
    }

    /**
     *
     * @param context
     * @param component
     * @param value
     */
    public void validateWholeAndListPrice(FacesContext context, UIComponent component, Object value) {
        LOG.debug("validateWholeAndListPrice");
        String lprice = (String) value;

        UIInput wpriceInput = (UIInput) component.findComponent("wprice");
        String wprice = (String) wpriceInput.getLocalValue();

        try {
            BigDecimal wpricebd = new BigDecimal(wprice);
            BigDecimal lpricebd = new BigDecimal(lprice);

            if (wpricebd.compareTo(lpricebd) >= 0) {
                String message = context.getApplication().evaluateExpressionGet(context, "#{msgs['listWholePriceValidate']}", String.class);
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", message);
                throw new ValidatorException(msg);
            }
        } catch (java.lang.NumberFormatException e) {
            String message = context.getApplication().evaluateExpressionGet(context, "#{msgs['numberformat']}", String.class);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", message);
            throw new ValidatorException(msg);
        }

    }

    /**
     *
     * @param context
     * @param component
     * @param value
     */
    public void validateWholeAndListPriceDecimal(FacesContext context, UIComponent component, Object value) {
        LOG.debug("validateWholeAndListPriceDecimal");
        //String lprice = (String) value;

        UIInput wpriceInput = (UIInput) component.findComponent("wprice");
        //String wprice = (String) wpriceInput.getLocalValue();
        BigDecimal wpricebd = (BigDecimal) wpriceInput.getLocalValue();;
        BigDecimal lpricebd = (BigDecimal) value;

        if (wpricebd.compareTo(lpricebd) >= 0) {
            String message = context.getApplication().evaluateExpressionGet(context, "#{msgs['listWholePriceValidate']}", String.class);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message);
            throw new ValidatorException(msg);
        }
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDataPublication() {
        return dataPublication;
    }

    public void setDataPublication(String dataPublication) {
        this.dataPublication = dataPublication;
    }

    public String getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(String numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getWholesalePrice() {
        return wholesalePrice;
    }

    public void setWholesalePrice(String wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public String getListPrice() {
        return listPrice;
    }

    public void setListPrice(String listPrice) {
        this.listPrice = listPrice;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getDateEntered() {
        return dateEntered;
    }

    public void setDateEntered(String dateEntered) {
        this.dateEntered = dateEntered;
    }

    public String isRemovalStatus() {
        return removalStatus;
    }

    public void setRemovalStatus(String removalStatus) {
        this.removalStatus = removalStatus;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public List<Format> getFormatList() {
        return formatList;
    }

    public void setFormatList(List<Format> formatList) {
        this.formatList = formatList;
    }

    public List<Genre> getGenreList() {
        return genreList;
    }

    public void setGenreList(List<Genre> genreList) {
        this.genreList = genreList;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public Authors getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Authors authorId) {
        this.authorId = authorId;
    }

    public Publishers getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Publishers publisherId) {
        this.publisherId = publisherId;
    }

    public Part getImageFile() {
        return imageFile;
    }

    public void setImageFile(Part imageFile) {
        this.imageFile = imageFile;
    }

    public List<String> getGenreNames() {
        return genreNames;
    }

    public void setGenreNames(List<String> genreNames) {
        this.genreNames = genreNames;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public List<Books> getAllBooks() {
        //setAllBooks(bookController.findBooksEntities());
        return allBooks;
    }

    public void setAllBooks(List<Books> allBooks) {
        this.allBooks = allBooks;
    }

    /**
     *
     * @return
     */
    public List<Books> getFilteredBookList() {
        return filteredBookList;
    }

    /**
     *
     * @param filteredBookList
     */
    public void setFilteredBookList(List<Books> filteredBookList) {
        this.filteredBookList = filteredBookList;
    }

    /**
     *
     * @return
     */
    public List<Format> getFormats() {
        this.setFormats(this.formatController.findFormatEntities());
        return formats;
    }

    /**
     *
     * @param formats
     */
    public void setFormats(List<Format> formats) {
        this.formats = formats;
    }

    /**
     * Sets the book's genre depending on the genre name
     *
     */
    public void setGenreByName() {
        LOG.debug("setGenreByName()");
        List<Genre> list = genreController.findGenreEntities();
        list.forEach(g -> LOG.debug(g.getGenre()));
        for (Genre g : list) {
            if (this.genreName.equals(g.getGenre())) {
                this.setGenre(g);
            }
        }
    }
}
