package com.ga3w20.beans;

import com.ga3w20.controllers.AnswersJpaController;
import com.ga3w20.controllers.QuestionsJpaController;
import com.ga3w20.entities.Answers;
import com.ga3w20.entities.Questions;
import com.ga3w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.axes.cartesian.CartesianScales;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearAxes;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearTicks;
import org.primefaces.model.charts.bar.BarChartOptions;
import org.primefaces.model.charts.hbar.HorizontalBarChartDataSet;
import org.primefaces.model.charts.hbar.HorizontalBarChartModel;
import org.primefaces.model.charts.optionconfig.title.Title;
import org.slf4j.LoggerFactory;

/**
 * This class will get a question from the db and will display the answers for the question after the question has been answered
 * 
 * @author Michael Mishin 1612993
 */
@Named
@SessionScoped
public class SurveyBean implements Serializable {
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(SurveyBackBean.class);
    
    private boolean renderSurvey = true;
    private boolean renderResult = false;
    
    private Questions currentQuestion;
    private Integer resultId;
    
    private HorizontalBarChartModel hbarModel;
    private HorizontalBarChartDataSet hbarDataSet;
    private ChartData graphData;
    
    @Inject
    private AnswersJpaController answersController;
    @Inject
    private QuestionsJpaController questionsController;
    
    /**
     * post construct constructor.
     * 
     * Every session chooses a random question.
     * @author Michael Mishin 1612993
     */
    @PostConstruct
    public void init()
    {
        currentQuestion = questionsController.getActiveQuestion();
        
        // creating the graph options and look
        hbarModel = new HorizontalBarChartModel();
        graphData = new ChartData();
        hbarDataSet = new HorizontalBarChartDataSet();
        
        hbarDataSet.setLabel(currentQuestion.getQuestionText());

        // colors
        List<String> bgColor = new ArrayList<>();
        bgColor.add("rgba(255, 159, 64, 0.2)");
        bgColor.add("rgba(255, 205, 86, 0.2)");
        bgColor.add("rgba(54, 162, 235, 0.2)");
        bgColor.add("rgba(153, 102, 255, 0.2)");
        hbarDataSet.setBackgroundColor(bgColor);

        List<String> borderColor = new ArrayList<>();
        borderColor.add("rgb(255, 159, 64)");
        borderColor.add("rgb(255, 205, 86)");
        borderColor.add("rgb(54, 162, 235)");
        borderColor.add("rgb(153, 102, 255)");
        hbarDataSet.setBorderColor(borderColor);
        hbarDataSet.setBorderWidth(1);

        graphData.addChartDataSet(hbarDataSet);

        //Options
        BarChartOptions options = new BarChartOptions();
        CartesianScales cScales = new CartesianScales();
        CartesianLinearAxes linearAxes = new CartesianLinearAxes();
        CartesianLinearTicks ticks = new CartesianLinearTicks();
        ticks.setBeginAtZero(true);
        linearAxes.setTicks(ticks);
        cScales.addXAxesData(linearAxes);
        options.setScales(cScales);

        Title title = new Title();
        title.setDisplay(false);
        title.setText(currentQuestion.getQuestionText());
        options.setTitle(title);

        hbarModel.setOptions(options);
        LOG.debug("graph options created");
    }
    
    /**
     * This method will save the id for the selected answer.
     * Will increment the value of the answer in the DB
     * 
     * @param resultId The id of the answer to increment
     * @author Michael Mishin 1612993
     */
    public void setResultId(Integer resultId)
    {
        //updates the DB with the answer
        Answers chosenAnswer = answersController.findAnswers(resultId);
        chosenAnswer.setChosenTimes(chosenAnswer.getChosenTimes()+1);
        
        try {
            answersController.edit(chosenAnswer);
            LOG.debug("answer was updated successfuly");
        } catch (RollbackFailureException ex) {
            LOG.error("rollaback", ex);
        } catch (Exception ex) {
            LOG.error("Exception", ex);
        }
        
        this.resultId = resultId; // saves the last answer Id
    }
    
    /**
     * This method will get the chosen answer id.
     * 
     * @return the id of the answer
     * @author Michael Mishin 1612993
     */
    public Integer getResultId()
    {
        
        return resultId;
    }
    
    /**
     * This method will get the chosen answer's text.
     * 
     * @return the text for the answer
     * @author Michael Mishin 1612993
     */
    public String getResultText()
    {
        LOG.debug("getting the answer text");
        return answersController.findAnswers(resultId).getAnswerText();
    }
    
    /**
     * This method will get the question text for the randomly chosen question.
     * 
     * @return The question text
     * @author Michael Mishin 1612993
     */
    public String getQuestionText()
    {
        LOG.debug("getting the question text");
        return currentQuestion.getQuestionText();
    }
    
    /**
     * This method will get all the answers for the current question.
     * 
     * @return 
     */
    public List<Answers> getAnswers()
    {
        LOG.debug("getting the Answers list for the current question");
        currentQuestion = questionsController.getActiveQuestion(); // gets the updated object
        return currentQuestion.getAnswersList();
    }
    
    /**
     * This method will check if the person has answered the survey.
     * If the person has answered the question, then the survey should not be rendered.
     * 
     * @return If the survey should be rendered
     * @author Michael Mishin 1612993
     */
    public boolean getRenderSurvey()
    {
        return renderSurvey;
    }
    
    /**
     * This method will check if the person has answered the survey.
     * If the person has answered the survey, then the result graph should be rendered.
     * 
     * @return If the result graph should be rendered.
     * @author Michael Mishin 1612993
     */
    public boolean getRenderResult()
    {
        return renderResult;
    }
    
    /**
     * This method will return the graph model needed for PrimeFaces component to function.
     * 
     * @return The horizontal bar component for PrimeFaces
     * @author Michael Mishin 1612993
     */
    public HorizontalBarChartModel getHbarModel() {
        return hbarModel;
    }
    
    /**
     * This method will update rendering and graph data when the person submits an answer
     * 
     * @author Michael Mishin 1612993
     */
    public void updateSurvey()
    {
        switchRendering();
        updateGraphData();
    }
    
    /**
     * This method will switch between the survey rendering and result rendering
     * 
     * @author Michael Mishin 1612993
     */
    private void switchRendering()
    {
        //updates the rendering (shows answers, removes the radio buttons
        this.renderResult = true;
        this.renderSurvey = false;
    }
    
    /**
     * This method will update the data required for the graph to generate properly.
     * 
     * @author Michael Mishin 1612993
     */
    private void updateGraphData()
    {
        List<Number> values = new ArrayList<>();
        List<String> labels = new ArrayList<>();
        
        getAnswers().forEach(answer ->{
            values.add(answer.getChosenTimes());
            labels.add(answer.getAnswerText());
        });
        
        hbarDataSet.setData(values);
        graphData.setLabels(labels);
        hbarModel.setData(graphData);
    }
}