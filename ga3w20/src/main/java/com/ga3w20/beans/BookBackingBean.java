package com.ga3w20.beans;

import com.ga3w20.controllers.BooksJpaController;
import com.ga3w20.controllers.SoldBooksJpaController;
import com.ga3w20.entities.Books;
import com.ga3w20.entities.Genre;
import com.ga3w20.entities.Reviews;
import com.ga3w20.entities.SoldBooks;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class purpose is to have a backing bean for the Books entity
 *
 * @author Alexander Berestetskyy 1634209
 */
@Named(value = "theBooks")
@SessionScoped
public class BookBackingBean implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(BookBackingBean.class);

    @Inject
    private BooksJpaController booksJpaController;
    @Inject
    private SoldBooksJpaController soldBooksJpaController;
    
    private String searchTerm;
    private List<Books> bookList;
    private HashMap<String, Long> genreMap;
    private Books singleBook;
    private List<Books> sixRecommendedBooks;
    private String genreFilter;
    private Long numOfReviews;
    private Double avgRating;
    private List<Reviews> approvedReviewsOfBook;
    private String selectBy = "bookTitle";
    private String selectByGenre = "Horror";
    private final List<String> genres = new ArrayList<String>();
    private List<Books> filteredBookList;
    private List<Books> allBooks;
    private String sbId="";

    /**
     * Constructor
     *
     * @author Alexander Berestetksyy
     */
    public BookBackingBean() {

    }

    /**
     * Mutator for the search term taken from the search bar.
     *
     * @param searchTerm term provided in search bar
     * @author Alexander Berestetskyy
     */
    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }
    
    /**
     * This method will make a search for all books to display them in the browse page
     * 
     * @return redirect to browse page
     * @author Michael Mishin
     */
    public String searchAll()
    {
        // all the books in the store start with Isbn 9. this search ignores disabled books
        setBookList(booksJpaController.findBooksWithIsbn("9"));
        setGenreMap(booksJpaController.findGenresWithIsbn("9"));
        
        return "/browsepage.xhtml?faces-redirect=true&includeViewParams=true";
    }

    /**
     * Accessor for the search term.
     *
     * @author Alexander Berestetskyy
     * @return 
     */
    public String getSearchTerm() {
        return this.searchTerm;
    }

    /**
     * Mutator for the book list retrieved to show on the browse page.
     *
     * @author Alexander Berestetskyy
     * @param bookList
     */
    public void setBookList(List<Books> bookList) {
        this.bookList = bookList;
    }

    /**
     * Accessor for the book list retrieved to show on the browse page.
     *
     * @return bookList representing all the books retrieved
     * @author Alexander Berestetskyy
     */
    public List<Books> getBookList() {
        return this.bookList;
    }

    /**
     * Accessor for the single book clicked from a list, shown on the book page.
     *
     * @author Alexander Berestetskyy
     * @param singleBook
     */

    public void setSingleBook(Books singleBook){
        this.singleBook = singleBook;
    }

    /**
     * Accessor for the single book clicked from a list, shown on the book page.
     *
     * @return Books representing a single book retrieved
     * @author Alexander Berestetskyy
     */
    public Books getSingleBook(){
        return this.singleBook;
    }

    /**
     * Mutator for the genre HashMap to show on the browse page.
     *
     * @author Alexander Berestetskyy
     * @param genreMap
     */
    public void setGenreMap(HashMap<String, Long> genreMap) {
        this.genreMap = genreMap;
    }

    /**
     * Accessor for the genre HashMap retrieved to show on the browse page.
     *
     * @return genreMap with the genre name and the number of books in that
     * genre
     * @author Alexander Berestetskyy
     */
    public HashMap<String, Long> getGenreMap() {
        return this.genreMap;
    }

    /**
     * Mutator for the six recommended books to show on the book page.
     *
     * @author Alexander Berestetskyy
     * @param sixRecommendedBooks
     */
    public void setSixRecommendedBooks(List<Books> sixRecommendedBooks) {
        this.sixRecommendedBooks = sixRecommendedBooks;
    }

    /**
     * Accessor for the six recommended books retrieved to show on the book
     * page.
     *
     * @return sixRecommendedBooks either 3 books from same author and 3 books
     * from same genre, or six books from same genre
     * @author Alexander Berestetskyy
     */
    public List<Books> getSixRecommendedBooks() {
        return this.sixRecommendedBooks;
    }


    /**
     * Accessor for the number of reviews for a book.
     * 
     * @return numOfReviews
     * @author Alexander Berestetskyy
     */
    public Long getNumOfReviews(){
        return this.numOfReviews;
    }
    
    /**
     * Mutator for the number of reviews for a book.
     * 
     * @author Alexander Berestetskyy
     * @param numOfReviews
     */
    public void setNumOfReviews(Long numOfReviews){
        this.numOfReviews = numOfReviews;
    }
    
    /**
     * Mutator for the select by to update the select on the menu.
     * 
     * @author Alexander Berestetskyy
     * @param selectBy
     */
    public void setSelectBy(String selectBy) {
        this.selectBy = selectBy;
    }
    
    /**
     * Accessor for the select by in menu, to either look up by all, title, author name, publisher or isbn.
     * 
     * @return selectBy will return a String from select on menu
     * @author Alexander Berestetskyy
     */
    public String getSelectBy(){
        return this.selectBy;
    }

     /**
     * Mutator for the select by genre to update the second select on menu.
     * 
     * @author Alexander Berestetskyy
     * @param selectByGenre
     */
    public void setSelectByGenre(String selectByGenre){
        this.selectByGenre = selectByGenre;
    }
 
    /**
     * Accessor for the select by genre in the menu.
     * 
     * @return selectByGenre will return a String representing a genre on second select in menu
     * @author Alexander Berestetskyy
     */
    public String getSelectByGenre(){
        return this.selectByGenre;
    }
    
    /**
     * Mutator for the genre filter on browsepage
     * 
     * @author Alexander Berestetskyy
     * @param genreFilter
     */
    public void setGenreFilter(String genreFilter){
        this.genreFilter = genreFilter;
    }
    
    /**
     * Accessor for the genre filter in browsepage.
     * 
     * @return genreFilter will return a String representing a genre filter on browsepage
     * @author Alexander Berestetskyy
     */
    public String getGenreFilter(){
        return this.genreFilter;
    }
    
    /**
     * Mutator for the average rating for a single book
     * 
     * @author Alexander Berestetskyy
     * @param avgRating
     */
    public void setAvgRating(Double avgRating){
        this.avgRating = avgRating;
    }
    
    /**
     * Accessor for average rating for a single book
     * 
     * @return avgRating 
     * @author Alexander Berestetskyy
     */
    public Double getAvgRating(){
        return this.avgRating;
    }
    
    /**
     * Mutator for the list of approved reviews for a single book
     * 
     * @author Alexander Berestetskyy
     * @param approvedReviewsOfBook
     */
    public void setApprovedReviewsOfBook(List<Reviews> approvedReviewsOfBook){
        this.approvedReviewsOfBook = approvedReviewsOfBook;
    }
    
    /**
     * Accessor for the list of approved reviews for a single book
     * 
     * @return approvedReviewsOfBook
     * @author Alexander Berestetskyy
     */
    public List<Reviews> getApprovedReviewsOfBook(){
        return this.approvedReviewsOfBook;
    }
    
    public void setSbId(String sbId){
        this.sbId=sbId;
    }
    public String getSbId(){
        return this.sbId;
    }
    
    /**
     * Accessor for the list of genres displayed on menu, retrieved from db and for scalability
     * 
     * @return return a list of genre
     * @author Alexander Berestetskyy
     */
    public List<String> getGenres(){
        List<Genre> genresList = booksJpaController.retrieveAllGenres();
        for(Genre genre : genresList){
            if(!this.genres.isEmpty()){
               //this.genres isn't empty
               int counter=0;
               for(String oldGenre : this.genres){
                   if(genre.getGenre().equals(oldGenre)){
                       counter++;
                   }
               }
               if(counter==0){
                   //add a new genre to the list
                   this.genres.add(genre.getGenre());
               }else{
                   counter=0;
               }
            }else{
               //this.genres is empty
               this.genres.add(genre.getGenre());
            }
        }
        return this.genres;
    }
    
    /**
     * Will go in the books controller to retrieve the books that have the corresponding pattern written in 
     * the search bar with all matching book titles, author name, publisher or isbn.It will also retrieve 
 all the genres found with the count of all the books found in that genre by using a HashMap
     * 
     * @param userId
     * @return string corresponding to the next page to go, which is browse page
     * @author Alexander Berestetskyy
     */
    public String processSeacrh(Integer userId){
        if(this.searchTerm.isEmpty()){
            LOG.debug("searchTerm is empty, therefore back on menu");
            return "/index.xhtml?faces-redirect=true";
        }else{
            switch(this.selectBy){
                case "bookTitle":
                    setBookList(booksJpaController.findBooksWithTitle(this.searchTerm));
                    setGenreMap(booksJpaController.findGenresWithTitle(this.searchTerm));
                    break;
                case "authorName":
                    setBookList(booksJpaController.findBooksWithAuthorName(this.searchTerm));
                    setGenreMap(booksJpaController.findGenresWithAuthorName(this.searchTerm));
                    break;
                 case "publisher":
                    setBookList(booksJpaController.findBooksWithPublisher(this.searchTerm));
                    setGenreMap(booksJpaController.findGenresWithPublisher(this.searchTerm));
                    break;
                 case "isbn":
                    setBookList(booksJpaController.findBooksWithIsbn(this.searchTerm));
                    setGenreMap(booksJpaController.findGenresWithIsbn(this.searchTerm));
                    break;
                default:
                    //should never go here, but if it does, send user back to home page
                    return "/index.xhtml?faces-redirect=true";
            }
            if(this.bookList.size()==1){
                showSelectedBook(this.bookList.get(0),userId);
                 return "/bookpage.xhtml?faces-redirect=true&includeViewParams=true";
            }
            return "/browsepage.xhtml?faces-redirect=true&includeViewParams=true";
        }
    }

    /**
     * Will go in the books controller to retrieve the books that have the corresponding pattern written in 
     * the search bar with all matching book titles, author name, publisher or isbn by also adding a genre filter
     * passed as an argument. 
     * 
     * @return string corresponding to the next page to go, which is browse page
     * @param aGenre corresponds to a genre that will be used as a filter
     * @author Alexander Berestetskyy
     */
    public String searchBooksWithGenre(String aGenre){
        LOG.debug("searchTerm in bookBackingBean: "+this.searchTerm);
        if(this.searchTerm == null || this.searchTerm.isEmpty()){
           //means that searchterm isn't set and user went through the "browse books" button
           setBookList(booksJpaController.findBooksWithGenreNoSearchTerm(aGenre));
        }else{
            //means that search term is set
           setBookList(booksJpaController.findBooksWithGenre(this.searchTerm, this.selectBy, aGenre));          
        }
        return "/browsepage.xhtml?faces-redirect=true&includeViewParams=true";
       
    }
    
    /**
     * Search by genre with the top 5 sellers of a genre and show remaining books in that genre
     * 
     * @return string representing the next page, which is the browse by genre
     * @author Alexander Berestetskyy
     */
    public String processSeacrhByGenre(){
        setBookList(booksJpaController.findAllBooksInGenreBestSeller(this.selectByGenre));
        return "/browsebygenre.xhtml?faces-redirect=true&includeViewParams=true";
    }
    
    /**
     * Will go in the books controller to retrieve the books that have the corresponding author
     * selected.It will also retrieve all the genres found with the count of all
     * the books found in that genre by using a HashMap
     * 
     * @param authorName
     * @return string corresponding to the next page to go, which is browse page
     * @author Alexander Berestetskyy
     */
    public String processAuthorSearch(String authorName){
        setBookList(booksJpaController.findBooksWithAuthorName(authorName));
        setGenreMap(booksJpaController.findGenresWithAuthorName(authorName));
        setSearchTerm(authorName);
        setSelectBy("authorName");
        return "/browsepage.xhtml?faces-redirect=true&includeViewParams=true";
    }
    
    /**
     * Show the information of that selected book to show on the book page,
     * including the six recommended books
     * 
     * @param oneBook
     * @param userId
     * @return string corresponding to the next page to go, which is book page
     * @author Alexander Berestetskyy
     */
    public String showSelectedBook(Books oneBook,Integer userId){
        LOG.debug("userId in showSelectedBook: "+userId);
        setSingleBook(oneBook);
        setApprovedReviewsOfBook(booksJpaController.getApprovedReviewsOfBook(oneBook.getIsbn()));
        setNumOfReviews(booksJpaController.findNumReviewsOfSingleBook(oneBook.getIsbn()));
        setAvgRating(booksJpaController.findAvgRatingOfSingleBook(oneBook.getIsbn()));
       //set all the recommended books, either 3 books in same genre and same author, or 6 books in same genre
       setSixRecommendedBooks(booksJpaController.findAllSixRecommendedBooks(oneBook.getAuthorId().getName(), oneBook.getGenreList().get(0), oneBook.getIsbn()));
       //checking if user has bought this book or not
       if(userId != 0){
           LOG.debug("userId is null");
           List<SoldBooks> userBoughtBook = soldBooksJpaController.findSaleOfBookWithUser(userId, oneBook.getIsbn());
           if(!userBoughtBook.isEmpty()){
                String newSbId = userBoughtBook.get(0).getId().toString();
                LOG.debug("new sb id: "+newSbId);
                setSbId(newSbId);
            }else{
                LOG.debug("no new sb id");
                setSbId("");
            }
       }else{
           setSbId("");
       }
       return "/bookpage.xhtml?faces-redirect=true&includeViewParams=true";  
    }
       
    @PostConstruct
    public void init(){
        setAllBooks(booksJpaController.findBooksEntities());
    }
    
    /**
     * 
     * @return 
     */
    public List<Books> getFilteredBookList() {
        return filteredBookList;
    }

    /**
     * 
     * @param filteredBookList 
     */
    public void setFilteredBookList(List<Books> filteredBookList) {
        this.filteredBookList = filteredBookList;
    }
    
    /**
     * 
     * @return 
     */
    public List<Books> getAllBooks() {
        setAllBooks(booksJpaController.findBooksEntities());
        return allBooks;
    }
    
    /**
     * 
     * @param allBooks 
     */
    public void setAllBooks(List<Books> allBooks) {
        this.allBooks = allBooks;
    }
    
    /**
     * 
     * @param isbn
     * @param discount 
     */
    public void editBookSalePrice(String isbn, long discount){
        this.booksJpaController.editBookPrice(isbn, discount);
    }
    
    /**
     * 
     * @param isbn 
     */
    public void removeBookDiscount(String isbn){
        this.booksJpaController.removeBookDiscount(isbn);
        
    }
}
