package com.ga3w20.beans;

import java.util.Objects;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Bean used to contain an email.
 * @author Ken Fogel
 */
@Named
@RequestScoped
public class MailBean {

    private final static Logger LOG = LoggerFactory.getLogger(MailBean.class);
    
    private String sendTo;
    private String plainTextMsg;
    private String htmlTextMsg;
    private String subject;

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public String getPlainTextMsg() {
        return plainTextMsg;
    }

    public void setPlainTextMsg(String plainTextMsg) {
        this.plainTextMsg = plainTextMsg;
    }

    public String getHtmlTextMsg() {
        return htmlTextMsg;
    }

    public void setHtmlTextMsg(String htmlTextMsg) {
        this.htmlTextMsg = htmlTextMsg;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.sendTo);
        hash = 53 * hash + Objects.hashCode(this.plainTextMsg);
        hash = 53 * hash + Objects.hashCode(this.htmlTextMsg);
        hash = 53 * hash + Objects.hashCode(this.subject);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MailBean other = (MailBean) obj;
        if (!Objects.equals(this.sendTo, other.sendTo)) {
            return false;
        }
        if (!Objects.equals(this.plainTextMsg, other.plainTextMsg)) {
            return false;
        }
        if (!Objects.equals(this.htmlTextMsg, other.htmlTextMsg)) {
            return false;
        }
        if (!Objects.equals(this.subject, other.subject)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MailBean{sendTo=").append(sendTo);
        sb.append(", plainTextMsg=").append(plainTextMsg);
        sb.append(", htmlTextMsg=").append(htmlTextMsg);
        sb.append(", subject=").append(subject);
        sb.append('}');
        return sb.toString();
    }
    
}
