package com.ga3w20.beans;

import com.ga3w20.controllers.NewsFeedJpaController;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * This Bean is to control the current News feed that is activated in the DB
 *
 * @author Michael Mishin 
 */
@Named
@RequestScoped
public class NewsFeedBean implements Serializable{
    private String currentActiveURL;
    
    private static final int NEWS_TO_SHOW = 10; // how many news to show from the given link
    
    @Inject
    private NewsFeedJpaController newsFeedJpaController;
    
    /**
     * Constructor that will get the first active url link for the news feed
     * 
     * @author Michael Mishin 1612993
     */
    @PostConstruct
    public void init()
    {
        currentActiveURL = newsFeedJpaController.findNewsFeedEntities().stream()
                .filter(newsFeed -> newsFeed.getIsActive())
                .findFirst()
                .get().getLink();
    }
    
    /**
     * This method will return the current active URL for the news feed.
     * 
     * @return The current active news URL string.
     * @author Michael Mishin 1612993
     */
    public String getCurrentActiveURL()
    {
        return currentActiveURL;
    }
    
    /**
     * This method will return the size for how many news to show from a given rss feed link
     * 
     * @return The size of the news feed
     * @author Michael Mishin 1612993
     */
    public int getHowManyNewsToShow()
    {
        return NEWS_TO_SHOW;
    }
}
