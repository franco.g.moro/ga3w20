/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ga3w20.beans;

import com.ga3w20.controllers.BooksJpaController;
import com.ga3w20.controllers.ReviewsJpaController;
import com.ga3w20.controllers.UsersJpaController;
import com.ga3w20.entities.Books;
import com.ga3w20.entities.Reviews;
import com.ga3w20.entities.Users;
import java.io.Serializable;
import java.util.Date;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class purpose is to have a backing bean for the Reviews entity
 *
 * @author Alexander Berestetskyy 1634209
 */
@Named(value = "theReviews")
@SessionScoped
public class ReviewBackingBean implements Serializable{
    
    private final static Logger LOG = LoggerFactory.getLogger(ReviewBackingBean.class);
    
    @Inject
    private ReviewsJpaController reviewsJpaController;
    @Inject
    private UsersJpaController usersJpaController;
    @Inject
    private BooksJpaController booksJpaController;
    
    private String textReview;
    private String rating;
    private String isbn;
    private String email;
    private Boolean canSendReview=false;
    /**
     * Constructor
     * @author Alexander Berestetksyy
     */
    public ReviewBackingBean(){
        
    }
    public ReviewBackingBean(String textReview, String rating, String isbn, String email){
        this.textReview = textReview;
        this.rating = rating;
        this.isbn = isbn;
        this.email = email;
    }
    /**
     * Mutator for the text of a review.
     *
     * @param textReview representing the text an user has written for a review
     * @author Alexander Berestetskyy
     */
    public void setTextReview(String textReview){
        this.textReview = textReview;
    }
    /**
     * Accessor for the text of a review.
     *
     * @return string the text an user has written for a review
     * @author Alexander Berestetskyy
     */
    public String getTextReview(){
        return this.textReview;
    }
    
    /**
     * Mutator for the rating of a review.
     *
     * @param rating the rating of a review
     * @author Alexander Berestetskyy
     */
    public void setRating(String rating){
        this.rating = rating;
    }
    
    /**
     * Accessor for the rating of a review.
     *
     * @return string the rating for a review
     * @author Alexander Berestetskyy
     */
    public String getRating(){
        return this.rating;
    }
    
    /**
     * Mutator for the isbn of a book (ie private key of Books entity).
     *
     * @param isbn represents a book
     * @author Alexander Berestetskyy
     */
    public void setIsbn(String isbn){
        this.isbn = isbn;
    }
    
    /**
     * Accessor for the isbn of a book (ie private key of Books entity).
     *
     * @return isbn of a book
     * @author Alexander Berestetskyy
     */
    public String getIsbn(){
        return this.isbn;
    }
    
    /**
     * Mutator for the email of a user (ie private key of User entity).
     *
     * @param email represents a user
     * @author Alexander Berestetskyy
     */
    public void setEmail(String email){
        this.email = email;
    }
    /**
     * Accessor for the email of a user (ie private key of User entity).
     *
     * @return email representing a user
     * @author Alexander Berestetskyy
     */
    public String getEmail(){
        return this.email;
    }
    
    /**
     * Mutator for representing if review can be sent or not
     *
     * @param canSendReview
     * @author Alexander Berestetskyy
     */
    public void setCanSendReview(Boolean canSendReview){
        this.canSendReview = canSendReview;
    }
    /**
     * Accessor for representing if review can be sent or not
     *
     * @return Boolean
     * @author Alexander Berestetskyy
     */
    public Boolean getCanSendReview(){
        return this.canSendReview;
    }
    
    /**
     * Check if user is logged in or not, and redirect him to sign in page if not
     * 
     * @param bookIsbn representing an isbn of a book
     * @param email representing the email of the user logged in, if any
     * @return string representing the name of the page to redirect
     * @author Alexander Berestetskyy
     * @throws java.lang.Exception
     */
    public String sendReviewForApproval(String bookIsbn,String email) throws Exception{
        if (this.textReview.length()==0 || this.textReview.length()>=3000 || this.rating.equals("")){
            return "/bookpage.xhtml?faces-redirect=true&includeViewParams=true";
        }
        setIsbn(bookIsbn);
        setEmail(email);
        if(email.isEmpty()){
            //it means that the client is logged in or registered and he needs to be beforehand
            return "/signin.xhtml?faces-redirect=true";
        }
        //means that user is already connected
        createReview();
        return "/bookpage.xhtml?faces-redirect=true&includeViewParams=true";
    }
    
    /**
     * Send a review, by an user, to manager for approval on a specific book
     * 
     * @return string representing the name of the page to redirect, bookpage
     * @author Alexander Berestetskyy
     * @throws java.lang.Exception
     */
    public String createReview() throws Exception{
        LOG.debug("review by "+email+" on book "+isbn+" with a rating of "+rating+" text review of "+textReview);
        Date date = new Date();
        Integer maxId = reviewsJpaController.getMaxIdOfReviews()+1;
        Reviews newReview = new Reviews(maxId,Short.parseShort(this.rating),this.textReview,date,"false");
        //set Books that review will be sent
        Books bookRetrieved = booksJpaController.findSingleBookForReview(this.isbn);
        newReview.setIsbn(bookRetrieved);
        //set user thar wrote review that will be sent
        Users idFound = usersJpaController.findIdOfUser(this.email);
        newReview.setUserId(idFound);
        reviewsJpaController.create(newReview);
        setCanSendReview(true);
        setTextReview("");
        setRating("");
        setIsbn("");
        setEmail("");
        return "/bookpage.xhtml?faces-redirect=true&includeViewParams=true";
    }
}
