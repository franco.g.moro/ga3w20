package com.ga3w20.beans;

/**
 * This class is used for testing report methods
 * 
 * @author Hadil Elhashani 
 * @param <T>
 * @param <U>
 * @param <X>
 */
public class Triplet<T, U, X> {
    
    private T value0;
    private U value1;
    private X value2;

    /**
     * 
     * @param value0 form date
     * @param value1 to date
     * @param value2 size of the list
     */
    public Triplet(T value0, U value1, X value2) {
        this.value0 = value0;
        this.value1 = value1;
        this.value2 = value2;
    }

    public T getValue0() {
        return value0;
    }

    public void setValue0(T value0) {
        this.value0 = value0;
    }

    public U getValue1() {
        return value1;
    }

    public void setValue1(U value1) {
        this.value1 = value1;
    }

    public X getValue2() {
        return value2;
    }

    public void setValue2(X value2) {
        this.value2 = value2;
    }
    
}
