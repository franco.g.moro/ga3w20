package com.ga3w20.beans;

import com.ga3w20.controllers.SalesJpaController;
import com.ga3w20.entities.Sales;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Hadil Elhashani
 */
@Named(value = "saleBean")
@SessionScoped
public class SaleBackingBean implements Serializable {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(SaleBackingBean.class);

    private List salesList;
    private List filteredSalesList;
    private String searchTerm;

    @Inject
    private SalesJpaController salesController;

    public SaleBackingBean() {
    }

    /**
     *
     * @param salesId
     */
    public void markSaleRemoved(Integer salesId) {
        LOG.debug("markSaleRemoved");
        LOG.debug("salesId = " + salesId);
        salesController.updateSalesRemovalStatus(salesId);
    }

    public List getSalesList() {
        salesList = salesController.findSalesEntities();
        return salesList;
    }

    public void setSalesList(List salesList) {
        this.salesList = salesList;
    }

    public List getFilteredSalesList() {
        return filteredSalesList;
    }

    public void setFilteredSalesList(List filteredSalesList) {
        this.filteredSalesList = filteredSalesList;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }
    
}
