package com.ga3w20.beans;

import com.ga3w20.cookies.LanguagePreferenceCookie;
import com.ga3w20.cookies.PreRenderViewBean;
import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This bean manages the locale. If you change the locale it sends a message to
 * redraw the page thus changing the language.
 *
 * @author Michael Mishin 1612993
 */
@Named
@SessionScoped
public class LocaleBean implements Serializable {
    private final static Logger LOG = LoggerFactory.getLogger(LocaleBean.class);

    private Locale locale;
    
    @Inject
    private LanguagePreferenceCookie languagePreference;
    
    @Inject
    private PreRenderViewBean preRenderView;

    public LocaleBean()
    {
        locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
    }
    
    @PostConstruct
    public void init() 
    {
        //get locale from cookie if enabled
        if(preRenderView.getCookiesEnabled())
        {
            languagePreference.checkCookies();
        }
    }

    /**
     * This method will get the current locale
     * 
     * @return The locale
     * @author Michael Mishin
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * This method will get the current language
     * 
     * @return The language
     * @author Michael Mishin
     */
    public String getLanguage() {
        return locale.getLanguage().toUpperCase();
    }

    /**
     * This method will set the language with the given parameters
     * 
     * @param language
     * @param country
     * @throws java.io.IOException
     * @author Michael Mishin
     */
    public void setLanguage(String language, String country) throws IOException {
        LOG.debug("setting language to:"+language+"_"+country);
        locale = new Locale(language, country);
        
        // writes the preference to a cookie
        if(preRenderView.getCookiesEnabled())
        {
            LOG.debug("writing to a cookie the language.");
            languagePreference.writeCookie();
            languagePreference.checkCookies();
        }
        else //if cookies are disabled. change the language in the session
        {
            FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
        }
        
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }

}
