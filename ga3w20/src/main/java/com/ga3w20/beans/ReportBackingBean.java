package com.ga3w20.beans;

import com.ga3w20.controllers.BooksJpaController;
import com.ga3w20.custom.entities.AuthorSale;
import com.ga3w20.custom.entities.ClientSale;
import com.ga3w20.custom.entities.Inventory;
import com.ga3w20.custom.entities.PublisherSale;
import com.ga3w20.custom.entities.TopClient;
import com.ga3w20.custom.entities.TopSeller;
import com.ga3w20.entities.Books;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.LoggerFactory;

/**
 * Used to mange all reports page
 *
 * @author Hadil Elhashani
 */
@Named
@RequestScoped
public class ReportBackingBean implements Serializable {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(ReportBackingBean.class);
    @Inject
    private BooksJpaController bookController;

    private List<TopSeller> topSellers;
    private List<TopClient> topClients;
    private List<Inventory> totalSales;
    private List<Inventory> stockReport;
    private List<Books> zeroSales;
    private List<AuthorSale> authorSale;
    private List<PublisherSale> publisherSale;
    private List<ClientSale> clientSale;

    private String fromDate;
    private String toDate;
    private String name;

    @PostConstruct
    private void init() {
        // DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        // LocalDateTime now = LocalDateTime.now();
        // this.toDate = dtf.format(LocalDateTime.of(2020, Month.FEBRUARY, 1, 0, 1));
        // this.fromDate = dtf.format(LocalDateTime.of(2020, Month.JANUARY, 1, 0, 1));

    }

    /**
     * Converts input string to date
     *
     * @author Hadil Elhashani
     * @param dateStr
     * @return
     */
    private Date convertToDate(String dateStr) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = df.parse(dateStr);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(BooksJpaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOG.debug("DATE= " + date.toString());
        return date;
    }

    /**
     *
     * @param context
     * @param component
     * @param value
     */
    public void validateDateRange(FacesContext context, UIComponent component, Object value) {
        LOG.debug("validateDateRange");
        String tDate = (String) value;

        UIInput fromDateInput = (UIInput) component.findComponent("fromDate");
        String fDate = (String) fromDateInput.getLocalValue();
        
        if (tDate == null || fDate == null || tDate.length() == 0 || fDate.length() == 0) {
            String message = context.getApplication().evaluateExpressionGet(context, "Please choose both date", String.class);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", message);
            throw new ValidatorException(msg);           
        }
        
        Date toDate = this.convertToDate(tDate);
        Date fromDate = this.convertToDate(fDate);
        

        if (fromDate.after(toDate)) {
            String message = context.getApplication().evaluateExpressionGet(context, "#{msgs['dateRangeMsg']}", String.class);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", message);
            throw new ValidatorException(msg);
        }
    }

    public List<TopSeller> getTopSellers() {
        this.setTopSellers(bookController.findTopSeller(this.fromDate, this.toDate));
        return topSellers;
    }

    public List<TopClient> getTopClients() {
        this.setTopClients(bookController.findTopClients(this.fromDate, this.toDate));
        return topClients;
    }

    public List<Inventory> getTotalSales() {
        this.setTotalSales(bookController.findTotalSales(this.fromDate, this.toDate));
        return totalSales;
    }

    public List<Inventory> getStockReport() {
        this.setStockReport(bookController.findStockReport(this.fromDate, this.toDate));
        return stockReport;
    }

    public List<Books> getZeroSales() {
        return zeroSales;
    }

    public List<AuthorSale> getAuthorSale() {
        this.setAuthorSale(bookController.findSalesByAuthor(this.fromDate, this.toDate, this.name));
        return authorSale;
    }

    public List<PublisherSale> getPublisherSale() {
        this.setPublisherSale(bookController.findSalesByPublisher(this.fromDate, this.toDate, this.name));
        return publisherSale;
    }

    public List<ClientSale> getClientSale() {
        this.setClientSale(bookController.findSalesByClient(this.fromDate, this.toDate, this.name));
        return clientSale;
    }

    public String getFromDate() {
        return fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public String getName() {
        return name;
    }

    public void setTopSellers(List<TopSeller> topSeller) {
        this.topSellers = topSeller;
    }

    public void setTopClients(List<TopClient> topClients) {
        this.topClients = topClients;
    }

    public void setTotalSales(List<Inventory> totalSales) {
        this.totalSales = totalSales;
    }

    public void setStockReport(List<Inventory> stockReport) {
        this.stockReport = stockReport;
    }

    public void setZeroSales(List<Books> zeroSales) {
        this.zeroSales = zeroSales;
    }

    public void setAuthorSale(List<AuthorSale> authorSale) {
        this.authorSale = authorSale;
    }

    public void setPublisherSale(List<PublisherSale> publisherSale) {
        this.publisherSale = publisherSale;
    }

    public void setClientSale(List<ClientSale> clientSale) {
        this.clientSale = clientSale;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void ZeroSaleBooks() {        
        if (toDate.compareTo(fromDate) > 0) {            
            this.zeroSales = this.bookController.findZeroSalesBook(fromDate, toDate);
        } else {
            this.zeroSales = null;
        }        
    }
}
