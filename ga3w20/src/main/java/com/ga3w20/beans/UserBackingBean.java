package com.ga3w20.beans;

import com.ga3w20.controllers.UsersJpaController;
import com.ga3w20.entities.Users;
import com.ga3w20.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Hadil Elhashani
 */
@Named(value = "userBean")
@SessionScoped
public class UserBackingBean implements Serializable{
    
    private Users user;
    private List<Users> usersList;
    private List<Users> filteredUsersList;
    
    @Inject
    private UsersJpaController userController;

    public UserBackingBean() {
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public List<Users> getUsersList() {
        setUsersList(userController.findUsersEntities());
        return usersList;
    }

    public void setUsersList(List<Users> usersList) {
        this.usersList = usersList;
    }

    public List<Users> getFilteredUsersList() {
        return filteredUsersList;
    }

    public void setFilteredUsersList(List<Users> filteredUsersList) {
        this.filteredUsersList = filteredUsersList;
    }
    
//    @PostConstruct
//    public void init(){
//        setUsersList(userController.findUsersEntities());
//    }
    
    public String editUser(Integer userId){
        this.user = userController.findUsers(userId);
        return "editUserInfo";
    }
    
    public String editUserInfo(){
        try {
            userController.edit(this.user);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(UserBackingBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(UserBackingBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "/management/editUserInfo.xhtml?faces-redirect=true";
    }
    
    
    
    
}
