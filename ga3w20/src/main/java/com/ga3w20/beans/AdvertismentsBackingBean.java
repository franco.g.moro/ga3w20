package com.ga3w20.beans;

import com.ga3w20.controllers.AdvertismentsJpaController;
import com.ga3w20.entities.Advertisments;
import com.ga3w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.RowEditEvent;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Yongchao
 */
@Named
@SessionScoped
public class AdvertismentsBackingBean implements Serializable {

    
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(AdvertismentsBackingBean.class);
    private List<Advertisments> advertismentsList;
    private String imageUrl;
    private String link;

    
    @Inject
    AdvertismentsJpaController advertismentsController;
    
    @PostConstruct
    private void init() {
        advertismentsList = advertismentsController.findAdvertismentsEntities();
    }

    public List<Advertisments> getAdvertismentsList() {
        return advertismentsList;
    }

    public void setAdvertismentsList(List<Advertisments> advertismentsList) {
        this.advertismentsList = advertismentsList;
    }

    public void onRowEdit(RowEditEvent event) {
        
        FacesMessage msg = new FacesMessage("Advertisment Edited. ");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        try {
            advertismentsController.edit((Advertisments)event.getObject());
        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }    

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    
    public String addAdvertisment() {
        Advertisments ad = new Advertisments();
        ad.setImageUrl(this.imageUrl);
        ad.setLink(this.link);
        
        this.imageUrl = "";
        this.link = "";
        try {
            advertismentsController.create(ad);
        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
        init();
        return "manageBannerAds";
    }
    
    public String deleteAdvertisment(int advertismentsId) {
        try {
            advertismentsController.destroy(advertismentsId);
        } catch (RollbackFailureException ex) {
            LOG.error(ex.getMessage());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
        init();
        return "manageBannerAds";
    }
    
    public void activateAdvertisment(Integer id) {
        this.advertismentsController.activateAdvertisment(id);
        this.advertismentsList = this.advertismentsController.findAdvertismentsEntities();
    }
}
