/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ga3w20.transactions;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Ken Fogel git: @omniprof
 * This is a custom validator that must implement the Validator interface. The
 * annotation provides the name that is used in the tag This implementation
 * performs the Luhn test
 *
 */
@FacesValidator("com.ga3w20.CreditCardValidator")
public class CreditCardValidator implements Validator {

    private final static Logger LOG = LoggerFactory.getLogger(CreditCardValidator.class);

    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) {
        if (value == null) {
            return;
        }
        String cardNumber;
        if (value instanceof CreditCard) {
            cardNumber = value.toString();
        } else {
            cardNumber = value.toString().replaceAll("\\D", ""); // remove
        }																	// non-digits
        if (!luhnCheck(cardNumber)) {
            FacesMessage message = com.ga3w20.util.Messages.getMessage(
                    "com.ga3w20.bundles.messages", "badLuhnCheck", null);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }

    private static boolean luhnCheck(String cardNumber) {
        int sum = 0;

        for (int i = cardNumber.length() - 1; i >= 0; i -= 2) {
            sum += Integer.parseInt(cardNumber.substring(i, i + 1));
            if (i > 0) {
                int d = 2 * Integer.parseInt(cardNumber.substring(i - 1, i));
                if (d > 9) {
                    d -= 9;
                }
                sum += d;
            }
        }

        return sum % 10 == 0;
    }
}
