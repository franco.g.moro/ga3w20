/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ga3w20.transactions;

import com.ga3w20.controllers.FormatJpaController;
import com.ga3w20.entities.Format;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is essentially only used to give out the list of formats but 
 * if we ever wanted to have different downloads for different books/formats
 * then this class would be a lot more useful.
 * @author Franco G. Moro
 */
@Named(value="downloads")
@SessionScoped
public class DownloadsController implements Serializable {
    private final static Logger LOG=LoggerFactory.getLogger(DownloadsController.class);
    
    @Inject
    private FormatJpaController formatJpa;
    
    private List<String> formats;

    public List<String> getFormats() {
        if(formats==null){
            formats=new ArrayList<String>();
            List<Format> formatEntities=formatJpa.findFormatEntities();
            for(Format f:formatEntities){
                formats.add(f.getFormat());
            }
        }
        return formats;
    }

    public void setFormats(List<String> formats) {
        this.formats = formats;
    }

    public FormatJpaController getFormatJpa() {
        return formatJpa;
    }

    public void setFormatJpa(FormatJpaController formatJpa) {
        this.formatJpa = formatJpa;
    }
    
    
    

    
    
    
    
}
