/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ga3w20.transactions;

import java.io.Serializable;

/**
 * This bean is used by CreditCardConverter and TransactionController
 * @author Franco G. Moro
 */
public class CreditCard implements Serializable {
    private final String number;
    public CreditCard(String number){
       this.number=number;
    }
    @Override
    public String toString() {
        return number;
    }
}
