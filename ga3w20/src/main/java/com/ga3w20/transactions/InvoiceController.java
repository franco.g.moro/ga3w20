/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ga3w20.transactions;

import com.ga3w20.authentication.UserController;
import com.ga3w20.beans.MailBean;
import com.ga3w20.entities.Books;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import jodd.mail.Email;
import jodd.mail.MailServer;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Franco G. Moro
 * This class contains the actions related to an invoice.
 */
@Named(value = "invoice")
@SessionScoped
public class InvoiceController implements Serializable{
    @Inject
    private MailBean mailBean;
    @Inject UserController userController;
    private final String emailSender;
    private final String smtpPassword;
    private final String smtpServerName;
    private String lastFourDigits;
    private String total;
    private String totalSavings;
    private String saleNumber;
    private List<Books> boughtBooks;
    private BigDecimal totalTax;
    private Boolean sendByEmail;
    private final static Logger LOG = LoggerFactory.getLogger(InvoiceController.class);
    
    /**
     * This controller is used to set the fields defined in web.xml
     */
    public InvoiceController(){
        FacesContext ctx = FacesContext.getCurrentInstance();
        
        emailSender = ctx.getExternalContext().getInitParameter("emailSender");
        smtpPassword = ctx.getExternalContext().getInitParameter("smtpPassword");
        smtpServerName = ctx.getExternalContext().getInitParameter("smtpServerName");
    }
    /**
     * This method checks if the user desires to receive the invoice by email.
     * Redirects to downloads page.
     * @return String
     */
    public String redirectToDownloads(){
        if(sendByEmail){
            sendEmail(createHTMLString(),com.ga3w20.util.Messages.getMessage("com.ga3w20.bundles.messages", "invoiceFrom",null).getSummary() ,
                    userController.getEmail());
        }
        return "downloads.xhtml?faces-redirect=true";
    }
    /**
     * This method is used to send an email through the google smtp server.
     */
    public void sendEmail(String html,String subject,String emailAdd) {
        mailBean.setHtmlTextMsg(html);
        mailBean.setSubject(subject)  ;
        mailBean.setSendTo(emailAdd);
        mailBean.setPlainTextMsg(" ");
        // Create am SMTP server object
        SmtpServer smtpServer = MailServer.create()
                .ssl(true)
                .host(smtpServerName)
                .auth(emailSender, smtpPassword)
                //.debugMode(true)
                .buildSmtpMailServer();

        // Using the fluent style of coding create a plain and html text message
        Email email = Email.create().from(emailSender)
                .to(mailBean.getSendTo())
                .subject(mailBean.getSubject())
                .textMessage(mailBean.getPlainTextMsg() + "\n\n" + LocalDateTime.now())
                .htmlMessage( mailBean.getHtmlTextMsg());

        // Like a file we open the session, send the message and close the
        // session. Session automatically closed by using try-with-resources
        try ( // A session is the object responsible for communicating with the server
                SendMailSession session = smtpServer.createSession()) {
            // Like a file we open the session, send the message and close the
            // session
            session.open();
            session.sendMail(email);
            LOG.info("Email sent");
        }
    }
    /**
     * This private method is used to build an html document representing the invoice.
     * This is so it can be sent as an email.
     * @return html
     */
    private String createHTMLString(){
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append("<html><META http-equiv=Content-Type "
                        + "content=\"text/html; charset=utf-8\">"
                        + "<body> ");
        stringBuilder.append("<table>");
        
        stringBuilder.append("<tr>");
        stringBuilder.append("<th>");
        stringBuilder.append(com.ga3w20.util.Messages.getMessage("com.ga3w20.bundles.messages", "saleNum",null).getSummary());
        stringBuilder.append("</th>");
        stringBuilder.append("<td>");
        stringBuilder.append(this.saleNumber);
        stringBuilder.append("</td>");
        stringBuilder.append("</tr>");
        
        stringBuilder.append("<tr>");
        stringBuilder.append("<th>");
        stringBuilder.append(com.ga3w20.util.Messages.getMessage("com.ga3w20.bundles.messages", "total",null).getSummary());
        stringBuilder.append("</th>");
        stringBuilder.append("<td>");
        stringBuilder.append(this.total+"$");
        stringBuilder.append("</td>");
        stringBuilder.append("</tr>");
        
        stringBuilder.append("<tr>");
        stringBuilder.append("<th>");
        stringBuilder.append(com.ga3w20.util.Messages.getMessage("com.ga3w20.bundles.messages", "totalSaving",null).getSummary());
        stringBuilder.append("</th>");
        stringBuilder.append("<td>");
        stringBuilder.append(this.totalSavings+"$");
        stringBuilder.append("</td>");
        stringBuilder.append("</tr>");
        
        stringBuilder.append("<tr>");
        stringBuilder.append("<th>");
        stringBuilder.append(com.ga3w20.util.Messages.getMessage("com.ga3w20.bundles.messages", "paidWith",null).getSummary());
        stringBuilder.append("</th>");
        stringBuilder.append("<td>");
        stringBuilder.append(this.lastFourDigits);
        stringBuilder.append("</td>");
        stringBuilder.append("</tr>");
        
        stringBuilder.append("</table>");
        stringBuilder.append("<br></br>");
        stringBuilder.append("<br></br>");
        stringBuilder.append("<table>");
        stringBuilder.append("<tr>");
        stringBuilder.append("<th>");
        stringBuilder.append(com.ga3w20.util.Messages.getMessage("com.ga3w20.bundles.messages", "titleEmail",null).getSummary());
        stringBuilder.append("</th>");
        stringBuilder.append("<th>");
        stringBuilder.append(com.ga3w20.util.Messages.getMessage("com.ga3w20.bundles.messages", "authorEmail",null).getSummary());
        stringBuilder.append("</th>");
        stringBuilder.append("</tr>");
        for(Books b:boughtBooks){
            stringBuilder.append("<tr>");
            stringBuilder.append("<td>");
            stringBuilder.append(b.getTitle());
            stringBuilder.append("</td>");
            stringBuilder.append("<td>");
            stringBuilder.append(b.getAuthorId().getName());
            stringBuilder.append("</td>");
            stringBuilder.append("</tr>");
        }
        stringBuilder.append("</table>");
        stringBuilder.append("</body");
        stringBuilder.append("</html>");
        return stringBuilder.toString();
    }

    public String getLastFourDigits() {
        return lastFourDigits;
    }

    public void setLastFourDigits(String lastFourDigits) {
        this.lastFourDigits = lastFourDigits;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotalSavings() {
        return totalSavings;
    }

    public void setTotalSavings(String totalSavings) {
        this.totalSavings = totalSavings;
    }

    public String getSaleNumber() {
        return saleNumber;
    }

    public void setSaleNumber(String saleNumber) {
        this.saleNumber = saleNumber;
    }

    public List<Books> getBoughtBooks() {
        return boughtBooks;
    }

    public void setBoughtBooks(List<Books> boughtBooks) {
        this.boughtBooks = boughtBooks;
    }

    public BigDecimal getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(BigDecimal totalTax) {
        this.totalTax = totalTax;
    } 

    public MailBean getMailBean() {
        return mailBean;
    }

    public void setMailBean(MailBean mailBean) {
        this.mailBean = mailBean;
    }

    public UserController getUserController() {
        return userController;
    }

    public void setUserController(UserController userController) {
        this.userController = userController;
    }

    public Boolean getSendByEmail() {
        return sendByEmail;
    }

    public void setSendByEmail(Boolean sendByEmail) {
        this.sendByEmail = sendByEmail;
    }
    
    
    
}
