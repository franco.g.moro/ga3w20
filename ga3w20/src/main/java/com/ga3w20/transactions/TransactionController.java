/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ga3w20.transactions;

import com.ga3w20.authentication.UserController;
import com.ga3w20.beans.ShoppingCartBean;
import com.ga3w20.controllers.SalesJpaController;
import com.ga3w20.controllers.SoldBooksJpaController;
import com.ga3w20.entities.Books;
import com.ga3w20.entities.Sales;
import com.ga3w20.entities.SoldBooks;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Franco G. Moro 1738714
 * This controller is used to handle transactions
 */
@Named("transactionController")
@RequestScoped
public class TransactionController implements Serializable {
    private final static Logger LOG = LoggerFactory.getLogger(TransactionController.class); 
    @Inject
    InvoiceController invoiceBean;
    @Inject
    ShoppingCartBean shoppingCartBean;
    @Inject
    UserController userController;
    @Inject
    SalesJpaController saleJpa;
    @Inject
    SoldBooksJpaController soldBookJpa;
    
    private CreditCard card;
    private Date expiry=new Date();
    
    
    /**
     * This method handles the processing of a purchase.
     * It also redirects the user to the invoice page.
     * @return 
     */
    public String purchase(){
        List<Books> books=shoppingCartBean.getBooks();
        List<SoldBooks> soldBooksList=new ArrayList<SoldBooks>();
        Sales sale=new Sales();
        sale.setUsersId(this.userController.getLoggedUser());
        sale.setIsRemoved(false);
        sale.setSubtotal(shoppingCartBean.getShoppingSaleTotal());
        sale.setTotal(shoppingCartBean.getTotalPrice());
        BigDecimal totalTax=(shoppingCartBean.getGST() ).add(shoppingCartBean.getHST()).add(shoppingCartBean.getPST()).add(shoppingCartBean.getQST());
        sale.setTotalTax(totalTax) ;
        sale.setDate(new Date(System.currentTimeMillis()));
        for(Books b:books){
            SoldBooks soldBook=new SoldBooks();
            soldBook.setBooksIsbn(b);
            soldBook.setPrice(b.getSalePrice());
            soldBook.setSalesId(sale);
            soldBooksList.add(soldBook);
        }
        
        try{
            saleJpa.create(sale);
            for(SoldBooks sb:soldBooksList){
                soldBookJpa.create(sb);
            }
            sale.setSoldBooksList(soldBooksList);
            saleJpa.edit(sale);
        }
        catch(Exception e){
            LOG.error(e.getMessage());
        }
        invoiceBean.setTotal(shoppingCartBean.getTotalPrice().toString());
        invoiceBean.setTotalSavings(shoppingCartBean.getSavingTotal().negate().toString());
        invoiceBean.setTotalTax(totalTax);
        invoiceBean.setSaleNumber(sale.getId().toString());
        invoiceBean.setLastFourDigits(card.toString().substring(card.toString().length()-4,card.toString().length()) );
        invoiceBean.setBoughtBooks(books);
        shoppingCartBean.clearCart();
        return "invoice.xhtml";
    }
    


    public CreditCard getCard() {
        if(this.card==null){
            return new CreditCard("");
        }
        return this.card;
    }

    public void setCard(CreditCard card) {
        this.card =card;
    }

    public Date getExpiry() {
        if(this.expiry == null){
            return new Date();
        }
        return expiry;
    }

    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    public ShoppingCartBean getShoppingCartBean() {
        return shoppingCartBean;
    }

    public void setShoppingCartBean(ShoppingCartBean shoppingCartBean) {
        this.shoppingCartBean = shoppingCartBean;
    }

    public UserController getUserController() {
        return userController;
    }

    public void setUserController(UserController userController) {
        this.userController = userController;
    }
    
    
}
