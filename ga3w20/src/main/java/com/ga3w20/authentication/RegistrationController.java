/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ga3w20.authentication;

import com.ga3w20.controllers.UsersJpaController;
import com.ga3w20.entities.Users;
import com.ga3w20.exceptions.RollbackFailureException;
import com.ga3w20.validation.HashUtil;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Franco G. Moro 1738714
 * This controller handler registration
 */
@Named(value="registration")
@RequestScoped
public class RegistrationController implements Serializable {
    
    private final static Logger LOG = LoggerFactory.getLogger(RegistrationController.class);
    @Inject
    UsersJpaController userJpaController;
    @Inject
    UserController userController;
    private Users newUser;
    private String password;
    private String passwordConfirm;
   
    
    /**
     * This class is used to register to the website.
     * @param rating
     * @param textReview
     * @return siteUrl
     * @throws java.lang.Exception
     */
    public String register(String rating, String textReview) throws Exception{
        HashUtil hu=new HashUtil();
        byte[]salt=hu.getSalt();
        this.newUser.setPasswordHash(hu.getHash(password, salt));
        this.newUser.setSalt(salt);
        this.newUser.setIsManager("false");
        try{
            this.userJpaController.create(newUser);
        }
        catch(RollbackFailureException e){
            LOG.debug(e.getMessage());
            return "singup.xhtml";
        }
        userController.setEmail(this.newUser.getEmail());
        userController.setPassword(this.password);
        if(!rating.equals("") && !textReview.equals("")){
            userController.logIn(rating,textReview);
            return "/bookpage.xhtml?faces-redirect=true&includeViewParams=true";
        }else{
            userController.logIn();
        }
        return "index.xhtml";
    }
    /**
     * This method is used to make sure the user enters the same password twice.
     * @param context
     * @param component
     * @param value 
     */
    public void validatePasswordError(FacesContext context, UIComponent component,Object value){
        String confirmPassword = (String) value;
        
        UIInput passwordInput = (UIInput) component.findComponent("pwd");
        String password = (String) passwordInput.getLocalValue();
        if(!password.equals(confirmPassword)||confirmPassword==null||password==null){
            String message = context.getApplication().evaluateExpressionGet(context, "#{msgs['pwdConfirmNoMatch']}", String.class);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message);
            throw new ValidatorException(msg);
        }
    }
    /**
     * Returns a new user if there is no created user.
     * @return User
     */
    public Users getNewUser() {
        if(this.newUser==null){
            this.newUser=new Users();
        }
        return newUser;
    }

    public void setNewUser(Users newUser) {
        this.newUser = newUser;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String paswordConfirm) {
        this.passwordConfirm = paswordConfirm;
    }

    public UsersJpaController getUserController() {
        return userJpaController;
    }

    public void setUserController(UsersJpaController userController) {
        this.userJpaController = userController;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
