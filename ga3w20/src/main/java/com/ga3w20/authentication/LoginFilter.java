package com.ga3w20.authentication;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Franco G. Moro 1738714
 * This filter is used to prevent user from access unauthenticated pages
 */
@WebFilter(filterName="LoginFilter")
public class LoginFilter implements Filter {
    private final static Logger LOG = LoggerFactory.getLogger(LoginFilter.class);
    @Inject
    private UserController userController;
    @Override
    public void doFilter(ServletRequest servletReq, ServletResponse servletResp, FilterChain fc) throws IOException, ServletException {
            
            HttpServletRequest request=(HttpServletRequest)servletReq;
            HttpServletResponse response =(HttpServletResponse) servletResp;
            //User sessionUser=(User)request.getSession().getAttribute("user");
            Object l=request.getSession().getAttribute("user");
            String url=request.getRequestURI();
            if(userController==null||!userController.isLogged()){
                //TODO:: add all possible cases where a user must be redirected for not being logged in.
                //If no user has been logged in, redirect to sign in.
                if(url.matches(".*/management/.*")||url.matches(".*/signout.xhtml")){
                    LOG.debug("going here in index LoginFilte and userController is null");
                    response.sendRedirect(request.getServletContext().getContextPath()+ "/signin.xhtml");
                }
                else{
                    fc.doFilter(servletReq, servletResp);
                }
            }
            else{
                
                // if user is already signed in.
                if(url.matches(".*/signin.xhtml")){
                    LOG.debug("going here in index LoginFilter");
                    response.sendRedirect(request.getServletContext().getContextPath()+ "/index.xhtml");
                }
                else if(url.matches(".*/signout.xhtml")){
                    LOG.debug("going here in Logout LoginFilter");
                    userController.logOut();
                    //request.getSession().removeAttribute("user");
                    response.sendRedirect(request.getServletContext().getContextPath()+ "/index.xhtml");
                }
                else if(url.matches(".*/management/.*")&& !userController.isManager()){
                    response.sendRedirect(request.getServletContext().getContextPath()+ "/signin.xhtml");
                }
                else{
                    fc.doFilter(servletReq, servletResp);
                }
            }
    }
    
}
