/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ga3w20.authentication;

import com.ga3w20.beans.ReviewBackingBean;
import com.ga3w20.beans.ShoppingCartBean;
import com.ga3w20.controllers.UsersJpaController;
import com.ga3w20.entities.Books;
import com.ga3w20.entities.Users;
import com.ga3w20.validation.HashUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Franco G. Moro 1738714
 */
@Named(value = "user")
@SessionScoped
public class UserController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(UserController.class);

    @PersistenceContext
    private EntityManager entityManager;
    @Inject
    private ReviewBackingBean reviewBean;
    @Inject
    private ShoppingCartBean shoppingCart;
    @Inject
    private UsersJpaController userJpa;
    private String email;
    private String password;
    private Integer userId = 0;
    private boolean isLogged = false;
    private boolean isManager = false;
    private List<Books> ownedBooks;

    private Users loggedUser;

    /**
     * This method is used to log in
     *
     * @return String
     */
    public String logIn() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Users> cq = cb.createQuery(Users.class);
        Root<Users> users = cq.from(Users.class);
        cq.select(users).where(cb.equal(users.get("email"), this.email));
        TypedQuery<Users> query = entityManager.createQuery(cq);
        List<Users> results = query.getResultList();
        if (results.size() == 0) {
            return "signin.xhtml";
        } else {
            HashUtil hu = new HashUtil();
            Users existingUser = results.get(0);
            setUserId(existingUser.getId());
            if (existingUser.getPasswordHash().equals(hu.getHash(this.password, existingUser.getSalt()))) {
                this.loggedUser = existingUser;
                this.isLogged = true;
                if (existingUser.getIsManager().equals("true")) {
                    this.isManager = true;
                } else {
                    this.isManager = false;
                }
                // Remove books that were already purchased from the cart
                if (!shoppingCart.isEmpty()) {
                    List<Books> purchasedBooks=userJpa.getUserBooks(this.loggedUser);
                    for(Books b: shoppingCart.getBooks()){
                        if(purchasedBooks.contains(b)){
                            shoppingCart.remove(b.getIsbn());
                        }
                    }
                    this.ownedBooks=purchasedBooks;
                }
            }
        }

        return "signin.xhtml";
    }

    /**
     * This log in method is used so that the reviews are kept within server
     * memory
     *
     * @author Alexander Berestetskyy
     * @author Franco G. Moro 1738714 (cleaned it up)
     * @param rating
     * @param textReview
     * @return
     * @throws Exception
     */
    public String logIn(String rating, String textReview) throws Exception {
        logIn();
        if (this.isLogged) {
            if (!rating.equals("") && !textReview.equals("")) {
                reviewBean.setEmail(this.email);
                reviewBean.createReview();
                return "/bookpage.xhtml?faces-redirect=true&includeViewParams=true";
            }
        } else {
            return "return.xhtml";
        }
        return "index.xhtml";
    }

    /**
     * This method is used to log out
     *
     * @return
     */
    public String logOut() {
        setUserId(0);
        if (!this.isLogged) {
            return "index.xhtml";
        }
        this.isLogged = false;
        this.email = "";
        this.password = "";
        this.loggedUser = null;
        this.ownedBooks=null;
        if (this.isManager) {
            this.isManager = false;
        }
        return "index.xhtml";
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Creates a new instance of User
     */
    public UserController() {
    }

    public Users getLoggedUser() {
        if (!this.isLogged) {
            return new Users();
        }
        return loggedUser;
    }

    public List<Books> getOwnedBooks() {
        if(this.ownedBooks == null){
            this.ownedBooks=userJpa.getUserBooks(loggedUser);
        }
        return ownedBooks;
    }

    public void setOwnedBooks(List<Books> ownedBooks) {
        this.ownedBooks = ownedBooks;
    }
    
    

    public void setLoggedUser(Users loggedUser) {
        this.loggedUser = loggedUser;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLogged() {
        return isLogged;
    }

    public void setIsLogged(boolean isLogged) {
        this.isLogged = isLogged;
    }

    public boolean isManager() {
        return isManager;
    }

    public void setIsManager(boolean isManager) {
        this.isManager = isManager;
    }

}
