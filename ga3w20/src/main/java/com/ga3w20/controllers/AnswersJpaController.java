package com.ga3w20.controllers;

import com.ga3w20.exceptions.NonexistentEntityException;
import com.ga3w20.entities.Answers;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.ga3w20.entities.Questions;
import com.ga3w20.exceptions.RollbackFailureException;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1738714
 * @author Michael Mishin 1612993
 */
@Named
@SessionScoped
public class AnswersJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(AnswersJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "ga3w20PU")
    private EntityManager em;
    
    public AnswersJpaController(){
    }

    public void create(Answers answers) throws RollbackFailureException {
        try {
            utx.begin();
            Questions questionId = answers.getQuestionId();
            if (questionId != null) {
                questionId = em.getReference(questionId.getClass(), questionId.getId());
                answers.setQuestionId(questionId);
            }
            em.persist(answers);
            if (questionId != null) {
                questionId.getAnswersList().add(answers);
                questionId = em.merge(questionId);
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

    public void edit(Answers answers) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Answers persistentAnswers = em.find(Answers.class, answers.getId());
            Questions questionIdOld = persistentAnswers.getQuestionId();
            Questions questionIdNew = answers.getQuestionId();
            if (questionIdNew != null) {
                questionIdNew = em.getReference(questionIdNew.getClass(), questionIdNew.getId());
                answers.setQuestionId(questionIdNew);
            }
            answers = em.merge(answers);
            if (questionIdOld != null && !questionIdOld.equals(questionIdNew)) {
                questionIdOld.getAnswersList().remove(answers);
                questionIdOld = em.merge(questionIdOld);
            }
            if (questionIdNew != null && !questionIdNew.equals(questionIdOld)) {
                questionIdNew.getAnswersList().add(answers);
                questionIdNew = em.merge(questionIdNew);
            }
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = answers.getId();
                if (findAnswers(id) == null) {
                    throw new NonexistentEntityException("The answers with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }    
    

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, HeuristicMixedException, HeuristicRollbackException, NotSupportedException, RollbackException, SystemException {
        try {
            utx.begin();
            Answers answers;
            try {
                answers = em.getReference(Answers.class, id);
                answers.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The answers with id " + id + " no longer exists.", enfe);
            }
            Questions questionId = answers.getQuestionId();
            if (questionId != null) {
                questionId.getAnswersList().remove(answers);
                questionId = em.merge(questionId);
            }
            em.remove(answers);
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Answers> findAnswersEntities() {
        return findAnswersEntities(true, -1, -1);
    }

    public List<Answers> findAnswersEntities(int maxResults, int firstResult) {
        return findAnswersEntities(false, maxResults, firstResult);
    }

    private List<Answers> findAnswersEntities(boolean all, int maxResults, int firstResult) {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Answers.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Answers findAnswers(Integer id) {
        return em.find(Answers.class, id);
    }

    public int getAnswersCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Answers> rt = cq.from(Answers.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
