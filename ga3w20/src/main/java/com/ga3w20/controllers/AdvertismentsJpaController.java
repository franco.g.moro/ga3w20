package com.ga3w20.controllers;

import com.ga3w20.exceptions.NonexistentEntityException;
import com.ga3w20.exceptions.PreexistingEntityException;
import com.ga3w20.entities.Advertisments;
import com.ga3w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1738714
 * @author Michael Mishin 1612993
 */
@Named
@SessionScoped
public class AdvertismentsJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(AdvertismentsJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "ga3w20PU")
    private EntityManager em;

    public AdvertismentsJpaController() {

    }

    public void create(Advertisments advertisments) throws PreexistingEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            em.persist(advertisments);
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback on create in Advertisments");
            } catch (IllegalStateException | SecurityException | SystemException e) {
                LOG.error("Rollback error");

                throw new RollbackFailureException("Advertisments " + advertisments + " already exists.", ex);
            }
        }

    }

    public void edit(Advertisments advertisments) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            advertisments = em.merge(advertisments);
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException e) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", e);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = advertisments.getId();
                if (findAdvertisments(id) == null) {
                    throw new NonexistentEntityException("The advertisments with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Advertisments advertisments;
            try {
                advertisments = em.getReference(Advertisments.class, id);
                advertisments.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The advertisments with id " + id + " no longer exists.", enfe);
            }
            em.remove(advertisments);
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException e) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException ex) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", ex);
            }
            throw e;
        }
    }

    public List<Advertisments> findAdvertismentsEntities() {
        return findAdvertismentsEntities(true, -1, -1);
    }

    public List<Advertisments> findAdvertismentsEntities(int maxResults, int firstResult) {
        return findAdvertismentsEntities(false, maxResults, firstResult);
    }

    private List<Advertisments> findAdvertismentsEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Advertisments.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Advertisments findAdvertisments(Integer id) {
        return em.find(Advertisments.class, id);
    }

    public int getAdvertismentsCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Advertisments> rt = cq.from(Advertisments.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    /**
     * Activate ad for with matching id parameter
     * while deactivating the rest
     * 
     * @author Hadil Elhashani
     * @param adId
     * @return 
     */
    public Advertisments activateAdvertisment(Integer adId){
        try {
            utx.begin();
        } catch (NotSupportedException | SystemException ex) {
            java.util.logging.Logger.getLogger(QuestionsJpaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        Advertisments ad = em.find(Advertisments.class, adId);
        this.findAdvertismentsEntities().forEach(adv -> adv.setIsActive(false));
        ad.setIsActive(true);
        try {
            utx.commit();
        } catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException | SystemException ex) {
            java.util.logging.Logger.getLogger(QuestionsJpaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ad;
    }

}
