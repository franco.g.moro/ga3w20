package com.ga3w20.controllers;

import com.ga3w20.exceptions.NonexistentEntityException;
import com.ga3w20.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.ga3w20.entities.Books;
import com.ga3w20.entities.Reviews;
import com.ga3w20.entities.Users;
import com.ga3w20.exceptions.IllegalOrphanException;
import com.ga3w20.exceptions.RollbackFailureException;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1738714
 * @author Hadil Elhashani
 */
@Named
@SessionScoped
public class ReviewsJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(ReviewsJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "ga3w20PU")
    private EntityManager em;

    public ReviewsJpaController() {
    }

    public void create(Reviews reviews) throws PreexistingEntityException, Exception {
        try {
            utx.begin();
            Books isbn = reviews.getIsbn();
            if (isbn != null) {
                isbn = em.getReference(isbn.getClass(), isbn.getIsbn());
                reviews.setIsbn(isbn);
            }
            Users userId = reviews.getUserId();
            if (userId != null) {
                userId = em.getReference(userId.getClass(), userId.getId());
                reviews.setUserId(userId);
            }
            em.persist(reviews);
            if (isbn != null) {
                isbn.getReviewsList().add(reviews);
                isbn = em.merge(isbn);
            }
            if (userId != null) {
                userId.getReviewsList().add(reviews);
                userId = em.merge(userId);
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

    public void edit(Reviews reviews) throws NonexistentEntityException, Exception {
        try {
            utx.begin();
            Reviews persistentReviews = em.find(Reviews.class, reviews.getId());
            Books isbnOld = persistentReviews.getIsbn();
            Books isbnNew = reviews.getIsbn();
            Users userIdOld = persistentReviews.getUserId();
            Users userIdNew = reviews.getUserId();
            if (isbnNew != null) {
                isbnNew = em.getReference(isbnNew.getClass(), isbnNew.getIsbn());
                reviews.setIsbn(isbnNew);
            }
            if (userIdNew != null) {
                userIdNew = em.getReference(userIdNew.getClass(), userIdNew.getId());
                reviews.setUserId(userIdNew);
            }
            reviews = em.merge(reviews);
            if (isbnOld != null && !isbnOld.equals(isbnNew)) {
                isbnOld.getReviewsList().remove(reviews);
                isbnOld = em.merge(isbnOld);
            }
            if (isbnNew != null && !isbnNew.equals(isbnOld)) {
                isbnNew.getReviewsList().add(reviews);
                isbnNew = em.merge(isbnNew);
            }
            if (userIdOld != null && !userIdOld.equals(userIdNew)) {
                userIdOld.getReviewsList().remove(reviews);
                userIdOld = em.merge(userIdOld);
            }
            if (userIdNew != null && !userIdNew.equals(userIdOld)) {
                userIdNew.getReviewsList().add(reviews);
                userIdNew = em.merge(userIdNew);
            }
            utx.commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = reviews.getId();
                if (findReviews(id) == null) {
                    throw new NonexistentEntityException("The reviews with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            Reviews reviews;
            try {
                reviews = em.getReference(Reviews.class, id);
                reviews.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The reviews with id " + id + " no longer exists.", enfe);
            }
            Books isbn = reviews.getIsbn();
            if (isbn != null) {
                isbn.getReviewsList().remove(reviews);
                isbn = em.merge(isbn);
            }
            Users userId = reviews.getUserId();
            if (userId != null) {
                userId.getReviewsList().remove(reviews);
                userId = em.merge(userId);
            }
            em.remove(reviews);
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Reviews> findReviewsEntities() {
        return findReviewsEntities(true, -1, -1);
    }

    public List<Reviews> findReviewsEntities(int maxResults, int firstResult) {
        return findReviewsEntities(false, maxResults, firstResult);
    }

    private List<Reviews> findReviewsEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Reviews.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Reviews findReviews(Integer id) {
        return em.find(Reviews.class, id);
    }

    public int getReviewsCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Reviews> rt = cq.from(Reviews.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    /**
     * 
     * @author Hadil Elhashani
     * @param id
     * @param status
     * @return 
     */
    public Reviews approveReview(String id, String status){
        try {
            utx.begin();
        } catch (NotSupportedException | SystemException ex) {
            java.util.logging.Logger.getLogger(ReviewsJpaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Reviews persistentReviews = em.find(Reviews.class, Integer.parseInt(id));
        persistentReviews.setIsApproved(status.equals("true")? "false" : "true");
        
        try {
            utx.commit();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful", "Review edited: " + persistentReviews.getReviewText()));
        } catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException | SystemException ex) {
            java.util.logging.Logger.getLogger(ReviewsJpaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return persistentReviews;
    }
    
    /**
     * Retrieving the number of ids in Reviews entity
     * 
     * @return int representing the number of ids in Reviews entity
     * @author Alexander Berestetskyy
     */
    public int getMaxIdOfReviews() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery q = em.getCriteriaBuilder().createQuery();
        Root<Reviews> review = q.from(Reviews.class);
        q.select(cb.count(review));
        TypedQuery<Long> query = em.createQuery(q);
        Long maxId = query.getSingleResult();
        return maxId.intValue();
    }
}
