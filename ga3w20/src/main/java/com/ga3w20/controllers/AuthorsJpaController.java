package com.ga3w20.controllers;

import com.ga3w20.exceptions.IllegalOrphanException;
import com.ga3w20.exceptions.NonexistentEntityException;
import com.ga3w20.entities.Authors;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.ga3w20.entities.Books;
import com.ga3w20.exceptions.RollbackFailureException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1738714
 * @author Hadil Elhashani
 */
@Named
@SessionScoped
public class AuthorsJpaController implements Serializable {
    private final static Logger LOG = LoggerFactory.getLogger(AuthorsJpaController.class);
    
    private Authors author;

    public Authors getAuthor() {
        if (author == null) {
            author = new Authors();
        }
        return author;
    }

    public void setAuthor(Authors author) {
        this.author = author;
    }
    
    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "ga3w20PU")
    private EntityManager em;
    
    public AuthorsJpaController() {
    }
    
    public void create(Authors authors) throws RollbackFailureException {
        if (authors.getBooksList() == null) {
            authors.setBooksList(new ArrayList<Books>());
        }
        try {
            utx.begin();
            List<Books> attachedBooksList = new ArrayList<Books>();
            for (Books booksListBooksToAttach : authors.getBooksList()) {
                booksListBooksToAttach = em.getReference(booksListBooksToAttach.getClass(), booksListBooksToAttach.getIsbn());
                attachedBooksList.add(booksListBooksToAttach);
            }
            authors.setBooksList(attachedBooksList);
            em.persist(authors);
            for (Books booksListBooks : authors.getBooksList()) {
                Authors oldAuthorIdOfBooksListBooks = booksListBooks.getAuthorId();
                booksListBooks.setAuthorId(authors);
                booksListBooks = em.merge(booksListBooks);
                if (oldAuthorIdOfBooksListBooks != null) {
                    oldAuthorIdOfBooksListBooks.getBooksList().remove(booksListBooks);
                    oldAuthorIdOfBooksListBooks = em.merge(oldAuthorIdOfBooksListBooks);
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

    public void edit(Authors authors) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            Authors persistentAuthors = em.find(Authors.class, authors.getId());
            List<Books> booksListOld = persistentAuthors.getBooksList();
            List<Books> booksListNew = authors.getBooksList();
            List<String> illegalOrphanMessages = null;
            for (Books booksListOldBooks : booksListOld) {
                if (!booksListNew.contains(booksListOldBooks)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Books " + booksListOldBooks + " since its authorId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Books> attachedBooksListNew = new ArrayList<Books>();
            for (Books booksListNewBooksToAttach : booksListNew) {
                booksListNewBooksToAttach = em.getReference(booksListNewBooksToAttach.getClass(), booksListNewBooksToAttach.getIsbn());
                attachedBooksListNew.add(booksListNewBooksToAttach);
            }
            booksListNew = attachedBooksListNew;
            authors.setBooksList(booksListNew);
            authors = em.merge(authors);
            for (Books booksListNewBooks : booksListNew) {
                if (!booksListOld.contains(booksListNewBooks)) {
                    Authors oldAuthorIdOfBooksListNewBooks = booksListNewBooks.getAuthorId();
                    booksListNewBooks.setAuthorId(authors);
                    booksListNewBooks = em.merge(booksListNewBooks);
                    if (oldAuthorIdOfBooksListNewBooks != null && !oldAuthorIdOfBooksListNewBooks.equals(authors)) {
                        oldAuthorIdOfBooksListNewBooks.getBooksList().remove(booksListNewBooks);
                        oldAuthorIdOfBooksListNewBooks = em.merge(oldAuthorIdOfBooksListNewBooks);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = authors.getId();
                if (findAuthors(id) == null) {
                    throw new NonexistentEntityException("The authors with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } 
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception{
        try {
            utx.begin();
            Authors authors;
            try {
                authors = em.getReference(Authors.class, id);
                authors.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The authors with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Books> booksListOrphanCheck = authors.getBooksList();
            for (Books booksListOrphanCheckBooks : booksListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Authors (" + authors + ") cannot be destroyed since the Books " + booksListOrphanCheckBooks + " in its booksList field has a non-nullable authorId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(authors);
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Authors> findAuthorsEntities() {
        return findAuthorsEntities(true, -1, -1);
    }

    public List<Authors> findAuthorsEntities(int maxResults, int firstResult) {
        return findAuthorsEntities(false, maxResults, firstResult);
    }

    private List<Authors> findAuthorsEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Authors.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Authors findAuthors(Integer id) {
        return em.find(Authors.class, id);
    }

    /**
     * Retrieving the number of authors found in db
     * 
     * @return int representing the number of authors found
     * @author Alexander Berestetskyy
     */
    public int getAuthorsCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Authors> rt = cq.from(Authors.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
}
