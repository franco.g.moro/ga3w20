package com.ga3w20.controllers;

import com.ga3w20.exceptions.NonexistentEntityException;
import com.ga3w20.exceptions.PreexistingEntityException;
import com.ga3w20.entities.Province;
import com.ga3w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.ParameterExpression;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Franco G. Moro1738714
 * @author 1738714 modified by Yongchao
 */
@Named
@SessionScoped
public class ProvinceJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(NewsFeedJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "ga3w20PU")
    private EntityManager em;

    public ProvinceJpaController() {
    }

    public void create(Province province) throws PreexistingEntityException, Exception {
        try {
            utx.begin();
            em.persist(province);
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

    public void edit(Province province) throws NonexistentEntityException, Exception  {
       try {
            utx.begin();
            province = em.merge(province);
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = province.getId();
                if (findProvince(id) == null) {
                    throw new NonexistentEntityException("The newsFeed with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(int id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Province province;
            try {
                province = em.getReference(Province.class, id);
                province.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The newsFeed with id " + id + " no longer exists.", enfe);
            }
            em.remove(province);
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Province> findProvinceEntities() {
        return findProvinceEntities(true, -1, -1);
    }

    public List<Province> findProvinceEntities(int maxResults, int firstResult) {
        return findProvinceEntities(false, maxResults, firstResult);
    }

    private List<Province> findProvinceEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Province.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Province findProvince(int id) {
        return em.find(Province.class, id);
    }
    
    public List<Province> findProvinceByName(String name) {
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Province> q = cb.createQuery(Province.class);
        Root<Province> provinces = q.from(Province.class);
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(provinces).where(cb.like(provinces.get("name"), t));
        
        TypedQuery<Province> query = em.createQuery(q);
        query.setParameter(t, "%"+name+"%");
        List<Province> provinceList = query.getResultList();
        return provinceList;
    }

    public int getProvinceCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Province> rt = cq.from(Province.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
