/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ga3w20.controllers;

import com.ga3w20.exceptions.NonexistentEntityException;
import com.ga3w20.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.ga3w20.entities.Books;
import com.ga3w20.entities.Format;
import com.ga3w20.exceptions.IllegalOrphanException;
import com.ga3w20.exceptions.RollbackFailureException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1738714 Yongchao
 */
@Named
@SessionScoped
public class FormatJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(FormatJpaController.class);

    private Format format;

    

    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "ga3w20PU")
    private EntityManager em;

    public FormatJpaController() {
    }
    public Format getFormat() {
        if (format == null) {
            format = new Format();
        }
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

    public void create(Format format) throws RollbackFailureException {
        if (format.getBooksList() == null) {
            format.setBooksList(new ArrayList<Books>());
        }
        
        try {
            utx.begin();
            List<Books> attachedBooksList = new ArrayList<Books>();
            for (Books booksListBooksToAttach : format.getBooksList()) {
                booksListBooksToAttach = em.getReference(booksListBooksToAttach.getClass(), booksListBooksToAttach.getIsbn());
                attachedBooksList.add(booksListBooksToAttach);
            }
            format.setBooksList(attachedBooksList);
            em.persist(format);
            for (Books booksListBooks : format.getBooksList()) {
                booksListBooks.getFormatList().add(format);
                booksListBooks = em.merge(booksListBooks);
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

    public void edit(Format format) throws IllegalOrphanException, NonexistentEntityException, Exception {
        
        try {
            utx.begin();
            Format persistentFormat = em.find(Format.class, format.getId());
            List<Books> booksListOld = persistentFormat.getBooksList();
            List<Books> booksListNew = format.getBooksList();
            List<Books> attachedBooksListNew = new ArrayList<Books>();
            for (Books booksListNewBooksToAttach : booksListNew) {
                booksListNewBooksToAttach = em.getReference(booksListNewBooksToAttach.getClass(), booksListNewBooksToAttach.getIsbn());
                attachedBooksListNew.add(booksListNewBooksToAttach);
            }
            booksListNew = attachedBooksListNew;
            format.setBooksList(booksListNew);
            format = em.merge(format);
            for (Books booksListOldBooks : booksListOld) {
                if (!booksListNew.contains(booksListOldBooks)) {
                    booksListOldBooks.getFormatList().remove(format);
                    booksListOldBooks = em.merge(booksListOldBooks);
                }
            }
            for (Books booksListNewBooks : booksListNew) {
                if (!booksListOld.contains(booksListNewBooks)) {
                    booksListNewBooks.getFormatList().add(format);
                    booksListNewBooks = em.merge(booksListNewBooks);
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        
        try {
            utx.begin();
            Format format;
            try {
                format = em.getReference(Format.class, id);
                format.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The format with id " + id + " no longer exists.", enfe);
            }
            List<Books> booksList = format.getBooksList();
            for (Books booksListBooks : booksList) {
                booksListBooks.getFormatList().remove(format);
                booksListBooks = em.merge(booksListBooks);
            }
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Format> findFormatEntities() {
        return findFormatEntities(true, -1, -1);
    }

    public List<Format> findFormatEntities(int maxResults, int firstResult) {
        return findFormatEntities(false, maxResults, firstResult);
    }

    private List<Format> findFormatEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Format.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Format findFormat(Integer id) {
        return em.find(Format.class, id);
    }

    public int getFormatCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Format> rt = cq.from(Format.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
}
