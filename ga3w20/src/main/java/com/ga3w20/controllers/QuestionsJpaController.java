package com.ga3w20.controllers;

import com.ga3w20.exceptions.IllegalOrphanException;
import com.ga3w20.exceptions.NonexistentEntityException;
import com.ga3w20.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.ga3w20.entities.Answers;
import com.ga3w20.entities.Questions;
import com.ga3w20.exceptions.RollbackFailureException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1738714
 * @author Michael Mishin 1612993
 * @author Hadil Elhashani
 */
@Named
@SessionScoped
public class QuestionsJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(QuestionsJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "ga3w20PU")
    private EntityManager em;

    public QuestionsJpaController() {
    }

    public void create(Questions questions) throws PreexistingEntityException, Exception {
        if (questions.getAnswersList() == null) {
            questions.setAnswersList(new ArrayList<Answers>());
        }
        try {
            utx.begin();
            List<Answers> attachedAnswersList = new ArrayList<Answers>();
            for (Answers answersListAnswersToAttach : questions.getAnswersList()) {
                answersListAnswersToAttach = em.getReference(answersListAnswersToAttach.getClass(), answersListAnswersToAttach.getId());
                attachedAnswersList.add(answersListAnswersToAttach);
            }
            questions.setAnswersList(attachedAnswersList);
            em.persist(questions);
            for (Answers answersListAnswers : questions.getAnswersList()) {
                Questions oldQuestionIdOfAnswersListAnswers = answersListAnswers.getQuestionId();
                answersListAnswers.setQuestionId(questions);
                answersListAnswers = em.merge(answersListAnswers);
                if (oldQuestionIdOfAnswersListAnswers != null) {
                    oldQuestionIdOfAnswersListAnswers.getAnswersList().remove(answersListAnswers);
                    oldQuestionIdOfAnswersListAnswers = em.merge(oldQuestionIdOfAnswersListAnswers);
                }
            }
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

    public void edit(Questions questions) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            Questions persistentQuestions = em.find(Questions.class, questions.getId());
            List<Answers> answersListOld = persistentQuestions.getAnswersList();
            List<Answers> answersListNew = questions.getAnswersList();
            List<String> illegalOrphanMessages = null;
            for (Answers answersListOldAnswers : answersListOld) {
                if (!answersListNew.contains(answersListOldAnswers)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Answers " + answersListOldAnswers + " since its questionId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Answers> attachedAnswersListNew = new ArrayList<Answers>();
            for (Answers answersListNewAnswersToAttach : answersListNew) {
                answersListNewAnswersToAttach = em.getReference(answersListNewAnswersToAttach.getClass(), answersListNewAnswersToAttach.getId());
                attachedAnswersListNew.add(answersListNewAnswersToAttach);
            }
            answersListNew = attachedAnswersListNew;
            questions.setAnswersList(answersListNew);
            questions = em.merge(questions);
            for (Answers answersListNewAnswers : answersListNew) {
                if (!answersListOld.contains(answersListNewAnswers)) {
                    Questions oldQuestionIdOfAnswersListNewAnswers = answersListNewAnswers.getQuestionId();
                    answersListNewAnswers.setQuestionId(questions);
                    answersListNewAnswers = em.merge(answersListNewAnswers);
                    if (oldQuestionIdOfAnswersListNewAnswers != null && !oldQuestionIdOfAnswersListNewAnswers.equals(questions)) {
                        oldQuestionIdOfAnswersListNewAnswers.getAnswersList().remove(answersListNewAnswers);
                        oldQuestionIdOfAnswersListNewAnswers = em.merge(oldQuestionIdOfAnswersListNewAnswers);
                    }
                }
            }
            utx.commit();
        } catch (IllegalOrphanException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = questions.getId();
                if (findQuestions(id) == null) {
                    throw new NonexistentEntityException("The questions with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            Questions questions;
            try {
                questions = em.getReference(Questions.class, id);
                questions.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The questions with id " + id + " no longer exists.", enfe);
            }
            //List<String> illegalOrphanMessages = null;
            List<Answers> answersListOrphanCheck = questions.getAnswersList();
//            for (Answers answersListOrphanCheckAnswers : answersListOrphanCheck) {
//                if (illegalOrphanMessages == null) {
//                    illegalOrphanMessages = new ArrayList<String>();
//                }
//                illegalOrphanMessages.add("This Questions (" + questions + ") cannot be destroyed since the Answers " + answersListOrphanCheckAnswers + " in its answersList field has a non-nullable questionId field.");
//            }
//            if (illegalOrphanMessages != null) {
//                throw new IllegalOrphanException(illegalOrphanMessages);
//            }
            
            for (Answers a: answersListOrphanCheck)      {
                em.remove(a);
            }       
            em.remove(questions);
            utx.commit();
        } catch ( NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Questions> findQuestionsEntities() {
        return findQuestionsEntities(true, -1, -1);
    }

    public List<Questions> findQuestionsEntities(int maxResults, int firstResult) {
        return findQuestionsEntities(false, maxResults, firstResult);
    }

    private List<Questions> findQuestionsEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Questions.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Questions findQuestions(Integer id) {
        return em.find(Questions.class, id);
    }

    public int getQuestionsCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Questions> rt = cq.from(Questions.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    /**
     * Activate survey question with matching id parameter 
     * while deactivating the rest 
     * 
     * @author Hadil Elhashani
     * @param quesId
     * @return 
     */
    public Questions activateQuestion(String quesId){
        try {
            utx.begin();
        } catch (NotSupportedException | SystemException ex) {
            java.util.logging.Logger.getLogger(QuestionsJpaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        int id = Integer.parseInt(quesId);
        Questions question = em.find(Questions.class, id);
        
        findQuestionsEntities().forEach(q -> q.setIsActive(false));
        question.setIsActive(true);
        
        try {
            utx.commit();
        } catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException | SystemException ex) {
            java.util.logging.Logger.getLogger(QuestionsJpaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return question;
    }
    
    /**
     * Retrieves the question that is active
     * 
     * @author Hadil Elhashani
     * @return 
     */
    public Questions getActiveQuestion(){
        List<Questions> questions = this.findQuestionsEntities();
        Questions ques = questions.get(0);
        
        for(Questions q: questions){
            if(q.getIsActive()){
                ques = q;
                break;
            }
        }
        return ques;
    }

}
