package com.ga3w20.controllers;

import com.ga3w20.exceptions.NonexistentEntityException;
import com.ga3w20.exceptions.PreexistingEntityException;
import com.ga3w20.entities.NewsFeed;
import com.ga3w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1738714
 * @author Michael Mishin 1612993
 */
@Named
@SessionScoped
public class NewsFeedJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(NewsFeedJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "ga3w20PU")
    private EntityManager em;

    public NewsFeedJpaController() {
    }

    public void create(NewsFeed newsFeed) throws PreexistingEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            em.persist(newsFeed);
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

    public void edit(NewsFeed newsFeed) throws NonexistentEntityException, Exception {
        try {
            utx.begin();
            newsFeed = em.merge(newsFeed);
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = newsFeed.getId();
                if (findNewsFeed(id) == null) {
                    throw new NonexistentEntityException("The newsFeed with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            NewsFeed newsFeed;
            try {
                newsFeed = em.getReference(NewsFeed.class, id);
                newsFeed.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The newsFeed with id " + id + " no longer exists.", enfe);
            }
            em.remove(newsFeed);
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<NewsFeed> findNewsFeedEntities() {
        return findNewsFeedEntities(true, -1, -1);
    }

    public List<NewsFeed> findNewsFeedEntities(int maxResults, int firstResult) {
        return findNewsFeedEntities(false, maxResults, firstResult);
    }

    private List<NewsFeed> findNewsFeedEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(NewsFeed.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public NewsFeed findNewsFeed(Integer id) {
        return em.find(NewsFeed.class, id);
    }

    public int getNewsFeedCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<NewsFeed> rt = cq.from(NewsFeed.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    /**
     * Activate news feed with matching id input and deactivating the rest
     *
     * @author Hadil Elhashani
     * @param newsId
     * @return
     */
    public void activateNewsFeed(Integer newsId) {
        NewsFeed news = null;
        try {
            utx.begin();
            //int id = Integer.parseInt(newsId);
            news = em.find(NewsFeed.class, newsId);

            findNewsFeedEntities().forEach(newsfeed -> newsfeed.setIsActive(false));
            news.setIsActive(true);
            utx.commit();

        } catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException | SystemException ex) {
            java.util.logging.Logger.getLogger(QuestionsJpaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotSupportedException ex) {
            java.util.logging.Logger.getLogger(NewsFeedJpaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

}
