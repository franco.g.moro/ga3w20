package com.ga3w20.controllers;

import com.ga3w20.entities.Books;
import com.ga3w20.exceptions.IllegalOrphanException;
import com.ga3w20.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.ga3w20.entities.Sales;
import java.util.ArrayList;
import java.util.List;
import com.ga3w20.entities.Reviews;
import com.ga3w20.entities.SoldBooks;
import com.ga3w20.entities.Users;
import com.ga3w20.exceptions.RollbackFailureException;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.ParameterExpression;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1738714
 * @author Hadil Elhashani Yongchao
 */
@Named
@SessionScoped
public class UsersJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(UsersJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "ga3w20PU")
    private EntityManager em;

    public UsersJpaController() {
    }

    public void create(Users users) throws RollbackFailureException {
        if (users.getSalesList() == null) {
            users.setSalesList(new ArrayList<Sales>());
        }
//        if (users.getInvoicesList() == null) {
//            users.setInvoicesList(new ArrayList<Invoices>());
//        }
        if (users.getReviewsList() == null) {
            users.setReviewsList(new ArrayList<Reviews>());
        }
        try {
            utx.begin();
//            Province provinceName = users.getProvinceName();
//            if (provinceName != null) {
//                provinceName = em.getReference(provinceName.getClass(), provinceName.getName());
//                users.setProvinceName(provinceName);
//            }
            List<Sales> attachedSalesList = new ArrayList<Sales>();
            for (Sales salesListSalesToAttach : users.getSalesList()) {
                salesListSalesToAttach = em.getReference(salesListSalesToAttach.getClass(), salesListSalesToAttach.getId());
                attachedSalesList.add(salesListSalesToAttach);
            }
            users.setSalesList(attachedSalesList);
//            List<Invoices> attachedInvoicesList = new ArrayList<Invoices>();
//            for (Invoices invoicesListInvoicesToAttach : users.getInvoicesList()) {
//                invoicesListInvoicesToAttach = em.getReference(invoicesListInvoicesToAttach.getClass(), invoicesListInvoicesToAttach.getId());
//                attachedInvoicesList.add(invoicesListInvoicesToAttach);
//            }
//            users.setInvoicesList(attachedInvoicesList);
            List<Reviews> attachedReviewsList = new ArrayList<Reviews>();
            for (Reviews reviewsListReviewsToAttach : users.getReviewsList()) {
                reviewsListReviewsToAttach = em.getReference(reviewsListReviewsToAttach.getClass(), reviewsListReviewsToAttach.getId());
                attachedReviewsList.add(reviewsListReviewsToAttach);
            }
            users.setReviewsList(attachedReviewsList);
            em.persist(users);
//            if (provinceName != null) {
//                provinceName.getUsersList().add(users);
//                provinceName = em.merge(provinceName);
//            }
            for (Sales salesListSales : users.getSalesList()) {
                Users oldUsersIdOfSalesListSales = salesListSales.getUsersId();
                salesListSales.setUsersId(users);
                salesListSales = em.merge(salesListSales);
                if (oldUsersIdOfSalesListSales != null) {
                    oldUsersIdOfSalesListSales.getSalesList().remove(salesListSales);
                    oldUsersIdOfSalesListSales = em.merge(oldUsersIdOfSalesListSales);
                }
            }
//            for (Invoices invoicesListInvoices : users.getInvoicesList()) {
//                Users oldUsersIdOfInvoicesListInvoices = invoicesListInvoices.getUsersId();
//                invoicesListInvoices.setUsersId(users);
//                invoicesListInvoices = em.merge(invoicesListInvoices);
//                if (oldUsersIdOfInvoicesListInvoices != null) {
//                    oldUsersIdOfInvoicesListInvoices.getInvoicesList().remove(invoicesListInvoices);
//                    oldUsersIdOfInvoicesListInvoices = em.merge(oldUsersIdOfInvoicesListInvoices);
//                }
//            }
            for (Reviews reviewsListReviews : users.getReviewsList()) {
                Users oldUserIdOfReviewsListReviews = reviewsListReviews.getUserId();
                reviewsListReviews.setUserId(users);
                reviewsListReviews = em.merge(reviewsListReviews);
                if (oldUserIdOfReviewsListReviews != null) {
                    oldUserIdOfReviewsListReviews.getReviewsList().remove(reviewsListReviews);
                    oldUserIdOfReviewsListReviews = em.merge(oldUserIdOfReviewsListReviews);
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

    public void edit(Users users) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            Users persistentUsers = em.find(Users.class, users.getId());
//            Province provinceNameOld = persistentUsers.getProvinceName();
//            Province provinceNameNew = users.getProvinceName();
            List<Sales> salesListOld = persistentUsers.getSalesList();
            List<Sales> salesListNew = users.getSalesList();
//            List<Invoices> invoicesListOld = persistentUsers.getInvoicesList();
//            List<Invoices> invoicesListNew = users.getInvoicesList();
            List<Reviews> reviewsListOld = persistentUsers.getReviewsList();
            List<Reviews> reviewsListNew = users.getReviewsList();
            List<String> illegalOrphanMessages = null;
            for (Sales salesListOldSales : salesListOld) {
                if (!salesListNew.contains(salesListOldSales)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Sales " + salesListOldSales + " since its usersId field is not nullable.");
                }
            }
//            for (Invoices invoicesListOldInvoices : invoicesListOld) {
//                if (!invoicesListNew.contains(invoicesListOldInvoices)) {
//                    if (illegalOrphanMessages == null) {
//                        illegalOrphanMessages = new ArrayList<String>();
//                    }
//                    illegalOrphanMessages.add("You must retain Invoices " + invoicesListOldInvoices + " since its usersId field is not nullable.");
//                }
//            }
            for (Reviews reviewsListOldReviews : reviewsListOld) {
                if (!reviewsListNew.contains(reviewsListOldReviews)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Reviews " + reviewsListOldReviews + " since its userId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
//            if (provinceNameNew != null) {
//                provinceNameNew = em.getReference(provinceNameNew.getClass(), provinceNameNew.getName());
//                users.setProvinceName(provinceNameNew);
//            }
            List<Sales> attachedSalesListNew = new ArrayList<Sales>();
            for (Sales salesListNewSalesToAttach : salesListNew) {
                salesListNewSalesToAttach = em.getReference(salesListNewSalesToAttach.getClass(), salesListNewSalesToAttach.getId());
                attachedSalesListNew.add(salesListNewSalesToAttach);
            }
            salesListNew = attachedSalesListNew;
            users.setSalesList(salesListNew);
//            List<Invoices> attachedInvoicesListNew = new ArrayList<Invoices>();
//            for (Invoices invoicesListNewInvoicesToAttach : invoicesListNew) {
//                invoicesListNewInvoicesToAttach = em.getReference(invoicesListNewInvoicesToAttach.getClass(), invoicesListNewInvoicesToAttach.getId());
//                attachedInvoicesListNew.add(invoicesListNewInvoicesToAttach);
//            }
//            invoicesListNew = attachedInvoicesListNew;
//            users.setInvoicesList(invoicesListNew);
            List<Reviews> attachedReviewsListNew = new ArrayList<Reviews>();
            for (Reviews reviewsListNewReviewsToAttach : reviewsListNew) {
                reviewsListNewReviewsToAttach = em.getReference(reviewsListNewReviewsToAttach.getClass(), reviewsListNewReviewsToAttach.getId());
                attachedReviewsListNew.add(reviewsListNewReviewsToAttach);
            }
            reviewsListNew = attachedReviewsListNew;
            users.setReviewsList(reviewsListNew);
            users = em.merge(users);
//            if (provinceNameOld != null && !provinceNameOld.equals(provinceNameNew)) {
//                provinceNameOld.getUsersList().remove(users);
//                provinceNameOld = em.merge(provinceNameOld);
//            }
//            if (provinceNameNew != null && !provinceNameNew.equals(provinceNameOld)) {
//                provinceNameNew.getUsersList().add(users);
//                provinceNameNew = em.merge(provinceNameNew);
//            }
            for (Sales salesListNewSales : salesListNew) {
                if (!salesListOld.contains(salesListNewSales)) {
                    Users oldUsersIdOfSalesListNewSales = salesListNewSales.getUsersId();
                    salesListNewSales.setUsersId(users);
                    salesListNewSales = em.merge(salesListNewSales);
                    if (oldUsersIdOfSalesListNewSales != null && !oldUsersIdOfSalesListNewSales.equals(users)) {
                        oldUsersIdOfSalesListNewSales.getSalesList().remove(salesListNewSales);
                        oldUsersIdOfSalesListNewSales = em.merge(oldUsersIdOfSalesListNewSales);
                    }
                }
            }
//            for (Invoices invoicesListNewInvoices : invoicesListNew) {
//                if (!invoicesListOld.contains(invoicesListNewInvoices)) {
//                    Users oldUsersIdOfInvoicesListNewInvoices = invoicesListNewInvoices.getUsersId();
//                    invoicesListNewInvoices.setUsersId(users);
//                    invoicesListNewInvoices = em.merge(invoicesListNewInvoices);
//                    if (oldUsersIdOfInvoicesListNewInvoices != null && !oldUsersIdOfInvoicesListNewInvoices.equals(users)) {
//                        oldUsersIdOfInvoicesListNewInvoices.getInvoicesList().remove(invoicesListNewInvoices);
//                        oldUsersIdOfInvoicesListNewInvoices = em.merge(oldUsersIdOfInvoicesListNewInvoices);
//                    }
//                }
//            }
            for (Reviews reviewsListNewReviews : reviewsListNew) {
                if (!reviewsListOld.contains(reviewsListNewReviews)) {
                    Users oldUserIdOfReviewsListNewReviews = reviewsListNewReviews.getUserId();
                    reviewsListNewReviews.setUserId(users);
                    reviewsListNewReviews = em.merge(reviewsListNewReviews);
                    if (oldUserIdOfReviewsListNewReviews != null && !oldUserIdOfReviewsListNewReviews.equals(users)) {
                        oldUserIdOfReviewsListNewReviews.getReviewsList().remove(reviewsListNewReviews);
                        oldUserIdOfReviewsListNewReviews = em.merge(oldUserIdOfReviewsListNewReviews);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = users.getId();
                if (findUsers(id) == null) {
                    throw new NonexistentEntityException("The users with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Users users;
            try {
                users = em.getReference(Users.class, id);
                users.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The users with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Sales> salesListOrphanCheck = users.getSalesList();
            for (Sales salesListOrphanCheckSales : salesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the Sales " + salesListOrphanCheckSales + " in its salesList field has a non-nullable usersId field.");
            }
//            List<Invoices> invoicesListOrphanCheck = users.getInvoicesList();
//            for (Invoices invoicesListOrphanCheckInvoices : invoicesListOrphanCheck) {
//                if (illegalOrphanMessages == null) {
//                    illegalOrphanMessages = new ArrayList<String>();
//                }
//                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the Invoices " + invoicesListOrphanCheckInvoices + " in its invoicesList field has a non-nullable usersId field.");
//            }
            List<Reviews> reviewsListOrphanCheck = users.getReviewsList();
            for (Reviews reviewsListOrphanCheckReviews : reviewsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the Reviews " + reviewsListOrphanCheckReviews + " in its reviewsList field has a non-nullable userId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
//            Province provinceName = users.getProvinceName();
//            if (provinceName != null) {
//                provinceName.getUsersList().remove(users);
//                provinceName = em.merge(provinceName);
//            }
            em.remove(users);
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Users> findUsersByFirstName(String firstName) {
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> q = cb.createQuery(Users.class);
        Root<Users> users = q.from(Users.class);
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(users).where(cb.like(users.get("firstName"), t));
        TypedQuery<Users> query = em.createQuery(q);
        query.setParameter(t, "%"+firstName+"%");
        List<Users> usersList = query.getResultList();
        return usersList;
    }
    public List<Users> findUsersByLastName(String lastName) {
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> q = cb.createQuery(Users.class);
        Root<Users> users = q.from(Users.class);
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(users).where(cb.like(users.get("lastName"), t));
        TypedQuery<Users> query = em.createQuery(q);
        query.setParameter(t, "%"+lastName+"%");
        List<Users> usersList = query.getResultList();
        return usersList;
    }
    
    public List<Users> findUsersByCompany(String companyName) {
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> q = cb.createQuery(Users.class);
        Root<Users> users = q.from(Users.class);
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(users).where(cb.like(users.get("companyName"), t));
        TypedQuery<Users> query = em.createQuery(q);
        query.setParameter(t, "%"+companyName+"%");
        List<Users> usersList = query.getResultList();
        return usersList;
    }
    
    public List<Users> findUsersByCity(String city) {
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> q = cb.createQuery(Users.class);
        Root<Users> users = q.from(Users.class);
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(users).where(cb.like(users.get("city"), t));
        TypedQuery<Users> query = em.createQuery(q);
        query.setParameter(t, "%"+city+"%");
        List<Users> usersList = query.getResultList();
        return usersList;
    }
    
    public List<Users> findUsersByProvince(String provinceName) {
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> q = cb.createQuery(Users.class);
        Root<Users> users = q.from(Users.class);
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(users).where(cb.like(users.get("provinceName"), t));
        TypedQuery<Users> query = em.createQuery(q);
        query.setParameter(t, "%"+provinceName+"%");
        List<Users> usersList = query.getResultList();
        return usersList;
    }
    
    public List<Users> findUsersByPostalCode(String postalCode) {
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> q = cb.createQuery(Users.class);
        Root<Users> users = q.from(Users.class);
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(users).where(cb.like(users.get("postalCode"), t));
        TypedQuery<Users> query = em.createQuery(q);
        query.setParameter(t, "%"+postalCode+"%");
        List<Users> usersList = query.getResultList();
        return usersList;
    }
    
    public List<Users> findUsersByHomeTelephone(String homeTelephoe) {
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> q = cb.createQuery(Users.class);
        Root<Users> users = q.from(Users.class);
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(users).where(cb.like(users.get("homeTelephoe"), t));
        TypedQuery<Users> query = em.createQuery(q);
        query.setParameter(t, "%"+homeTelephoe+"%");
        List<Users> usersList = query.getResultList();
        return usersList;
    }
    
    public List<Users> findUsersByCellphone(String cellphone) {
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> q = cb.createQuery(Users.class);
        Root<Users> users = q.from(Users.class);
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(users).where(cb.like(users.get("cellphone"), t));
        TypedQuery<Users> query = em.createQuery(q);
        query.setParameter(t, "%"+cellphone+"%");
        List<Users> usersList = query.getResultList();
        return usersList;
    }
    
    public List<Users> findUsersByEmail(String email) {
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> q = cb.createQuery(Users.class);
        Root<Users> users = q.from(Users.class);
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(users).where(cb.like(users.get("email"), t));
        TypedQuery<Users> query = em.createQuery(q);
        query.setParameter(t, "%"+email+"%");
        List<Users> usersList = query.getResultList();
        return usersList;
    }
    
    public List<Users> findUsersEntities() {
        return findUsersEntities(true, -1, -1);
    }

    public List<Users> findUsersEntities(int maxResults, int firstResult) {
        return findUsersEntities(false, maxResults, firstResult);
    }

    private List<Users> findUsersEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Users.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    public Users findUsers(Integer id) {
        return em.find(Users.class, id);

    }
    
    /**
     * 
     * @author Hadil Elhashani
     * @param id
     * @return 
     */
    public List<Users> findUsersById(String id) {
        if(id.equals("") || id == null){
            return findUsersEntities();
        }
        int i = Integer.parseInt(id);
        List<Users> users = new ArrayList<Users>();
        users.add(em.find(Users.class, i));
        return users;
    }
    /**
     * @author Franco G. Moro
     * This method returns all the books entities which a user has bought.
     * @param user
     * @return List<Books>
     */
    public List<Books> getUserBooks(Users user){
        CriteriaBuilder cb=em.getCriteriaBuilder();
        CriteriaQuery<Books> booksQuery=cb.createQuery(Books.class);
        Root<Books> books=booksQuery.from(Books.class);
        
        Join<Books,SoldBooks> soldBooks=books.join("soldBooksList");
        Join<SoldBooks,Sales> sales=soldBooks.join("salesId");
        Join<Sales,Users> users=sales.join("usersId");
        
        booksQuery.select(books);
        booksQuery.where(cb.equal(users.get("id"), user.getId() ));
        
        TypedQuery<Books> typedQuery=em.createQuery(booksQuery);
        
        List<Books> booksList=typedQuery.getResultList();
        return booksList;
    }
    /**
     * Retrieve the number of users in the database
     * 
     * @return int number of users in database
     * @author Alexander Berestetskyy
     */
    public int getUsersCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Users> rt = cq.from(Users.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    /**
     * Retrieve the user with the given email (argument)
     * 
     * @param emailGiven email of an user
     * @return Users the user found based on argument
     * @author Alexander Berestetskyy
     */
    public Users findIdOfUser(String emailGiven){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> q = cb.createQuery(Users.class);
        Root<Users> user = q.from(Users.class);
        ParameterExpression<String> t = cb.parameter(String.class); 
        q.select(user).where(cb.like(user.get("email"), t));
        TypedQuery<Users> query = em.createQuery(q).setMaxResults(1);
        query.setParameter(t, "%"+emailGiven+"%");
        Users userFound = query.getSingleResult();
        return userFound;
    }
}
