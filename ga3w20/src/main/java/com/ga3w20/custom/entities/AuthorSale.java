package com.ga3w20.custom.entities;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * Represent total sales of books written by an author
 *
 * @author Hadil Elhashani
 */
public class AuthorSale {

    private String authorName;
    private String bookTitle;
    private BigDecimal bookPrice;
    private Date datePurchased;

    public AuthorSale(String authorName, String bookTitle, BigDecimal bookPrice, Date datePurchased) {
        this.authorName = authorName;
        this.bookTitle = bookTitle;
        this.bookPrice = bookPrice;
        this.datePurchased = datePurchased;
    }

    

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public BigDecimal getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(BigDecimal bookPrice) {
        this.bookPrice = bookPrice;
    }

    public Date getDatePurchased() {
        return datePurchased;
    }

    public void setDatePurchased(Date datePurchased) {
        this.datePurchased = datePurchased;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.authorName);
        hash = 71 * hash + Objects.hashCode(this.bookTitle);
        hash = 71 * hash + Objects.hashCode(this.bookPrice);
        hash = 71 * hash + Objects.hashCode(this.datePurchased);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AuthorSale other = (AuthorSale) obj;
        if (!Objects.equals(this.authorName, other.authorName)) {
            return false;
        }
        if (!Objects.equals(this.bookTitle, other.bookTitle)) {
            return false;
        }
        if (!Objects.equals(this.bookPrice, other.bookPrice)) {
            return false;
        }
        if (!Objects.equals(this.datePurchased, other.datePurchased)) {
            return false;
        }
        return true;
    }

}
