package com.ga3w20.custom.entities;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Represents a book with the number of times it has been purchased 
 * 
 * @author Hadil Elhashani
 */
public class TopSeller {
    
    private String isbn;
    private String imageUrl;
    private String bookTitle;
    private BigDecimal listPrice;
    private BigDecimal wholePrice;
    private Number totalSales;

    public TopSeller(String isbn, String imageUrl, String bookTitle, BigDecimal listPrice, BigDecimal wholePrice, Number totalSales) {
        this.isbn = isbn;
        this.imageUrl = imageUrl;
        this.bookTitle = bookTitle;
        this.listPrice = listPrice;
        this.wholePrice = wholePrice;
        this.totalSales = totalSales;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public BigDecimal getListPrice() {
        return listPrice;
    }

    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }

    public BigDecimal getWholePrice() {
        return wholePrice;
    }

    public void setWholePrice(BigDecimal wholePrice) {
        this.wholePrice = wholePrice;
    }

    public Number getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(Number totalSales) {
        this.totalSales = totalSales;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.isbn);
        hash = 59 * hash + Objects.hashCode(this.imageUrl);
        hash = 59 * hash + Objects.hashCode(this.bookTitle);
        hash = 59 * hash + Objects.hashCode(this.listPrice);
        hash = 59 * hash + Objects.hashCode(this.wholePrice);
        hash = 59 * hash + Objects.hashCode(this.totalSales);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TopSeller other = (TopSeller) obj;
        if (!Objects.equals(this.isbn, other.isbn)) {
            return false;
        }
        if (!Objects.equals(this.imageUrl, other.imageUrl)) {
            return false;
        }
        if (!Objects.equals(this.bookTitle, other.bookTitle)) {
            return false;
        }
        if (!Objects.equals(this.listPrice, other.listPrice)) {
            return false;
        }
        if (!Objects.equals(this.wholePrice, other.wholePrice)) {
            return false;
        }
        if (!Objects.equals(this.totalSales, other.totalSales)) {
            return false;
        }
        return true;
    }

    
}
