package com.ga3w20.custom.entities;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * Represent total sales of books published by a publisher
 * 
 * @author Hadil Elhashani
 */
public class PublisherSale {
    
    private String publisherName;
    private String bookTitle;
    private BigDecimal bookPrice;
    private Date datePurchased;

    public PublisherSale(String publisherName, String bookTitle, BigDecimal bookPrice, Date datePurchased) {
        this.publisherName = publisherName;
        this.bookTitle = bookTitle;
        this.bookPrice = bookPrice;
        this.datePurchased = datePurchased;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public BigDecimal getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(BigDecimal bookPrice) {
        this.bookPrice = bookPrice;
    }

    public Date getDatePurchased() {
        return datePurchased;
    }

    public void setDatePurchased(Date datePurchased) {
        this.datePurchased = datePurchased;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.publisherName);
        hash = 53 * hash + Objects.hashCode(this.bookTitle);
        hash = 53 * hash + Objects.hashCode(this.bookPrice);
        hash = 53 * hash + Objects.hashCode(this.datePurchased);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PublisherSale other = (PublisherSale) obj;
        if (!Objects.equals(this.publisherName, other.publisherName)) {
            return false;
        }
        if (!Objects.equals(this.bookTitle, other.bookTitle)) {
            return false;
        }
        if (!Objects.equals(this.bookPrice, other.bookPrice)) {
            return false;
        }
        if (!Objects.equals(this.datePurchased, other.datePurchased)) {
            return false;
        }
        return true;
    }
    
}
