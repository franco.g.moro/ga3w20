package com.ga3w20.custom.entities;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * Represents inventory information Used for stock report and total sales pages
 * in management site
 *
 * @author Hadil Elhashani
 */
public class Inventory {

    private String isbn;
    private String title;
    private String imageUrl;
    private BigDecimal listPrice;
    private BigDecimal salePrice;
    private BigDecimal wholeSalePrice;
    private Number totalSales;
    private long salesFigure;
    private Date date;

    /**
     * Used for total sales
     *
     * @param title
     * @param imageUrl
     * @param totalSales
     */
    public Inventory(String title, String imageUrl, Number totalSales) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.totalSales = totalSales;
    }

    /**
     * Used for stock report
     *
     * @param title
     * @param imageUrl
     * @param listPrice
     * @param wholeSalePrice
     * @param salesFigure
     */
    public Inventory(String title, String imageUrl, BigDecimal listPrice, BigDecimal wholeSalePrice, long salesFigure) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.listPrice = listPrice;
        this.wholeSalePrice = wholeSalePrice;
        this.salesFigure = salesFigure;
    }

    public Inventory(String isbn, String title, String imageUrl, BigDecimal listPrice, BigDecimal salePrice, BigDecimal wholeSalePrice, Number totalSales, long salesFigure, Date date) {
        this.isbn = isbn;
        this.title = title;
        this.imageUrl = imageUrl;
        this.listPrice = listPrice;
        this.salePrice = salePrice;
        this.wholeSalePrice = wholeSalePrice;
        this.totalSales = totalSales;
        this.salesFigure = salesFigure;
        this.date = date;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public BigDecimal getListPrice() {
        return listPrice;
    }

    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public BigDecimal getWholeSalePrice() {
        return wholeSalePrice;
    }

    public void setWholeSalePrice(BigDecimal wholeSalePrice) {
        this.wholeSalePrice = wholeSalePrice;
    }

    public Number getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(Number totalSales) {
        this.totalSales = totalSales;
    }

    public long getSalesFigure() {
        return salesFigure;
    }

    public void setSalesFigure(long salesFigure) {
        this.salesFigure = salesFigure;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.isbn);
        hash = 97 * hash + Objects.hashCode(this.title);
        hash = 97 * hash + Objects.hashCode(this.imageUrl);
        hash = 97 * hash + Objects.hashCode(this.listPrice);
        hash = 97 * hash + Objects.hashCode(this.salePrice);
        hash = 97 * hash + Objects.hashCode(this.wholeSalePrice);
        hash = 97 * hash + Objects.hashCode(this.totalSales);
        hash = 97 * hash + (int) (this.salesFigure ^ (this.salesFigure >>> 32));
        hash = 97 * hash + Objects.hashCode(this.date);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Inventory other = (Inventory) obj;
        if (this.salesFigure != other.salesFigure) {
            return false;
        }
        if (!Objects.equals(this.isbn, other.isbn)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.imageUrl, other.imageUrl)) {
            return false;
        }
        if (!Objects.equals(this.listPrice, other.listPrice)) {
            return false;
        }
        if (!Objects.equals(this.salePrice, other.salePrice)) {
            return false;
        }
        if (!Objects.equals(this.wholeSalePrice, other.wholeSalePrice)) {
            return false;
        }
        if (!Objects.equals(this.totalSales, other.totalSales)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

}
