package com.ga3w20.selector;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * This class purpose is to control the current page displayed.
 *
 * @author Michael Mishin 1612993
 */
@Named
@RequestScoped
public class Selector {
    
    private final String welcomePage;
    private final String signup;
    private final String signin;
    private final String management;
    private final String shoppingCart;
    private final String browsepage;
    private final String browsebygenre;
    private final String bookpage;
    private final String page404;
    private final String invoice;
    private final String downloads;
    private final String help;

    /**
     * constructor
     * @author Michael Mishin
     * @author Franco G. Moro
     */
    public Selector() {
        welcomePage = "/WEB-INF/content/welcomepage.xhtml";
        signup = "/WEB-INF/content/signupcontent.xhtml";
        signin = "/WEB-INF/content/signincontent.xhtml";
        management = "/WEB-INF/content/management.xhtml";
        shoppingCart = "/WEB-INF/content/shoppingCartContent.xhtml";
        browsepage = "/WEB-INF/content/browsepage.xhtml";
        browsebygenre = "/WEB-INF/content/browsebygenre.xhtml";
        bookpage = "/WEB-INF/content/bookpage.xhtml";
        page404 = "/WEB-INF/errorpages/404.xhtml";
        invoice = "/WEB-INF/content/invoice.xhtml";
        downloads= "/WEB-INF/content/downloads.xhtml";
        help = "/WEB-INF/content/help.xhtml";
    }

    /**
     * This method returns the path to the welcome page.
     * 
     * @return The path to the content
     * @author Michael Mishin
     */
    public String getWelcomePage()
    {
        return welcomePage;
    }

    /**
     * This method returns the path to the sign-up page.
     * 
     * @return The path to the content
     * @author Michael Mishin
     */
    public String getSignup()
    {
        return signup;
    }
    
    /**
     * This method returns the path to the sign-in page.
     * 
     * @return The path to the content
     * @author Michael Mishin
     */
    public String getSignin()
    {
        return signin;
    }
    
    /**
     * This method returns the path to the welcome page.
     * This method returns the path to the shopping cart page.
     * 
     * @return The path to the content
     * @author Michael Mishin
     */
    public String getManagement()
    {
        return management;
    }
    
    /**
     * This method returns the path to the welcome page.
     * This method returns the path to the shopping cart page.
     * 
     * @return The path to the content
     * @author Michael Mishin
     */
    public String getShoppingCart()
    {
        return shoppingCart;
    }
    
    /**
     * This method returns the path to the browse page.
     * 
     * @return The path to the content
     * @author Alexander Berestetskyy
     */
    public String getBrowsepage(){
        return browsepage;
    }
    
    /**
     * This method returns the path to the browse by genre page.
     * 
     * @return The path to the content
     * @author Alexander Berestetskyy
     */
    public String getBrowsebygenre(){
        return browsebygenre;
    }
    
    /**
     * This method returns the path to the book page.
     * 
     * @return The path to the content
     * @author Alexander Berestetskyy
     */
    public String getBookpage(){
        return bookpage;
    }
    
    /**
     * This method returns the path to the 404 page.
     * 
     * @return The path to the 404 page
     * @author Michael Mishin
     */
    public String getPage404(){
        return page404;
    }
    
    /**
     * This method returns the path to the invoice page.
     * 
     * @return The path to the invoice
     * @author Michael Mishin
     */
    public String getInvoice(){
        return invoice;
    }

    public String getDownloads() {
        return downloads;
    }

     /**
     * This method returns the path to the help page.
     * 
     * @return The path to the help page
     * @author Alexander Berestetskyy
     */
    public String getHelp(){
        return help;
    }
}
