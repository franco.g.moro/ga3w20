package com.ga3w20.selector;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Define pages for all report pages
 * 
 * @author Hadil Elhashani
 */
@Named
@RequestScoped
public class Report {

    private final String salesByAuthor;
    private final String salesByClient;
    private final String salesByPublisher;
    private final String topSellers;
    private final String topClients;
    private final String stockReport;
    private final String zeroSales;
    private final String totalSales;

    public Report() {
        this.salesByAuthor = "/WEB-INF/content/management/salesByAuthor.xhtml";
        this.salesByClient = "/WEB-INF/content/management/salesByClient.xhtml";
        this.salesByPublisher = "/WEB-INF/content/management/salesByPublisher.xhtml";
        this.topSellers = "/WEB-INF/content/management/topSellers.xhtml";
        this.topClients = "/WEB-INF/content/management/topClients.xhtml";
        this.stockReport = "/WEB-INF/content/management/stockReport.xhtml";
        this.zeroSales = "/WEB-INF/content/management/zeroSales.xhtml";
        this.totalSales = "/WEB-INF/content/management/totalSales.xhtml";
    }

    public String getSalesByAuthor() {
        return salesByAuthor;
    }

    public String getSalesByClient() {
        return salesByClient;
    }

    public String getSalesByPublisher() {
        return salesByPublisher;
    }

    public String getTopSellers() {
        return topSellers;
    }

    public String getTopClients() {
        return topClients;
    }

    public String getStockReport() {
        return stockReport;
    }

    public String getZeroSales() {
        return zeroSales;
    }

    public String getTotalSales() {
        return totalSales;
    }
   
}


