package com.ga3w20.selector;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * 
 *
 * @author Hadil Elhashani 1640612
 */
@Named 
@RequestScoped
public class Management {
    
    private final String managerPage;
    private final String managerHelp;
    private final String manageBannerAds;
    private final String manageClients;
    private final String manageNewsFeed;
    private final String manageOrders;
    private final String manageReports;
    private final String manageReviews;
    private final String manageSales;
    private final String manageSurveys;
    private final String manageSurveyEditAnswer;
    private final String manageSurveyEditQuestion;
    private final String manageInventory;
    private final String addBook;
    private final String editBook;
    private final String editUserInfo;



    /**
     * constructor
     * @author Hadil Elhashani 
     */
    public Management() {
        this.managerPage = "/WEB-INF/content/management.xhtml";
        this.managerHelp = "/WEB-INF/content/help.xhtml";
        this.manageBannerAds = "/WEB-INF/content/management/manageBannerAds.xhtml";
        this.manageClients = "/WEB-INF/content/management/manageClients.xhtml";
        this.manageNewsFeed = "/WEB-INF/content/management/manageNewsFeed.xhtml";
        this.manageOrders = "/WEB-INF/content/management/manageOrders.xhtml";
        this.manageReports = "/WEB-INF/content/management/manageReports.xhtml";
        this.manageReviews = "/WEB-INF/content/management/manageReviews.xhtml";
        this.manageSales = "/WEB-INF/content/management/manageSales.xhtml";
        this.manageSurveys = "/WEB-INF/content/management/manageSurveys.xhtml";
        this.manageSurveyEditAnswer = "/WEB-INF/content/management/manageSurveyEditAnswer.xhtml";
        this.manageSurveyEditQuestion = "/WEB-INF/content/management/manageSurveyEditQuestion.xhtml";
        this.manageInventory = "/WEB-INF/content/management/manageInventory.xhtml";
        this.addBook = "/WEB-INF/content/management/addBook.xhtml";
        this.editBook = "/WEB-INF/content/management/editBook.xhtml";
        this.editUserInfo = "/WEB-INF/content/management/editUserInfo.xhtml";
    }

    /**
     * This method returns the path to the management page.
     * 
     * @return 
     * @author Hadil Elhashani
     */
    public String getManagerPage()
    {
        return managerPage;
    }

    public String getManagerHelp()
    {
        return managerHelp;
    }
    
    
    public String getManageBannerAds() {
        return manageBannerAds;
    }

    public String getManageClients() {
        return manageClients;
    }

    public String getManageNewsFeed() {
        return manageNewsFeed;
    }

    public String getManageOrders() {
        return manageOrders;
    }

    public String getManageReports() {
        return manageReports;
    }

    public String getManageReviews() {
        return manageReviews;
    }

    public String getManageSales() {
        return manageSales;
    }

    public String getManageSurveys() {
        return manageSurveys;
    }
    
    public String getManageSurveyEditAnswer() {
        return manageSurveyEditAnswer;
    }
    
    public String getManageSurveyEditQuestion() {
        return manageSurveyEditQuestion;
    }

    public String getManageInventory()
    {
        return manageInventory;
    }
    
    public String getAddBook() {
        return addBook;
    }

    public String getEditBook() {
        return editBook;
    }

    public String getEditUserInfo() {
        return editUserInfo;
    }
    
}
