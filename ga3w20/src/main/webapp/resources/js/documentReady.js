/**
 * This file loads elements and prepares events on Pageload
 * 
 * @author Michael Mishin 1612993
 */
$(document).ready(function(){
    
    //This Section is for collapsing the description area in the shopping cart 
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
      coll[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.maxHeight){
          content.style.maxHeight = null;
        } else {
          content.style.maxHeight = content.scrollHeight + "px";
        } 
      });
    }
    
    var btn = $('#backToTop');

    $(window).scroll(function() {
      if ($(window).scrollTop() > 300) {
        btn.addClass('show');
      } else {
        btn.removeClass('show');
      }
    });

    btn.on('click', function(e) {
      e.preventDefault();
      $('html, body').animate({scrollTop:0}, '300');
    });
    
    
    // this section adds classes dynamicly for the survey for animation because JSF does not like it when I do this notmally :(
    
    $('.SurveyContainer input[type=radio]').each(function() {
    $(this).after($('<div />', {
        class: 'check',
        })); 
    });
    
});

