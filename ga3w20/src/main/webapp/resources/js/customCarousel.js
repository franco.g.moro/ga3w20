/*
 * This js file is to set the settings of the carousel used in the front page multiple times
 * 
 * @author Michael Mishin
 */ 

$(document).ready(function(){
    
    // new Books ================================
    $('.showcaseNewBooks').slick({
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.shelfNewBooks'
     });
    
    $('.shelfNewBooks').slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.showcaseNewBooks',
        autoplay: true,
        autoplaySpeed: 2500,
        variableWidth: true,
        adaptiveHeight: true
    });
    
    //Books on sale ================================
    $('.showcaseBooksOnSale').slick({
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.shelfBooksOnSale'
     });
    
    $('.shelfBooksOnSale').slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.showcaseBooksOnSale',
        autoplay: true,
        autoplaySpeed: 2500,
        variableWidth: true,
        adaptiveHeight: true
    });
    
    // recommended books ================================
    $('#frontPageRecommendation').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        responsive: [
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false
          }
        }]
    });
    
    //news feed (changes with screen size)================
    $('#newsScroll').slick({
        dots: true,
        infinite: true,
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false
          }
        }]
    });
    
    // this will make sure to allign images in middle
    $('.newsFeedData').find('img').addClass('center-block');
});